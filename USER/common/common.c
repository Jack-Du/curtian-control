#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"


/***
***
*将hex数组转换成字符串
*
*打印显示
*
*
*
*******/
uint8_t hextostring(uint8_t *dest,uint8_t *src, uint8_t len)
{

	uint8_t i = 0x00;
	
	const uint8_t array[16]= {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	uint8_t *tmp = (uint8_t*)malloc((sizeof(uint8_t) *len) << 1);

	if (tmp == NULL)
	{
		return 0x00;
	}
	
	for (i = 0x00;i < len ;i ++)
	{
		tmp[i << 1] = array[src[i] >>4];
		tmp[(i << 1) + 1] = array[src[i] & 0x0f];
	}

	memcpy(dest,tmp,len*2);
	free(tmp);
	
	return (len <<1);
}

void Sring_Mask_ToOx(char*srcbuf,uint8_t src_len,uint8_t* dstbuf,uint8_t dstlen)
{
			uint8_t i;
			char TempBuf[12];
			memset(dstbuf, 0, dstlen);
			memset(TempBuf,0,12);
     
			for(i = 0; i < src_len; i++) 
			{
					if(srcbuf[i]>='0' && srcbuf[i]<='9')
						TempBuf[i] = srcbuf[i]-48;
					else if(srcbuf[i]>='A' && srcbuf[i]<='F')
						TempBuf[i] = srcbuf[i]-55;
					else if(srcbuf[i]>='a' && srcbuf[i]<='f')
						TempBuf[i] = srcbuf[i]-87;
	 		}	
			for(i = 0;i< 6;i++)
			{
					dstbuf[i] = TempBuf[i*2]*16+TempBuf[i*2+1];
			}
}

void SringToOx(char*srcbuf,uint8_t src_len,uint8_t* dstbuf,uint8_t dstlen)
{
		uint8_t i,j,k;
		char TempBuf[16];
		//获取待设置的网络掩码
			memset(dstbuf, 0, dstlen);
			memset(TempBuf,0,16);
//			j = dstlen - 1;
      j = 0;
			k = 0;
			for(i = 0; i < src_len; i++) 
			{
				if(srcbuf[i] != '.')
				{
					TempBuf[k++] = srcbuf[i];
				}
				else
				{
					dstbuf[j] = atoi(TempBuf);
					memset(TempBuf,0,16);
					k = 0;
					if(j == (dstlen -1))
					{
						break;
					}
					else
					{
						j++;
					}
				}					
	 		}		
			dstbuf[j] = atoi(TempBuf);
}
