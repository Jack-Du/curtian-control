#ifndef _COMMON_H
#define _COMMON_H

#include <stdint.h>

extern uint8_t hextostring(uint8_t *dest,uint8_t *src, uint8_t len);
extern void Sring_Mask_ToOx(char*srcbuf,uint8_t src_len,uint8_t* dstbuf,uint8_t dstlen);
extern void SringToOx(char*srcbuf,uint8_t src_len,uint8_t* dstbuf,uint8_t dstlen);
extern void get_system_restart_symbol(void );

#endif

