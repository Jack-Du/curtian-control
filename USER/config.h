#ifndef _CONFIG_H
#define _CONFIG_H


/*******************************网络参数相关******************************************/
/*本机IP地址*/
/*网关*/
/*掩码*/
/*MAC地址*/
/**/
/*服务器地址 :*/
/*服务器端口*/
/*本机监听端口:默认8899*/

#include <stdint.h>

//#define   uint8_t unsigned char
//#define   uint16_t unsigned short int 
//#define uint32_t unsigned int 

/*软件版本号:*/
#define SOFT_VERSION "1.4"
#define BOARD_VERSION "om_v:3.1"

/*定义mcu 类型*/
#define BOARD_MCU_STM32F103RC

#if !defined (BOARD_MCU_STM32F103RB) && !defined (BOARD_MCU_STM32F103RC) && !defined (BOARD_MCU_STM32F103RE) 
	/*默认mcu 型号*/
	#define BOARD_MCU_STM32F103RC
#endif

#define DEBUG_FLASH 0X00
#define USE_WIFI_ETH_COMM_ENABLE


/***
*
*
*只用WiFi进行通信
*
*
* 不使用 ucosiii 系统
* 启用断网重连功能
*
*
*
*
*/
#ifdef USE_WIFI_COMM_ENABLE 

/*关闭w5500 网络*/
#define ENABLE_W5500_NET 0X00
/*启动模式检测*/
#define SYSTEM_START_INFO_CHECK_ENABLE 0X00

#define DEBUG_W5500_PRINTF_ENABLE 0X00


/*使能串口wifi通信*/
#define SERIAL_WIFI_COMMUNICATION_ENABLE 0X01
#define RTL8711_WIFI_MONITOR_ENABLE 0X01

//0,不支持ucos
//1,支持ucos
#define SYSTEM_SUPPORT_OS		0x00		//定义系统文件夹是否支持UCOS

/***
*
*
*只用以太网进行通信
*
*
* 不使用 ucosiii 系统
* 启用断网重连功能
*
*
*
*
*/
#elif defined USE_ETH_COMM_ENABLE 

/*关闭w5500 网络*/
#define ENABLE_W5500_NET 0X01
/*启动模式检测*/
#define SYSTEM_START_INFO_CHECK_ENABLE 0X01
#define DEBUG_W5500_PRINTF_ENABLE 0X01

/*使能串口wifi通信*/
#define SERIAL_WIFI_COMMUNICATION_ENABLE 0X00
#define RTL8711_WIFI_MONITOR_ENABLE 0X00

//0,不支持ucos
//1,支持ucos
#define SYSTEM_SUPPORT_OS		0x00		//定义系统文件夹是否支持UCOS

/***
*
*
*同时使用WiFi和以太网通信
*
*
* 使用 ucosiii 系统
* 启用断网重连功能
*
*
*
*
*/
#elif defined USE_WIFI_ETH_COMM_ENABLE
/*关闭w5500 网络*/
#define ENABLE_W5500_NET 0X01
/*启动模式检测*/
#define SYSTEM_START_INFO_CHECK_ENABLE 0X01
#define DEBUG_W5500_PRINTF_ENABLE 0X00


/*使能串口wifi通信*/
#define SERIAL_WIFI_COMMUNICATION_ENABLE 0X01
#define RTL8711_WIFI_MONITOR_ENABLE 0X01

//0,不支持ucos
//1,支持ucos
#define SYSTEM_SUPPORT_OS		0x01		//定义系统文件夹是否支持UCOS

#else

/*关闭w5500 网络*/
#define ENABLE_W5500_NET 0X01
/*启动模式检测*/
#define SYSTEM_START_INFO_CHECK_ENABLE 0X01

#define DEBUG_W5500_PRINTF_ENABLE 0X01


/*使能串口wifi通信*/
#define SERIAL_WIFI_COMMUNICATION_ENABLE 0X00
#define RTL8711_WIFI_MONITOR_ENABLE 0X00

//0,不支持ucos
//1,支持ucos
#define SYSTEM_SUPPORT_OS		0x00		//定义系统文件夹是否支持UCOS

#endif

#define	SOCKET0						0  //tcp server:环控网关
#define	SOCKET1						1  //udp       :配置工具	
#define	SOCKET2						2  //
#define	SOCKET3						3  //
#define	SOCKET4						4  //
#define	SOCKET5						5
#define	SOCKET6						6
#define	SOCKET7						7


extern char BuildDataStr[];   //编译时的日期
extern char BuildTimeStr[];   //编译时的时间

extern char SoftVersion[];
extern char BoardVersion[];
extern char LoaderVersion[16];



/*初始化*/
extern void init_config_info(void);

/*从flash读取配置信息*/
extern void read_config_info(void);

/**将修改的信息存储到flash*/
extern void write_config_info(void);


#endif
