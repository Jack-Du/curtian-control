/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/stm32f10x_it.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h"  
#include "out.h"    //控制继电器
#include "bsp_usart3.h"
#include "w5500_net_handle.h"
#include "system_start_info.h"
#include "serial_comm.h"
#include <string.h>

extern  u8 W5500_Interrupt;	//W5500中断标志(0:无中断,1:有中断)  
extern  u16  HoldReg[20];
extern  u16 outgpio; //控制继电器的变量
/** @addtogroup STM32F10x_StdPeriph_Template
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
	
	usart_printf_string("Hard Fault\n");
			/*复位系统*/
#if SYSTEM_START_INFO_CHECK_ENABLE == 0X01
		SystemStartInfo.SystemResetInfo.system_reset_bit.system_update = 0x00;
		SystemStartInfo.SystemResetInfo.system_reset_bit.hard_fault = 0x01;
#endif	

	/*系统使能	*/
#if SYSTEM_SUPPORT_OS == 0x01
			SoftWDTRst();
#else
			delay_ms(1000);
			
			/*软件复位*/
			__set_FAULTMASK(1);
			NVIC_SystemReset();
#endif

  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

///**
//  * @brief  This function handles PendSVC exception.
//  * @param  None
//  * @retval None
//  */
//void PendSV_Handler(void)
//{
//}

///**
//  * @brief  This function handles SysTick Handler.
//  * @param  None
//  * @retval None
//  */
//void SysTick_Handler(void)
//{
//}

/******************************************************************************/
/*                 STM32F10x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f10x_xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 

/*******************************************************************************
* 函数名  : EXTI4_IRQHandler
* 描述    : 中断线4中断服务函数(W5500 INT引脚中断服务函数)
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 无
*******************************************************************************/
void EXTI4_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line4) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line4);
		W5500_Interrupt=1;
	}
}

//PC8 
//PC9
//PC3
void EXTI9_5_IRQHandler(void)
{

	uint16_t outgpio = 0x00;
	
	if(EXTI_GetITStatus(EXTI_Line8) != RESET) //确保是否产生了EXTI Line中断  PC8
	{
		
	  if(GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_8) == 0 )  //判断是高电平就关灯
		{
		 outgpio = outgpio | 0x0001; 
		 HoldReg[0] = outgpio; 

		 set_relay_current_state(outgpio);		 
		 enable_relay_out(0x01); 
		}
	  if( GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_8) == 1 )  //判断是低电平就开灯
		{
		 outgpio = outgpio & 0xfffe; 
		 HoldReg[0] = outgpio; 

		 set_relay_current_state(outgpio);		 
		 enable_relay_out(0x01); 

		}		
		EXTI_ClearITPendingBit(EXTI_Line8);     //清除中断标志位 	

	}
	
	if(EXTI_GetITStatus(EXTI_Line9) != RESET) //确保是否产生了EXTI Line中断  PC9
	{
		
	  if( GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_9) == 0 )  //判断是高电平就关灯
		{
			 outgpio = outgpio | 0x0002;
			 HoldReg[0] = outgpio; 

			 set_relay_current_state(outgpio);		 
			 enable_relay_out(0x01); 
		}
	  if( GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_9) == 1 )  //判断是低电平就开灯
		{
			 outgpio = outgpio & 0xfffd;  
			 HoldReg[0] = outgpio; 

			 set_relay_current_state(outgpio);		 
			 enable_relay_out(0x01); 

		}		
		EXTI_ClearITPendingBit(EXTI_Line9);     //清除中断标志位 
		
	}
  
}
 
void EXTI3_IRQHandler(void)
{

	uint16_t outgpio = 0x00;
	if(EXTI_GetITStatus(EXTI_Line3) != RESET) //确保是否产生了EXTI Line中断
	{
		
   	if( GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_3) == 0 )  //判断是高电平就关灯
		{
			 outgpio = outgpio | 0x0004;
			 HoldReg[0] = outgpio; 

			 set_relay_current_state(outgpio);		 
			 enable_relay_out(0x01); 
		}
	  if( GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_3) == 1 )  //判断是低电平就开灯
		{
			 outgpio = outgpio & 0xfffb; 
			 HoldReg[0] = outgpio; 
			 
			 set_relay_current_state(outgpio); 	 
			 enable_relay_out(0x01); 
		}			
		EXTI_ClearITPendingBit(EXTI_Line3);     //清除中断标志位 	
	}

}



/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
