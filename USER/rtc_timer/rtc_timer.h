#ifndef _RTC_TIMER_H_
#define _RTC_TIMER_H_

#include "stm32f10x.h"
#include "time.h"

/*       
		tm_sec表「秒」数，在[0,61]之间，多出来的两秒是用来处理跳秒问题用的。  
        tm_min表「分」数，在[0,59]之间。  
       tm_hour表「时」数，在[0,23]之间。  
       tm_mday表「本月第几日」，在[1,31]之间。  
       tm_mon表「本年第几月」，在[0,11]之间。  
       tm_year要加1900表示那一年。  
       tm_wday表「本第几日」，在[0,6]之间。  
       tm_yday表「本年第几日」，在[0,365]之间，闰年有366日。  
       tm_isdst表是否为「日光节约时间」。 
	   */
typedef struct
{
	int tm_sec;  
	int tm_min;  
	int tm_hour;  
	int tm_mday;  
	int tm_mon;  
	int tm_year;  
	int tm_wday;  
	int tm_yday;  
	int tm_isdst;  
}TIME;

typedef struct
{
    u8 second; //秒
    u8 minute; //分钟
    u8 hour;   //小时
    u8 day;    //日
    u8 month;  //月
    u8 year;   //年
}RTC_time_t;    /* 实时时钟的时间格式类型 */


//曾在某平台下多线程中使用localtime库函数。可恶的是，每当程序运行
//一段时间后，都要出现内存泄露。查了数个夜晚为什么，无从断定。
//只能狠狠心，自己写了个localtimes，功能与localtime库函数相同。
//有了它以后，我就不再因内存泄露而烦恼了。
//要知道“内存泄露”是程序员大的一大耻辱。
//这个localtimes在多线程下已经运行了3年，还没发生问题，放心使用。

//一年中每个月的天数，非闰年
extern const char Days[12];
extern RTC_time_t RtcTime;
extern TIME Time;
extern time_t SecondTime;
/*++------------------------------------------------------------------------

  Function:
              localtimes

Modification History

Jurassic           2002.1    Created.              
--------------------------------------------------------------------------*/
//获取当前时间的time_t 格式
extern time_t Get_Time_T(void);

//当前time_t时间转换为Time 格式
//会改变全局变量Time
extern u8 Get_Time(void);
extern void init_rtc_time(void);
extern void rtc_internal_timing(void);

#endif

