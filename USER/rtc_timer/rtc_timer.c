#include "rtc_timer.h"

time_t SecondTime=0;   //time_t 格式的计时
TIME Time;
RTC_time_t RtcTime;
u16 TickHookCnt=0;     //计数器10分钟


//static inline unsigned long mktime (unsigned int year, unsigned int mon,
//    unsigned int day, unsigned int hour,
//    unsigned int min, unsigned int sec)

time_t linux_mktime(struct tm * timeptr)    
{
    if (0 >= (int) (timeptr->tm_mon -= 2)) {    /* 1..12 -> 11,12,1..10 */
         timeptr->tm_mon  += 12;      /* Puts Feb last since it has leap day */
         timeptr->tm_year  -= 1;
    }

    return (((
             (unsigned long) (timeptr->tm_year/4 - timeptr->tm_year/100 + timeptr->tm_year/400 + 367*timeptr->tm_mon/12 + timeptr->tm_mday) +
             timeptr->tm_year*365 - 719499
          )*24 + timeptr->tm_hour /* now have hours */
       )*60 + timeptr->tm_min /* now have minutes */
    )*60 + timeptr->tm_sec; /* finally seconds */
}

//曾在某平台下多线程中使用localtime库函数。可恶的是，每当程序运行
//一段时间后，都要出现内存泄露。查了数个夜晚为什么，无从断定。
//只能狠狠心，自己写了个localtimes，功能与localtime库函数相同。
//有了它以后，我就不再因内存泄露而烦恼了。
//要知道“内存泄露”是程序员大的一大耻辱。
//这个localtimes在多线程下已经运行了3年，还没发生问题，放心使用。

//一年中每个月的天数，非闰年
const char Days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

/*++------------------------------------------------------------------------

  Function:
              localtimes

Modification History

Jurassic           2002.1    Created.              
--------------------------------------------------------------------------*/
void linux_localtime(time_t time,TIME *tm_time)
{
    u32 n32_Pass4year;
    u32 n32_hpery;

    //取秒时间
    tm_time->tm_sec=(int)(time%60);
    time=time/60;
    //取分钟时间
    tm_time->tm_min=(int)(time%60);
    time/= 60;
    //取过去多少个四年，每四年有 1461*24 小时
    n32_Pass4year=((unsigned int)time / (1461L * 24L));
    //计算年份
    tm_time->tm_year=(n32_Pass4year << 2)+70;
    //四年中剩下的小时数
    time %= 1461L * 24L;
    //校正闰年影响的年份，计算一年中剩下的小时数
    for (;;)
    {
        //一年的小时数
        n32_hpery = 365 * 24;
        //判断闰年
        if ((tm_time->tm_year & 3) == 0)
		{
            //是闰年，一年则多24小时，即一天
            n32_hpery += 24;
		}
        if (time < n32_hpery)
		{
            break;
		}
        tm_time->tm_year++;
        time -= n32_hpery;
    }
    //小时数
    tm_time->tm_hour=(int)(time % 24);
    //一年中剩下的天数
    time /= 24;
    //假定为闰年
    time++;
    //校正润年的误差，计算月份，日期
    if ((tm_time->tm_year & 3) == 0)
    {
		if (time > 60)
		{
			time--;
		}
		else
		{
			if (time == 60)
			{
				tm_time->tm_mon = 1;
				tm_time->tm_mday = 29;
				return ;
			}
		}
    }
    //计算月日
    for (tm_time->tm_mon = 0; Days[tm_time->tm_mon] < time;tm_time->tm_mon++)
    {
          time -= Days[tm_time->tm_mon];
    }

    tm_time->tm_mday = (int)(time);

    return;

}
/**
*
*初始化rtc软件计时初始值
*
*/
void init_rtc_time(void)
{
	RtcTime.year = 21;	
	RtcTime.month = 10;
	RtcTime.day = 19;
	RtcTime.hour = 9;
	RtcTime.minute = 45;
	RtcTime.second = 0;
	return ;
}

//获取当前时间的time_t 格式
time_t Get_Time_T(void)
{
	struct tm t;                        // !!!!!!!!!!   这个地方用的是非正式的tm
	time_t t_of_day;

	t.tm_year=RtcTime.year+2000;	
	t.tm_mon=RtcTime.month;
	t.tm_mday=RtcTime.day;
	t.tm_hour=RtcTime.hour;
	t.tm_min=RtcTime.minute;
	t.tm_sec=RtcTime.second;
	t.tm_isdst=0;	
	t_of_day=linux_mktime(&t);	
	return t_of_day;
}

//当前time_t时间转换为Time 格式
//会改变全局变量Time
u8 Get_Time(void)
{
	struct tm t;
	TIME ext_time;
	linux_localtime(SecondTime,&ext_time);
	ext_time.tm_year+=1900;
	ext_time.tm_mon+=1;
	memcpy(&(Time),&ext_time,sizeof(TIME));		
//	DebugOut(0,"\r\nGet_Time:%d-%02d-%02d %02d:%02d:%02d",Time.tm_year,Time.tm_mon,Time.tm_mday,Time.tm_hour,Time.tm_min,Time.tm_sec);	
	
	return 0;
}

void rtc_internal_timing(void)
{
	TickHookCnt++;
	if(TickHookCnt>=60000)
	{	    
		TickHookCnt=0;
    }
	if((TickHookCnt%200)==0)	  //1s 计时
	{
		SecondTime++;   /**内部时钟计数*/
	}

}

