#include "RTU.H"  
#include "out.h"
#include "HWLED.h"
#include "serial_comm.h"

u16   HoldReg[20]={0}; 	   //保持寄存器  03, 06 功能码用
u16   InputReg[20]={0};    //输入寄存器  04功能码用

//对应功能码 03
// 0  1  2  3  4  5    6    7
//01 03 00 00 00 00 CRCH CRCL 
//返回数据格式 ：
// 0  1  2  3  4    5    6  
//01 03 02 01 02 CRCH CRCL   =  7个字节  
//    02 ：表示有效字节数
void ReadHoldingReg(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s)  // pointer_1是从网口接收缓存指针pointer_2是网口发送缓存指针
{ 
 u16 Address=0;//待读取寄存器起始地址HoldReg[i],i为0-99对应地址从0到99 
 u16 Num=0;    //要读取的寄存器个数 
 u16 SendKey;  //要发送数据的校验值   
 Address=(u16)(*(pointer_1+2))*256+(*(pointer_1+3));//先得到寄存器起始地址  	
 Num=(u16)(*(pointer_1+4))*256+(*(pointer_1+5));    //先得到要读取的寄存器个数 一个寄存器  =  2字节
 //把寄存器个数合并    
	 if(len_r==8)   //如果接收到的有效字节数是 8
	 { 
		  if(Address<100) //只要地址小于100就是合法地址 
		  { 
  
				    //用于for循环 
				    u8 i; 
				    u8 j; 
					
					*pointer_2 = *pointer_1;           //第 0个字节是ID号
            		*(pointer_2+1) = *(pointer_1+1);   //第 1个字节是功能码					 
				    *(pointer_2+2)=Num*2;              //第2个字节为要发送的字节个数 
				    *len_s = 1+1+1+Num*2+2;            //有效字节个数等于丛机地址+功能码+字节个数+寄存器信息+CRC校验  
               		//     0       3     0 + 1    1   5
				    for(i=Address,j=3;i<Address+Num;i++,j+=2) 
				    { 
				     *(pointer_2+j)=(u8)(HoldReg[i]>>8);//先放高位   
				     *(pointer_2+j+1)=(u8)(HoldReg[i]&0x00FF);//再放低位  
				    } 
				    //写入校验码       		
				    SendKey=CRC16(pointer_2,*len_s-2);//开始校验的地址，计算的字节数    		
				    //将计算出来的校验码装入输出数据缓存中 		
				    *(pointer_2 + (*len_s)-2)=(u8)(SendKey>>8); 		
				    *(pointer_2+ (*len_s)-1 )=(u8)(SendKey&0x00FF);
             						 
  
		  } 	
		else
		{
		
			* ( pointer_2+1 ) =* ( pointer_1+1 );
			ErrorHandle ( 2,pointer_2 ,len_s); //错误起始地址

		}
	 } 	
	else 
	{ 
		*len_s = 0;
	  return ; 	
	} 

} 

void ReadIntputReg(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s)  // pointer_1是从网口接收缓存指针pointer_2是网口发送缓存指针  04 功能码
{ 
 u16 Address=0;//待读取寄存器起始地址HoldReg[i],i为0-99对应地址从0到99 
 u16 Num=0;    //要读取的寄存器个数 
 u16 SendKey;  //要发送数据的校验值   
 Address=(u16)(*(pointer_1+2))*256+(*(pointer_1+3));//先得到寄存器起始地址  	
 Num=(u16)(*(pointer_1+4))*256+(*(pointer_1+5));    //先得到要读取的寄存器个数 一个寄存器  =  2字节
 //把寄存器个数合并    
	 if(len_r==8)   //如果接收到的有效字节数是 8
	 { 
		  if(Address<100) //只要地址小于100就是合法地址 
		  { 
 
				    //用于for循环 
				    u8 i; 
				    u8 j; 
						*pointer_2 = *pointer_1;           //第 0个字节是ID号
            *(pointer_2+1) = *(pointer_1+1);   //第 1个字节是功能码					 
				    *(pointer_2+2)=Num*2;              //第2个字节为要发送的字节个数 
				    *len_s = 1+1+1+Num*2+2;            //有效字节个数等于丛机地址+功能码+字节个数+寄存器信息+CRC校验  
               		//     0       3     0 + 1    1   5
						    for(i=Address,j=3;i<Address+Num;i++,j+=2) 
						    { 
						     *(pointer_2+j)=(u8)(InputReg[i]>>8);//先放高位   
						     *(pointer_2+j+1)=(u8)(InputReg[i]&0x00FF);//再放低位  
						    } 
				    //写入校验码       		
				    SendKey=CRC16(pointer_2,*len_s-2);//开始校验的地址，计算的字节数    		
				    //将计算出来的校验码装入输出数据缓存中 		
				    *(pointer_2 + (*len_s)-2)=(u8)(SendKey>>8); 		
				    *(pointer_2+ (*len_s)-1 )=(u8)(SendKey&0x00FF);
             						 	 
		  } 	
		else
		{	
			* ( pointer_2+1 ) =* ( pointer_1+1 );
			ErrorHandle ( 2,pointer_2,len_s ); //错误起始地址
		}
	 } 	
	else 
	{ 
		*len_s = 0;
	  return ;
	} 

} 


//对应功能码 06
// 0  1  2  3  4  5    6    7
//01 06 00 00 00 00 CRCH CRCL 
//返回数据格式 ：
//01 01 CRCH CRCL 
void WritHoldingReg(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s) //写数据到单个寄存器中  06  向一个寄存器写一个字节的数据
{
 u16 Address=0;//待读取寄存器起始地址HoldReg[i],i为0-99对应地址从0到99 
 u16 Data=0;    //要读取的寄存器个数 
 u16 SendKey;  //要发送数据的校验值   
 Address=(u16)(*(pointer_1+2))*256+(*(pointer_1+3));//得到寄存器地址  	
 Data=(u16)(*(pointer_1+4))*256+(*(pointer_1+5));    //得到具体的数据
 //把寄存器个数合并    
	 if(len_r==8)   //如果接收到的有效字节数是 8
	 { 
		  if(Address<100) //只要地址小于100就是合法地址 
		  { 
			*pointer_2 = *pointer_1;           //第 0个字节是ID号                
            *(pointer_2+1) = 0x01;             //第 1个字节 01 表示成功		     	 				 	  
						HoldReg[Address]  = Data;          //向寄存器里面写数据              
				    *len_s = 4;      		
				    SendKey=CRC16(pointer_2,2);        //开始校验的地址，计算的字节数    		
				    //将计算出来的校验码装入输出数据缓存中 		
				    *(pointer_2 + (*len_s)-2)=(u8)(SendKey>>8); 		
				    *(pointer_2 + (*len_s)-1 )=(u8)(SendKey&0x00FF);
		  } 
		  else
		{
			* ( pointer_2+1 ) =* ( pointer_1+1 );
			ErrorHandle ( 3,pointer_2,len_s );
		}
	 } 	
	else 
	{ 
		*len_s = 0;
	  return ;	
	} 
}

//01 07 01 02 03 04 CRCH CRCL
void WritHoldingReg7(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s)  //07功能码
{	
 u8 i = 0;
 u16 Address=0;
 u16 Address1=0;
 u16 Data=0;     
 //u16 Num=0; 
 u16 SendKey; 
 u16 Num_Reg;
 uint16_t outgpio = 0x00;
 	
 Address=(u16)(*(pointer_1+2));   //起始地址 	
 Num_Reg = (u16)(*(pointer_1+3)); //继电器个数
 Data=(u16)(*(pointer_1+4))*256+(*(pointer_1+5)); //具体的数据
	 if(len_r==8)    
	 { 
		  if(Address<100)  
		  {     
      *pointer_2 = *pointer_1;           //第 1个字节是ID号                
      *(pointer_2+1) = 0x01;             //第 2个字节 01 表示成功	
      *len_s = 4;                        //发送4个字节      	   			
			//Data1 = Data>> Num_Reg;          //把要发送有用数据取出来 右移 num——reg次			   
			for(i = 0; i< Num_Reg ;i++)        //
			{   
				 
					if(Data&0x0001)  //先看最低位 如果是 1 就写1
					{
						outgpio |= (1<<(u16)(Address+i)); //
					}                                          
					else             //如果是0 就写 0
					{
						Address1 = 1<<(Address+i);   
						outgpio &= (~Address1); 
							
					}
					Data >>=1;
		   }	
        			
				SendKey=CRC16(pointer_2,2);        //开始校验的地址，计算的字节数    						    
				*(pointer_2 + (*len_s)-2)=(u8)(SendKey>>8);      //将计算出来的校验码装入输出数据缓存中 				
				*(pointer_2 + (*len_s)-1 )=(u8)(SendKey&0x00FF);  

				 HoldReg[0] = outgpio;		 //第0个字节表示继电器状态 
			 
				 set_relay_current_state(outgpio); 	 
				 enable_relay_out(0x01); 
		   } 
			else
			{
			
				* ( pointer_2+1 ) =* ( pointer_1+1 );
				ErrorHandle ( 3,pointer_2 ,len_s);
			}
    					
	  }
	else 
	{ 
		*len_s = 0;
	  return ;	
	} 

}

//   0x08 
//    01 08 f1 f2 f3 f4 CRCH CRCL
void WritHoldingReg8(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s)   //08风机盘管空调控制指令  
{
    u8 i =0;	
    u16 SendKey;  	
	 if(len_r==8)  //如果是八个字节 表示指令对   
	 {     
      *pointer_2 = *pointer_1;           //第 1个字节是ID号                
      *(pointer_2+1) = 0x01;             //第 2个字节 01 表示成功	
      *len_s = 4;                        //发送4个字节       		 
     for(i = 0; i<4 ; i++)
		 {
		  FJPG_HW_TAB[i] = (u8)(*(pointer_1+2+i));   //把数据给了缓存。
		 }
		    FJPG_HW_Send_Bit  = 0x02;
		 	  SendKey=CRC16(pointer_2,2);        //开始校验的地址，计算的字节数    						    
				*(pointer_2 + (*len_s)-2)=(u8)(SendKey>>8);      //将计算出来的校验码装入输出数据缓存中 				
				*(pointer_2 + (*len_s)-1 )=(u8)(SendKey&0x00FF);  
	 }
	else 
	{ 
		*len_s = 0;
	  return ;
	} 
	
	
	
	
} 			
//  0x09  9 + 1 +1 +2
void WritHoldingReg9(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s)  //09格力空调控制指令 
{
    u8 i =0;	
    u16 SendKey;  	
	 if(len_r==13)  //如果是9个字节 表示指令对   
	 {     
      *pointer_2 = *pointer_1;           //第 1个字节是ID号                
      *(pointer_2+1) = 0x01;             //第 2个字节 01 表示成功	
      *len_s = 4;                        //发送4个字节       		 
     for(i = 0; i<9 ; i++)
		 {
		    GL_HW_TAB[i] = (u8)(*(pointer_1+2+i));   //把数据给了缓存。
		 }
		    GL_HW_Send_Bit  = 0x02;
		 	  SendKey=CRC16(pointer_2,2);        //开始校验的地址，计算的字节数    						    
				*(pointer_2 + (*len_s)-2)=(u8)(SendKey>>8);      //将计算出来的校验码装入输出数据缓存中 				
				*(pointer_2 + (*len_s)-1 )=(u8)(SendKey&0x00FF);  
	 }
	else 
	{ 
		*len_s = 0;
	  return ;
	} 
	
}
 
//0x0A  14 + 1 + 1 + 2  

void WritHoldingRegA(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s)  //海尔空调控制指令  
{
    u8 i =0;	
    u16 SendKey;  	
	 if(len_r==18)  //如果是14个字节 表示指令对   
	 {     
      *pointer_2 = *pointer_1;           //第 1个字节是ID号                
      *(pointer_2+1) = 0x01;             //第 2个字节 01 表示成功	
      *len_s = 4;                        //发送4个字节       		 
     for(i = 0; i<14 ; i++)
		 {
		  Haier_HW_TAB[i] = (u8)(*(pointer_1+2+i));   //把数据给了缓存。
		 }
		    Haier_HW_Send_Bit  = 0x02;
		 	  SendKey=CRC16(pointer_2,2);        //开始校验的地址，计算的字节数    						    
				*(pointer_2 + (*len_s)-2)=(u8)(SendKey>>8);      //将计算出来的校验码装入输出数据缓存中 				
				*(pointer_2 + (*len_s)-1 )=(u8)(SendKey&0x00FF);  
	 }
	else 
	{ 
		*len_s = 0;
	  return ;
	} 
	
	
	
}			            


void WritHoldingRegB ( u8* pointer_1,u8* pointer_2 ,u8 len_r,u8* len_s) //0B功能码
{
	u8 i =0;
	u16 SendKey;
	if (  ( len_r ) ==22 ) //指令字段长度22字节
	{
	      *pointer_2 = *pointer_1;           //第 1个字节是ID号                
      *(pointer_2+1) = 0x01;             //第 2个字节 01 表示成功	
      
		*len_s = 0x04;			   //回复帧字节长度
		SendKey=CRC16 ( pointer_2,2 ); //校验
		* ( pointer_2+2 ) = ( u8 ) ( SendKey>>8 );
		* ( pointer_2+3 ) = ( u8 ) ( SendKey&0x00FF );
		
		for ( i = 0; i<18 ; i++ )
		{
			Glx_HW_TAB[i] = ( u8 ) ( * ( pointer_1+2+i ) ); //将数据缓存
		}
		GLx_HW_Send_Bit  = 0x02;
	}
	else
	{
		  *len_s = 0;
		return ;

	}

}

/**
*  01 0C A A' B B' C C' A A' B B' C C' crc
*****/
void WritHoldingRegC ( u8* pointer_1,u8* pointer_2 ,u8 len_r,u8* len_s) //0C功能码
{
	u8 i =0;
	u16 crcvalue = 0x00;
	u16 SendKey;

	//计算crc
	crcvalue = CRC16(pointer_1,len_r-0x02);
	if (crcvalue == (pointer_1[len_r -0x01] |(pointer_1[len_r -0x02] << 0x08))) 
	{
		*pointer_2 = *pointer_1;		   //第 1个字节是ID号 			   
	    *(pointer_2+1) = 0x01;			  //第 2个字节 01 表示成功	 
	   
		 *len_s = 0x04; 			//回复帧字节长度
		 SendKey=CRC16 ( pointer_2,2 ); //校验
		 * ( pointer_2+2 ) = ( u8 ) ( SendKey>>8 );
		 * ( pointer_2+3 ) = ( u8 ) ( SendKey&0x00FF );
		 
		 for ( i = 0; i<len_r-0x04 ; i++ )
		 {
			 Md_HW_TAB[i] = ( u8 ) (*(pointer_1+2+i )); //将数据缓存
		 }

		switch(len_r-0x04)
		{
			case 0x06:
				Md_HW_Send_Type = MIDEA_WIND_DIRECTION_SWING;
				break;
			case 0x0C:
				Md_HW_Send_Type = MIDEA_BOOTUP;
				break;
			case 0x12:
				Md_HW_Send_Type = MIDEA_SHUTDOWN;
				break;

			default:
				*len_s = 0x00;
				return;
		}		
		Md_HW_Send_Bit	= 0x02;
	}
	else
	{
		  *len_s = 0;
		return ;
	}

}

/**
*心跳:每分钟接收一次
*
*/
void ReadHeartBeat( u8* pointer_1,u8* pointer_2 ,u8 len_r,u8* len_s)//16功能码
{
	u8 i =0;
	u16 SendKey;
	if (  ( len_r ) ==8 ) //指令长度
	{
			*pointer_2 = *pointer_1;		   //第 1个字节是ID号 			   
		*(pointer_2+1) = 0x01;			   //第 2个字节 01 表示成功   
		
		  *len_s = 0x04;			 //回复帧字节长度

		SendKey=CRC16 ( pointer_2,2 ); //检验
		* ( pointer_2+2 ) = ( u8 ) ( SendKey>>8 );
		* ( pointer_2+3 ) = ( u8 ) ( SendKey&0x00FF );

	}
	else
	{
		  *len_s = 0;
		return ;

	}

}

void  ErrorHandle ( u8 Mode,u8* Pointer ,u8 *SendNum)
{
	u16 SendKey;//要发送数据的校验值
	
	switch ( Mode )
	{
		case 1:
			* ( Pointer+2 ) =0x01; //错误功能码
			break;
		case 2:
			* ( Pointer+2 ) =0x02; //错误地址
			break;
		case 3:
			* ( Pointer+2 ) =0x03; //错误数据
			break;
		case 6:
			* ( Pointer+2 ) =0x06; //从设备忙
			break;
	}
	
	*SendNum=0x05;//输出寄存器有效数据个数
	* ( Pointer+1 ) |=0x80; //功能码最高位置一
	
	//写入校验码
	SendKey=CRC16 ( Pointer,*SendNum-2 );
	
	//将计算出来的校验码装入输出数据缓存中
	* ( Pointer+ ( *SendNum-2 ) ) = ( u8 ) ( SendKey>>8 );
	* ( Pointer+ ( *SendNum  - 1) ) = ( u8 ) ( SendKey&0x00FF );

}

/***********
*
*接收帧处理 
*
*	pointer_in[0]:接收到的数据长度
*	pointer_out[0]:要发送的数据长度
*
*	指令接收状态 : 校验正确 返回响应
*					校验异常 返回指令异常响应
*****************************/
uint8_t MessageHandle ( u8* pointer_in,u8* pointer_out,u8 rev_num,u8 * send_num)
{
	u16 CalKey;
	u16 RcvKey;

	u8 ret = 0x00;

	if ((rev_num == 0x00) && (rev_num < 0x04))
	{
		return (ret = 0x01);
	}

	RcvKey= ( u16 ) * ( pointer_in+ ( rev_num-2 ) );
	RcvKey=RcvKey<<8;
	RcvKey=RcvKey| ( u16 ) * ( pointer_in+ ( rev_num-1 ) );
	CalKey=CRC16 ( pointer_in,rev_num-2 );
	if ( CalKey==RcvKey )
	{
		switch ( * ( pointer_in+1 ) )
		{
			case 0x03:
				ReadHoldingReg ( pointer_in,pointer_out,rev_num,send_num);
				break;
			case 0x04:
				ReadIntputReg  ( pointer_in,pointer_out,rev_num,send_num);
				break;
			case 0x06:
				WritHoldingReg  ( pointer_in,pointer_out,rev_num,send_num);
				break;
			case 0x07:
				WritHoldingReg7  ( pointer_in,pointer_out,rev_num,send_num);
				break;
			case 0x08:
				WritHoldingReg8  ( pointer_in,pointer_out,rev_num,send_num);
				break;
			case 0x09:
				WritHoldingReg9  ( pointer_in,pointer_out ,rev_num,send_num);
				break;
			case 0x0A:
				WritHoldingRegA  ( pointer_in,pointer_out,rev_num,send_num);
				break;
			case 0x0B:
				WritHoldingRegB  ( pointer_in,pointer_out,rev_num,send_num);
				break;
			case 0x0C:
				WritHoldingRegC  ( pointer_in,pointer_out,rev_num,send_num);
				break;
			case 0x16:
				ReadHeartBeat 	( pointer_in,pointer_out,rev_num,send_num);
				break;
			default:
			{
				* ( pointer_out+1 ) =* ( pointer_in+1 );
				ErrorHandle ( 1,pointer_out, send_num);
			}
			break;

		}

	}
	else
	{
		ret = 0x01;
	}

	return ret;
}




