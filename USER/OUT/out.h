#ifndef    __OUT_H__
#define    __OUT_H__
#include "stm32f10x.h"
//IO口操作宏定义
#define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 

//IO口地址映射
#define GPIOA_ODR_Addr    (GPIOA_BASE+12) //0x4001080C 
#define GPIOB_ODR_Addr    (GPIOB_BASE+12) //0x40010C0C 
#define GPIOC_ODR_Addr    (GPIOC_BASE+12) //0x4001100C 
#define GPIOD_ODR_Addr    (GPIOD_BASE+12) //0x4001140C  
#define GPIOE_ODR_Addr    (GPIOE_BASE+12) //0x4001180C 
#define GPIOF_ODR_Addr    (GPIOF_BASE+12) //0x40011A0C    
#define GPIOG_ODR_Addr    (GPIOG_BASE+12) //0x40011E0C    

#define GPIOA_IDR_Addr    (GPIOA_BASE+8) //0x40010808 
#define GPIOB_IDR_Addr    (GPIOB_BASE+8) //0x40010C08 
#define GPIOC_IDR_Addr    (GPIOC_BASE+8) //0x40011008 
#define GPIOD_IDR_Addr    (GPIOD_BASE+8) //0x40011408 
#define GPIOE_IDR_Addr    (GPIOE_BASE+8) //0x40011808 
#define GPIOF_IDR_Addr    (GPIOF_BASE+8) //0x40011A08 
#define GPIOG_IDR_Addr    (GPIOG_BASE+8) //0x40011E08 
 
//IO口操作,只对单一的IO口!
//确保n的值小于16!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出 
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出 
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入 

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出 
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入 

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出 
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //输出 
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //输入

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入

///////////////////////作为输出的接口////////////////////////////////
#define OUT0 PCout(0)   // 0    
#define OUT1 PCout(1)   // 1     
#define OUT2 PCout(2)   // 2     
#define OUT3 PCout(3)   // 3    
#define OUT4 PAout(1)   // 4     
#define OUT5 //PAout(1)   // 5    
#define OUT6 //PCout(5)  // 6     
#define OUT7 //PAout(5)  // 7    
#define OUT8 //PAout(7)  // 8    
#define OUT9 //PCout(4)  // 9    
#define OUT10 //PGout(0) // 10 
#define OUT11 //PGout(1) // 11 
#define OUT12 //PFout(13)//12  
#define OUT13 //PAout(4) // 13   
#define OUT14 //PAout(6) // 14      
#define OUT15 //PDout(9) // 15  

////输入

#define IN1  PCin(8)  
#define IN2  PCin(9)  
#define IN3  PBin(11)  

///////输出
#define OUT1A PBout(13)    //     
#define OUT1B PBout(12)    //  

#define OUT2A PBout(15)   //     
#define OUT2B PBout(14)   // 

#define OUT3A PCout(7)   //     
#define OUT3B PCout(6)   // 

///////////////////////////作为输入的接口///////////////////////////////////
////#define IN0  PCin(12)   //0   ---C12  --雷达物理报警 1
////#define IN1  PDin(1)   	//1   ---D1   --雷达物理报警 2
////#define IN2  PDin(0)   	//2   ---D0   --红灯反馈 1 
////#define IN3  PDin(2)   	//3   ---D2   --红灯反馈 2
////#define IN4  PDin(3)   	//4   ---D3   --中断开关
////#define IN5  PDin(4)   	//5   ---D4   --VPR强制报警
////#define IN6  PDin(5)   	//6   ---D5   --市电信号
////#define IN7  PDin(6)   	//7   ---XX   --安全回路 1 
////#define IN8  PDin(7)   	//8   ---XX   --服务器电源 
////#define IN9  PGin(9)   	//9   ----    --安全回路 2 
//#define IN10  1//PGin(10)    //10---XX
//#define IN11  1//PGin(11)    //11---XX
//#define IN12  1//PGin(12)    //12---XX
//#define IN13  1//PGin(13)    //13---XX
//#define IN14  1//PGin(14)    //14---XX
//#define IN15  1//PGin(15)    //15---XX

/*延时 * 200ms */
#define RELAY_DELAY_RECOVER_TIME 10 //延时10s关闭

/*输出继电器*/
typedef struct
{
	/*继电器是否发生变化：是否需要向外输出*/
	uint8_t enablerelayout;

	/*继电器输出状态*/
	uint16_t relaycurrentstate;
	
	/*继电器恢复 从状态发生变化 到 恢复到原始状态的等待时间*/
	uint32_t relayrecoverwaitcnt;
	
}RELAY_CTRL_STRUCT;

extern RELAY_CTRL_STRUCT RelayCtrlStru;

void GPIO_IN_Config(void);
void GPIO_OUT_Config(void); 
void GPIO_OUT_C_Config(void); //磁保持继电器控制引脚
 
extern void set_relay_current_state(uint16_t currentstate);
extern void enable_relay_out(uint8_t state);

extern void relay_ctrl_handle(void);
#endif


