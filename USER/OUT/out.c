#include "out.h"

u16 outgpio;
u16 Jdq_Count = 0;

RELAY_CTRL_STRUCT RelayCtrlStru = {0x00};

void GPIO_IN_Config(void)
{
	
}

void GPIO_OUT_Config(void)
{
 GPIO_InitTypeDef  GPIO_InitStructure;	 
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOC, ENABLE); 
 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3;     
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;		 //IO口速度为10MHz
 GPIO_Init(GPIOC, &GPIO_InitStructure);					 //根据设定参数初始   
	
 GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_1;
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;		 //IO口速度为50MHz
 GPIO_Init(GPIOA, &GPIO_InitStructure);					  
}

void GPIO_OUT_C_Config(void)  //磁保持继电器控制引脚
{
 GPIO_InitTypeDef  GPIO_InitStructure;	 
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOC, ENABLE); 
 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7|GPIO_Pin_6;    
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;		 //IO口速度为10MHz
 GPIO_Init(GPIOC, &GPIO_InitStructure);					 //根据设定参数初始   
	
 GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;		 //IO口速度为50MHz
 GPIO_Init(GPIOB, &GPIO_InitStructure);					  

}

void OUT_IO(void)
{	
  						                      
  if((outgpio) & 0x0001) {OUT1A = 0;OUT1B = 1; } else {OUT1A = 1;OUT1B = 0; }	  //第一路继电器 
  if((outgpio) & 0x0002) {OUT2A = 0;OUT2B = 1; } else {OUT2A = 1;OUT2B = 0; }   //第二路继电器
  if((outgpio) & 0x0004) {OUT3A = 0;OUT3B = 1; } else {OUT3A = 1;OUT3B = 0; }	  //第三路继电器
  Jdq_Count = 0; 
}
void OUTC(void)  // 延时关闭引脚
{
	Jdq_Count++;
 if(Jdq_Count>20000)
 {
   Jdq_Count = 0; 
	 OUT1A = 1;
	 OUT1B = 1;
	 OUT2A = 1;
	 OUT2B = 1;
	 OUT3A = 1;
	 OUT3B = 1;
 }
}


void out_io(void)
{	
  						                      
  if((RelayCtrlStru.relaycurrentstate) & 0x0001) {OUT1A = 0;OUT1B = 1; } else {OUT1A = 1;OUT1B = 0; }	  //第一路继电器 
  if((RelayCtrlStru.relaycurrentstate) & 0x0002) {OUT2A = 0;OUT2B = 1; } else {OUT2A = 1;OUT2B = 0; }   //第二路继电器
  if((RelayCtrlStru.relaycurrentstate) & 0x0004) {OUT3A = 0;OUT3B = 1; } else {OUT3A = 1;OUT3B = 0; }	  //第三路继电器
  
  RelayCtrlStru.relayrecoverwaitcnt = 0; 
}
void outc(void)  // 延时关闭引脚
{
	 if((RelayCtrlStru.relayrecoverwaitcnt ++) > RELAY_DELAY_RECOVER_TIME)
	 {
	 
	  	 RelayCtrlStru.relayrecoverwaitcnt = 0; 

		 /*将H桥控制引脚置为高阻态 ，继电器线圈处于断路状态*/
		 OUT1A = 1;
		 OUT1B = 1;
		 OUT2A = 1;
		 OUT2B = 1;
		 OUT3A = 1;
		 OUT3B = 1;
	 }
}


void set_relay_current_state(uint16_t currentstate)
{
	RelayCtrlStru.relaycurrentstate = currentstate;
	return ;
}


void enable_relay_out(uint8_t state)
{
	RelayCtrlStru.enablerelayout = state;
	return ;
}

/**************************************************************************************
*
*继电器控制
*
*	继电器从输出状态到 复位状态
*	磁保持继电器
*
*
*****************************************************************************************/
void relay_ctrl_handle(void)
{
	/*继电器控制输出*/
	if (RelayCtrlStru.enablerelayout == 0x01)
	{
		out_io();
		RelayCtrlStru.enablerelayout = 0x00;
	}

	/*继电器控制引脚复位*/	
	outc(); 	
}


















