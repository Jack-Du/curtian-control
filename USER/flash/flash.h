#ifndef __FLASH_H
#define __FLASH_H 			   
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "stm32f10x_flash.h"
#include "config.h"
/**************************************************************************/
/***
*
*f103RBT6 : 128k rom ,20k ram 内部flash每页1024字节
*f103RCT6 : 256k rom ,48k ram 内部flash每页2048字节
*f103RET6 : 512k rom ,64k ram 内部flash每页2048字节
**
*
**	stm32 <64k 低密度
**	      64k < 128k 中密度
**        >128k 高密度
*/
/**************************************************************************/




#define FLASH_BASE_ADDR   ((u32)0x08000000)
#define FLASH_BOOT_SIZE  		0x5000
#define StartAddr         ((u32)0x08000000)

#define	PageUsedFlashNum		2048		//1个页使用的字节数	，请使用偶数个字节，写的时候是2字节2字节的写入的		
#define	FLASH_PAGE_SIZE		2048	 //一页2字节 0x800

#ifdef BOARD_MCU_STM32F103RB

//#define EndAddr    				((u32)0x0801FFFF) 

/*存储区页起始地址 ：最后20k*/
//#define STORE_START_PAGE_ADDR  ((u32)0x0801B000) 
						
#elif defined BOARD_MCU_STM32F103RC
#define EndAddr    				((u32)0x0803FFFF) 

/*存储区页起始地址 ：最后20k*/
#define STORE_START_PAGE_ADDR  ((u32)0x0803B000) 

#elif defined BOARD_MCU_STM32F103RE

#define EndAddr    				((u32)0x0807FFFF) 

/*存储区页起始地址 ：最后20k*/
#define STORE_START_PAGE_ADDR  ((u32)0x0807B000) 

#endif

/********************************************************页信息**********************************************/
/*网络参数存储区起始地址*/
#define DEVICE_NETINFO_STORAGE_PAGE (STORE_START_PAGE_ADDR + 0x00)
#define DEVICE_NETINFO_STORAGE_PAGE_OFFSET (FLASH_PAGE_SIZE)

/*系统运行参数*/
#define SYSTEM_RUN_PARAM_STORAGE_PAGE (DEVICE_NETINFO_STORAGE_PAGE + DEVICE_NETINFO_STORAGE_PAGE_OFFSET)
#define SYSTEM_RUN_PARAM_STORAGE_PAGE_OFFSET (FLASH_PAGE_SIZE)

/**********************************************************************************************************/

typedef union Flash_Date
{
	uint32_t  word;
	uint16_t  halfword[2];
	uint8_t   byte[4];
} Flash_Date;

extern u8 FlashTemp[PageUsedFlashNum];
extern uint8_t g_Flashmux;

/**
函数描述：flash控制器初始化
*/
void FlashInit(void);

/*
函数描述:不带擦除，写一串u8数据
*/
FLASH_Status FlashWritNWithoutErasing(u32 Startaddr, u8 *date,u32 len);

/**
函数描述：带擦除,写一串u8数据	,不能跨页
*/
FLASH_Status FlashWritNWithErasing(u32 Startaddrs, u8 *date, u32 len);
/**
函数描述：从地址内读一个字
*/
uint32_t FlashReadWord(uint32_t Startaddr);

/**
函数描述：向地址内写入一个字
*/
void FlashWriteWord(uint32_t Startaddr,uint32_t value);

/**
函数描述：读n个字节
*/
void FlashReadNByte(u32 addr, u8 *date,u16 len);

/**
函数描述：向目标地址Desaddr写入len个字节，
*/
void FlashWriteData(uint32_t Desaddr, uint32_t SrcAddr, uint32_t len);

/*
函数描述:从起始地址Startaddr读flash内部len个字节
*/
void FlashReadNByte(uint32_t Startaddr, uint8_t *date,uint16_t len);
#endif



