/*********************************************************************************************
flash操作注意事项：
1.对flash写操作时，一定要先对flash擦除 stm32vc 1页2k字节
0x14-0x63	子模块20-99（1为启用，0为停用）
0x64-0x67	32个开关使能状态（1字节8位代表8个开关使能状态，1为开启，0为关闭）
0x68-0x6c	使用的模块id + 版本号

****************************************************************************		
开关运行参数配置



*********************************************************************************************/
#include "flash.h"
#include "os.h"
#include "stm32f10x_flash.h"

//支持UCOSIII
#ifdef 	CPU_CFG_CRITICAL_METHOD					//CPU_CFG_CRITICAL_METHOD定义了,说明要支持UCOSIII	
#define os_running		OSRunning			//OS是否运行标记,0,不运行;1,在运行
#endif


/*20151112 modify by jsw begin*/
#define	GerPageStartAddr(x)		((x)&(~0x000007FF))	   //获取x地址所在的页地址
#define GetPageOffset(x)      ((x)&0x000007FF) 

FLASH_Status FLASHStatus;

u8 FlashTemp[PageUsedFlashNum];
uint8_t g_Flashmux = 0;

/*
Flash初始化
*/
void FlashInit(void)
{
	//总线初始化
	 RCC_AHBPeriphClockCmd(RCC_AHBPeriph_FSMC,ENABLE);

//FLASH设备初始化
	FLASH_Unlock();
	FLASH_SetLatency(FLASH_Latency_2);
	FLASH_HalfCycleAccessCmd(FLASH_HalfCycleAccess_Disable);
	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
	FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR); //清除待处理标志位	
	//Flash枷锁
  	FLASH_Lock();
	g_Flashmux	= 0;
}

/*
	擦除falsh
	Startaddr: 	擦除的页地址 (页地址为该页内的任何一个地址)
	len:		擦除的页数
*/
FLASH_Status ErasePageN(u32 Startaddr,u16 len)
{
	u16	i=0;
	
	if(g_Flashmux	== 1)
	{
		return FLASH_ERROR_WRP;
	}
	g_Flashmux	= 1;
	//Flash解锁
	FLASH_Unlock();
	
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR); 
	
	FLASHStatus = FLASH_COMPLETE;
	if((Startaddr >= StartAddr) && ((Startaddr+len*FLASH_PAGE_SIZE) <= EndAddr))
	{
		for(i = 0; (i <= len - 1) && (FLASHStatus  == FLASH_COMPLETE); i++) 
	     { 
	      FLASHStatus = FLASH_ErasePage(Startaddr + (FLASH_PAGE_SIZE * i)); 
	     }  
	}
	//Flash枷锁
  	FLASH_Lock();
	g_Flashmux	= 0;
	return FLASHStatus;
}


/*
	不带擦除，写一串u8数据
*/
FLASH_Status FlashWritNWithoutErasing(u32 Startaddr, u8 *date,u32 len)
{  	
	
	u32 Address;
	u32	ENDAddres;
	u16	DataTemp;
	FLASHStatus = FLASH_COMPLETE;
  if(g_Flashmux	== 1)
	{
		return FLASH_ERROR_WRP;
	}
	g_Flashmux	= 1;
	//Flash解锁
	FLASH_Unlock();

	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR); 
	

	Address			=	Startaddr;
	ENDAddres		=	Address	+ len ;

	while((Address < ENDAddres) && (FLASHStatus == FLASH_COMPLETE))
	{
		DataTemp		= date[Address - Startaddr+1]*0x100+ date[Address - Startaddr]; //将两u8组装成u16
		FLASHStatus		= FLASH_ProgramHalfWord(Address, DataTemp);
		Address			= Address + 2;
	}
  //Flash枷锁
  FLASH_Lock();
	g_Flashmux	= 0;
	
	return FLASHStatus;
}

uint32_t FlashReadWord(uint32_t Startaddr)
{
	uint32_t data;
	uint32_t *p = (uint32_t*)(Startaddr);
	data = *p;
 return data;		
}

void FlashWriteWord(uint32_t Startaddr,uint32_t value)
{
	uint32_t Address;
	uint32_t	ENDAddres;
	uint32_t	PageAddres;
	Flash_Date date;
	
	FLASHStatus 	= 	FLASH_COMPLETE;
	Address			= 	Startaddr;
	ENDAddres		=	Address	+ 4 ;
	PageAddres		=	GerPageStartAddr(Address);
	
	date.word = value; //解决写FLASH失败的问题

#if SYSTEM_SUPPORT_OS == 0x01
	if(os_running==1)	
	{
		delay_osschedlock();						//阻止OS调度
	}
	__disable_irq();
#endif

	//Flash解锁
	FLASH_Unlock();
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR); 
	
	FlashReadNByte(PageAddres, FlashTemp,PageUsedFlashNum);		//读取改页有用数据
	
	while((Address < ENDAddres) && (FLASHStatus == FLASH_COMPLETE))
	{
			FlashTemp[Address-PageAddres]		= date.byte[Address-Startaddr];
			Address			= Address + 1;
	}
	FLASH_ErasePage(PageAddres);		//擦除该页数据
	FlashWritNWithoutErasing(PageAddres,FlashTemp,PageUsedFlashNum);  //写该页数据

//Flash枷锁
 FLASH_Lock();

#if SYSTEM_SUPPORT_OS == 0x01
	 if(os_running==1)  
	 {
		 delay_osschedunlock(); 					 //恢复OS调度			 
	 }
	 __enable_irq();
#endif

}
/**
函数描述：带擦除,写一串u8数据	,不能跨页
*/
FLASH_Status FlashWritNWithErasing(u32 Startaddrs, u8 *date, u32 len)
{
	u32 Address;
	u32	ENDAddres;
	u32	PageAddres;

	FLASHStatus 	= 	FLASH_COMPLETE;
	Address			= 	Startaddrs;
	ENDAddres		=	Address	+ len ;
	PageAddres		=	GerPageStartAddr(Address);
	
	//判断是否跨页操作
	if((GetPageOffset(Address) + len > PageUsedFlashNum) || len == 0)
	{
		return FLASH_ERROR_PG;
	}
	if((Startaddrs >= StartAddr) && ((Startaddrs+len) <= EndAddr))
	{
		FlashReadNByte(PageAddres, FlashTemp,PageUsedFlashNum);		//读取改页有用数据
	
		while((Address < ENDAddres) && (FLASHStatus == FLASH_COMPLETE))
		{
			FlashTemp[Address-PageAddres]		= date[Address-Startaddrs];
			Address			= Address + 1;
		}
		ErasePageN(PageAddres,1);		//擦除该页数据
		FlashWritNWithoutErasing(PageAddres,FlashTemp,PageUsedFlashNum);  //写该页数据
	}

	return FLASHStatus;	
}



/**
函数描述：向目标地址Desaddr写入len个字节，
*/
void FlashWriteData(uint32_t Desaddr, uint32_t SrcAddr, uint32_t len)
{
	  uint16_t i = 0;
		uint16_t PageNum = 0;
		uint16_t PageRemainByteNum = 0;
		uint16_t PageOffset = 0;
		uint32_t RemainLen = 0;
		
		uint32_t Address = 0;

		Address = Desaddr;
		RemainLen = len;
    	//写首页的后半部分

#if SYSTEM_SUPPORT_OS == 0x01
		if(os_running==1)	
		{
			delay_osschedlock();						//阻止OS调度
		}
		__disable_irq();
#endif

		
		PageOffset = GetPageOffset(Address);
		PageRemainByteNum = (uint16_t)(PageUsedFlashNum - PageOffset);
		
		if(PageRemainByteNum > RemainLen)
		{
				PageRemainByteNum = RemainLen;
		}
		
		if(PageRemainByteNum != 0)
		{
			FlashWritNWithErasing(Address, (uint8_t*)SrcAddr, PageRemainByteNum);
		
			Address += PageRemainByteNum;
			SrcAddr += PageRemainByteNum;
			RemainLen -= PageRemainByteNum; 
		}			
		
		//写中间N多页
		PageNum = RemainLen / PageUsedFlashNum;
		PageRemainByteNum = RemainLen % PageUsedFlashNum;
		
		if(PageNum != 0)
		{
			for(i = 0; i < PageNum; i++)
			{
					FlashWritNWithErasing(Address, (uint8_t*)SrcAddr, PageUsedFlashNum);
					Address += PageUsedFlashNum;
					SrcAddr += PageUsedFlashNum;
					RemainLen -= PageUsedFlashNum; 		
			}
		}
		//写末页的前部分
		
		if(PageRemainByteNum != 0)
		{
			FlashWritNWithErasing(Address, (uint8_t*)SrcAddr, PageRemainByteNum);
		}

#if SYSTEM_SUPPORT_OS == 0x01
		 if(os_running==1)  
		 {
			 delay_osschedunlock(); 					 //恢复OS调度			 
		 }
		__enable_irq();
#endif

		return ;	
}

/*
函数描述:从起始地址Startaddr读flash内部len个字节
*/
void FlashReadNByte(uint32_t Startaddr, uint8_t *date,uint16_t len)
{
	uint32_t	i;
	if((Startaddr >= StartAddr) && ((Startaddr+len) <= EndAddr))
	{
		for(i=0;i<len;i++)
		{
		 	date[i] = *(volatile uint8_t *) Startaddr++;
		}
	}
}


/****
*
*
*	flash 写入时关中断、禁止任务调度	
*
*
*
*
*
************/











