#include <stdio.h>
#include <string.h>

#include "system_start_info.h"
#include "w5500_net_handle.h"
#include "stm32f10x.h"


SYSTEM_START_INFO_STRUCT SystemStartInfo = {0X00};

#if SYSTEM_START_INFO_CHECK_ENABLE == 0x01

void set_system_start_info(void);
/*****
*
*定时器初始化 ;
*	记录系统运行时间
*		当系统正常运行时间超过5min ，清除系统频繁冷启动标记
*		如果系统出现超过连续5次冷启动，并且启动之后运行时间小于5min 则认为是人为的将系统网络参数恢复默认值操作
*
*
*
*********************/

#if ENABLE_HARDWARE_TIMER == 0X01

/********************************************************************
* Function Name  : system_tim_init(u16 arr,u16 psc)
* Function       : tim模式初始化
* parameter      : arr - 自动重装值
				   psc - 时钟预分频数	
* Description    : 频率f = 72M/[(psc+1)*(arr+1)]               
* Return         : void  1/10 =  10   =  (2000 *10 -1) * (36000 - 1)
*********************************************************************/
void system_tim_init(u16 arr,u16 psc)  //产生
{
	/* 初始化结构体定义 */
	TIM_TimeBaseInitTypeDef	TIM_TimeBaseInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	/* 使能相应端口的时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);	  //使能定时器1时钟
	
	/* TIM2 初始化*/
	TIM_TimeBaseInitStructure.TIM_Period = arr;	    //下一个更新事件装入活动的自动重装载寄存器周期的值
	TIM_TimeBaseInitStructure.TIM_Prescaler = psc;	//作为TIMx时钟频率除数的预分频值 
	TIM_TimeBaseInitStructure.TIM_ClockDivision = 0;  //时钟分割:TDTS = Tck_tim
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;	//TIM向上计数模式
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0x00;
	
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseInitStructure);
	TIM_ClearFlag(TIM1, TIM_FLAG_Update);

	/*中断设置*/
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn; //TIM1中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0; //0优先级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3; //从优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ使能
	NVIC_Init(&NVIC_InitStructure); //初始化NVIC
	TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE); 

	/* 使能定时器 */
	TIM_Cmd(TIM1, DISABLE);  //关闭定时器 	 
  
}

/***
*
*启动、关闭定时器操作
*	ops :0 停止定时器    	1 开启定时器
*
*****/
void start_stop_tim(uint8_t ops)
{
	if (ops)
	{
		/* 使能定时器 */
		TIM_Cmd(TIM1, ENABLE);  //关闭定时器	 
	}
	else
	{
		/* 使能定时器 */
		TIM_Cmd(TIM1, DISABLE);  //关闭定时器	 
	}

}

/*****
*
*
*tim1 定时器中断处理函数
*
*
*
*
************/
void TIM1_UP_IRQHandler(void)
{
	if (TIM_GetFlagStatus(TIM1, TIM_FLAG_Update))
	{
		TIM_ClearITPendingBit(TIM1, TIM_FLAG_Update);
		
		SystemStartInfo.timecount ++;
		system_start_printf_string("定时时间到。。。");
	}
}

#else
void system_tim_init(u16 arr,u16 psc)  //产生
{}

void start_stop_tim(uint8_t ops)
{}

#endif
/******
***mcu 软件复位
***
***
**********/
void mcuRestart(void)
{
//　　__set_FAULTMASK(1); //关闭所有中断
//　　NVIC_SystemReset(); //复位
}

/****
*
*初始化结构体
*
*
*
*
*
************************/
//       - RCC_FLAG_PINRST: 引脚复位
//       - RCC_FLAG_PORRST: POR/PDR复位
//       - RCC_FLAG_SFTRST: 软件复位
//       - RCC_FLAG_IWDGRST: 独立看门狗复位
//       - RCC_FLAG_WWDGRST: 窗口看门狗复位
//       - RCC_FLAG_LPWRRST: 低电量复位
void init_system_start_info(void)
{

	memset(&SystemStartInfo,0,sizeof (SYSTEM_START_INFO_STRUCT));
	

	SystemStartInfo.step = SYSTEM_START_HANDLE_INIT;
	SystemStartInfo.timecount = 0x00;


	/*判读当前是否是冷启动*/
	if(RCC_GetFlagStatus(RCC_FLAG_PORRST) != RESET)
    {
       //这是上电复位
       SystemStartInfo.iscoldstart = POWER_ON_RESET_FLAG;
    }
	else if (RCC_GetFlagStatus(RCC_FLAG_WWDGRST)!= RESET)
    {
       //窗口看门狗复位
		SystemStartInfo.iscoldstart = WWDG_RESET_FLAG;
	   	SystemStartInfo.step = SYSTEM_START_HANDLE_FINISH;
    }
	else if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST)!= RESET)
    {
       //窗口看门狗复位
		SystemStartInfo.iscoldstart = IWDG_RESET_FLAG;
	   	SystemStartInfo.step = SYSTEM_START_HANDLE_FINISH;
    }
    else if (RCC_GetFlagStatus(RCC_FLAG_SFTRST) != RESET)
    {
       //软件复位
		SystemStartInfo.iscoldstart = SOFT_RESET_FLAG;
	    SystemStartInfo.step = SYSTEM_START_HANDLE_FINISH;
    }
	else
	{
		SystemStartInfo.iscoldstart = OTHER_RESET_FLAG;
		SystemStartInfo.step = SYSTEM_START_HANDLE_FINISH;
	}
    RCC_ClearFlag();//清除RCC中复位标志

	system_start_printf_string("\nsystem reset mode:");
	system_start_printf_int(SystemStartInfo.iscoldstart);
	system_start_printf_string("\n");

	if (get_system_start_info_recover_net_enable_state() == 0x00)
	{
		SystemStartInfo.iscoldstart = OTHER_RESET_FLAG;
	    SystemStartInfo.step = SYSTEM_START_HANDLE_FINISH;
		
		return ;
	}

	set_system_start_info();
	return;
	
}

/************************************************************************************
*
*设置上电冷启动次数
*
*上电启动过程中，首先确定上电是否是冷启动，如果是冷启动 ，记录冷启动次数
*				 在系统正常运行超过5min钟之后，清除记录的冷启动标记次数
*				
*				 连续冷启动次数超过5次，恢复网络默认参数，同时清除记录的冷启动标记次数
*
************************************************************************************/
void set_system_start_info(void)
{

	/*读取保存的冷启动次数信息*/
	if (SystemStartInfo.iscoldstart == POWER_ON_RESET_FLAG)
	{
		/*读取IP地址*/
		FlashReadNByte(CONTINUES_COLD_START_NUM_ADDR,(uint8_t*)&SystemStartInfo.coldstartnum,sizeof(SystemStartInfo.coldstartnum));

		system_start_printf_string("\n冷启动次数:");
		system_start_printf_int(SystemStartInfo.coldstartnum);
		system_start_printf_string("\n");

		if ((SystemStartInfo.coldstartnum == 0x00) ||(SystemStartInfo.coldstartnum == 0xff))
		{
			SystemStartInfo.coldstartnum = 0x01;
		}

		/*连续冷启动次数超过5次*/
		else if (SystemStartInfo.coldstartnum >= 0x04)
		{
			SystemStartInfo.coldstartnum = 0x00;
			SystemStartInfo.isrecovernetflag = 0x01;

			SystemStartInfo.step = SYSTEM_START_HANDLE_FINISH;
		}
		else
		{
			SystemStartInfo.coldstartnum ++;

		}
	}
	else
	{
		/*清除标记*/
		SystemStartInfo.coldstartnum = 0x00;
	}

	FlashWriteData(CONTINUES_COLD_START_NUM_ADDR,(uint32_t )&SystemStartInfo.coldstartnum,sizeof(SystemStartInfo.coldstartnum));

	return;
}

void clear_system_start_info(void)
{
	SystemStartInfo.coldstartnum = 0x00;
	
	FlashWriteData(CONTINUES_COLD_START_NUM_ADDR,(uint32_t )&SystemStartInfo.coldstartnum,sizeof(SystemStartInfo.coldstartnum));
	return ;
}

/******
*
*功能 :获取定时器是否超时
*
*返回值:	超时 :返回1，否则 0
*
*
**************/
u8 get_system_start_info_timeout_state(void)
{
	if (SystemStartInfo.timecount >= TIM_COUNT_MAX_VALUE)
	{
		return 0x01;
	}

	return 0x00;
}

/******
*
*功能 :获取定时器是否超时
*
*返回值:	超时 :返回1，否则 0
*
*
**************/
u8 get_system_start_info_recover_net_flag(void)
{
	return (SystemStartInfo.isrecovernetflag);
}

void set_system_start_info_recover_net_flag(u8 flag)
{
	(SystemStartInfo.isrecovernetflag = flag);
	return ;
}

u8 get_system_start_info_recover_net_enable_state(void)
{
	FlashReadNByte(LOCAL_HAND_RECOVER_NET_ENABLE_ADDR,(uint8_t*)&SystemStartInfo.recovernetenable,sizeof(SystemStartInfo.recovernetenable));
	if (SystemStartInfo.recovernetenable == 0xff)
	{
		SystemStartInfo.recovernetenable = 0x00;
	}
	
	return (SystemStartInfo.recovernetenable);
}

void set_system_start_info_recover_net_enable_state(u8 state)
{

	(SystemStartInfo.recovernetenable = state);
	FlashWriteData(LOCAL_HAND_RECOVER_NET_ENABLE_ADDR,(uint32_t )&SystemStartInfo.recovernetenable,sizeof(SystemStartInfo.recovernetenable));

	return ;
}



/************************************************************************************
*
*系统启动信息处理
*
*上电启动过程中，首先确定上电是否是冷启动，如果是冷启动 ，记录冷启动次数
*				 在系统正常运行超过5min钟之后，清除记录的冷启动标记次数
*				
*				 连续冷启动次数超过5次，恢复网络默认参数，同时清除记录的冷启动标记次数
*
************************************************************************************/
void system_start_info_handle(void )
{
	switch(SystemStartInfo.step)
	{
		case SYSTEM_START_HANDLE_INIT:

			SystemStartInfo.timecount = 0x00;
			SystemStartInfo.step = SYSTEM_START_HANDLE_RUNNING;

			system_tim_init(35999,59999);

			/*启动定时器*/
			start_stop_tim(0x01);

			break;

		case SYSTEM_START_HANDLE_RUNNING:

#if ENABLE_HARDWARE_TIMER == 0X00		
			SystemStartInfo.timecount ++;
			system_start_printf_string("\n定时时间到，定时器计数增加一\n");
#endif
		
			/*定时时间超过5分钟 ，删除冷启动记录次数*/	
			if (SystemStartInfo.timecount >= TIM_COUNT_MAX_VALUE)
			{
				system_start_printf_string("\n定时时间超过5分钟，关闭定时器。。。。\n");
				
				start_stop_tim(0x00);
				clear_system_start_info();
				
				SystemStartInfo.timecount = 0x00;
				SystemStartInfo.step = SYSTEM_START_HANDLE_FINISH;
			}
			break;

		case SYSTEM_START_HANDLE_FINISH:
			{
			}
			break;

		default:
		break;
	}

	return ;
}

#else

void init_system_start_info(void){}
void set_system_start_info(void){}
void system_start_info_handle(void ){}
u8 get_system_start_info_timeout_state(void)
{
	return 0x01;
}

u8 get_system_start_info_recover_net_flag(void)
{
	return 0x00;
}
void set_system_start_info_recover_net_flag(u8 flag)
{
	return ;
}

u8 get_system_start_info_recover_net_enable_state(void)
{
	return (0x00);
}

void set_system_start_info_recover_net_enable_state(u8 state)
{
	return ;
}

#endif

/*
*
*设置网络参数修改方式
*
*
*
*/
void set_net_modify_model(uint8_t model)
{

	SystemStartInfo.netmodifymodel = model;
	FlashWriteData(LOCAL_NET_MODIFY_FLAG,(uint32_t )&SystemStartInfo.netmodifymodel,sizeof(SystemStartInfo.netmodifymodel));
	
	return ;
}

/*
*
*设置网络参数修改方式
*
*
*
*/
uint8_t get_net_modify_model(void)
{

	FlashReadNByte(LOCAL_NET_MODIFY_FLAG,(uint8_t *)&SystemStartInfo.netmodifymodel,sizeof(SystemStartInfo.netmodifymodel));

	if (SystemStartInfo.netmodifymodel == 0xff)
	{
		SystemStartInfo.netmodifymodel = 0x00;
	}
	else if ((SystemStartInfo.netmodifymodel != NET_NOT_MODIFY) &&(SystemStartInfo.netmodifymodel != NET_NOT_CONFIG) &&(SystemStartInfo.netmodifymodel != NET_RECOVER_BY_HAND) &&(SystemStartInfo.netmodifymodel != NET_REMOTE_CONFIG))
	{
		SystemStartInfo.netmodifymodel = NET_UNDEFINED_MODIFY;
	}

	return SystemStartInfo.netmodifymodel;
}

