#ifndef _SYSTEM_START_INFO_H
#define _SYSTEM_START_INFO_H

#include <stdint.h>
#include "flash.h"
#include "bsp_usart3.h"

/*系统重启之后的状态信息*/

#define SYSTEM_START_DEBUG_ENABLE 0X00
#define ENABLE_HARDWARE_TIMER 0X00

/*定时器计数最大值 10 ，时间5分钟*/
#define TIM_COUNT_MAX_VALUE 0X09

/*上电冷启动次数记录 :超过5次将以太网参数设置成默认值*/
#define CONTINUES_COLD_START_NUM_ADDR 			SYSTEM_RUN_PARAM_STORAGE_PAGE + 0X00

/*板子上电启动模式*/
#define BOARD_START_MODE_ADDR 					SYSTEM_RUN_PARAM_STORAGE_PAGE + 0X04

/*LOADER VERSION :占用2个4字节地址*/
#define BOOT_LOADER_SOFT_VERSION_ADDR          (SYSTEM_RUN_PARAM_STORAGE_PAGE + 0x08)

/*网络参数修改原因标记*/
#define LOCAL_NET_MODIFY_FLAG          (SYSTEM_RUN_PARAM_STORAGE_PAGE + 0x20)


/*启动模式*/
#define  BOOT_MODE           0xA5A5A5A5
#define  FRAMEWARE_MODE      0x55555555 
#define UNDEFINE_MODE        0XFFFFFFFF

#define  RESET_TURE          0x12345678
#define  RESET_FLASE         0xAABBCCDD


/***系统复位标志******/
typedef struct
{
	u32 hard_fault:1; 		//硬件异常
	u32 remote_ctrl_reset:1;//远程重启
	u32 system_update:1;	//系统升级
	u32 recv:29;			//保留
}SYSTEM_RESET_BIT;

typedef union
{
	u32 system_reset_flag;	
	SYSTEM_RESET_BIT system_reset_bit;
}SYSTEM_RESET_INFO_STRUCT;



#if SYSTEM_START_DEBUG_ENABLE == 0x01
	#define system_start_printf_string(a)   usart_printf_string(a)
	#define system_start_printf_int(a)   usart_printf_int(a)
	#define system_start_printf(a,b)   usart_printf(a,b)
#else	
	#define system_start_printf_string(a) 
	#define system_start_printf_int(a) 
	#define system_start_printf(a,b)   
#endif


/*记录上电启动状态*/
typedef struct
{
	/*当前流程*/
	uint8_t step;

	/*是否是上电冷启动*/
	uint8_t iscoldstart;
	
	/*上电连续冷启动次数*/
	uint8_t coldstartnum;
	
	/*是否重置网络参数*/
	uint8_t isrecovernetflag;

	/*定时时间到 标记*/
	uint16_t timecount;

	/*网络参数修改原因*/
	uint8_t netmodifymodel;

	/*手动复位IP地址标记*/
	uint8_t recovernetenable;
	
	SYSTEM_RESET_INFO_STRUCT SystemResetInfo;

}SYSTEM_START_INFO_STRUCT;

/****************************程序运行流程************************/
typedef enum
{
	SYSTEM_START_HANDLE_INIT = 0X00,
	SYSTEM_START_HANDLE_RUNNING = 0X01,	
	SYSTEM_START_HANDLE_FINISH = 0X02,	
}SYSTEM_START_HANDLE_STEPS;

/***************************************复位方式**************************************/
typedef enum
{
	POWER_ON_RESET_FLAG = 0X00,
	WWDG_RESET_FLAG = 0X01,
	IWDG_RESET_FLAG = 0X02,
	SOFT_RESET_FLAG = 0X03,
	OTHER_RESET_FLAG = 0X04,
}SYSTEM_RESET_TYPE;


/*定义 网络参数修改方式
*
*0:默认未修改 1:网络区参数未设置(全部ff) 2：冷启动手动恢复默认 3:远程配置 4:未知异常 5:预留
*
***/
typedef enum
{
	NET_NOT_MODIFY = 0X00,
	NET_NOT_CONFIG = 0X01,
	NET_RECOVER_BY_HAND = 0X02,
	NET_REMOTE_CONFIG = 0X03,
	NET_UNDEFINED_MODIFY = 0X04,
	NET_MODIFY_OTHER 
}STRUCT_NET_MODIFY_REASON;
	

extern SYSTEM_START_INFO_STRUCT SystemStartInfo;


/***************************************定义接口*****************************************************/

extern void init_system_start_info(void);
extern void system_start_info_handle(void );
extern void set_net_modify_model(uint8_t model);
extern u8  get_net_modify_model(void);

extern u8 get_system_start_info_recover_net_flag(void);
extern void set_system_start_info_recover_net_flag(u8 flag);

extern void set_system_start_info_recover_net_enable_state(u8  state);
extern u8  get_system_start_info_recover_net_enable_state(void);


extern u8 get_system_start_info_timeout_state(void);

/***************************************定义接口*****************************************************/

#endif
