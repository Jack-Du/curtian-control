#ifndef __USART_H
#define __USART_H
#include "stdio.h"	
#include "sys.h" 
#include <stdio.h>
#include <stm32f10x.h>

#define  FRAME_HEAD_LEN       4
#define  FRAME_TAIL_LEN       1

#define  FRAME_HEAD_OFFSET	  0
#define  FRAME_TYPE_OFFSET    1
#define  FRAME_DST_OFFSET	    2
#define  FRAME_LEN_OFFSET	    3
#define  FRAME_KEY_OFFSET     4

#define  FRAME_TAIL_OFFSET     1
#define  FRAME_CRC_OFFSET      1

/*********老版协议常数宏定义******************/
#define  FRAME_TYPE_OFFSET_OLD    1

#define  STM32_USART2
#define UART_BUFF_LEN   16

#define  USART_CMD_FINISH_FLAG   0xFE

#define USART_REC_LEN  			200  	//定义最大接收字节数 200
#define EN_USART1_RX 			1		//使能（1）/禁止（0）串口1接收
typedef enum
{
	RECIVE_INIT  = 0,
	RECIVE_START ,
	RECIVEING    ,
	RECIVED  ,
	TRANSMITING,
	TRANMITED      

}UartStatusType;

 typedef struct UartDevice
{
	uint8_t uartnum;
	uint8_t uartflag; 
	uint8_t uartcmdlen;
	uint8_t uartcmdbuf[UART_BUFF_LEN];
}UartDeviceType;



extern uint16_t USART_RX_STA;    
extern uint8_t USART_RX_BUF[USART_REC_LEN];



extern uint8_t Uart2_Get_Flag;
extern uint16_t Uart2_Get_Data ;
extern uint8_t Uart2CmdBuf[UART_BUFF_LEN];  	//接收缓冲,最大64个字节.
//接收到的数据长度
extern uint8_t Uart2CmdLen ; 

extern uint8_t Uart3CmdBuf[UART_BUFF_LEN];
extern uint8_t Uart3CmdLen ;
extern uint8_t Uart3_Get_Flag;
extern uint16_t Uart3_Get_Data ;



extern uint8_t Uart5CmdBuf[UART_BUFF_LEN];
extern uint8_t Uart5CmdLen ;
extern uint8_t Uart5_Get_Flag;
extern uint16_t Uart5_Get_Data ;



void Rcc_Configuration(void);
void UsartGPIO_Configuration(void);

void USART1_Configuration(void);
void Usart1SendByte(uint8_t date );
void Usart1SendData(uint8_t *buf,uint8_t len);
void USART2_Configuration(void);
void Usart2EnableIt(void);
void Usart2DisableIt(void);
uint16_t Usart2ReceiveByte(void);
void Usart2SendByte(uint8_t date );
void Usart2_BuffReset(void);
void Usart2IRQHandler(void);

void USART3_Configuration(void);
/**
函数描述:Uasrt3中断处理函数
*/
void Usart3ReceiveHandle(void);

/**
函数描述：Uasrt3发送len个字节
*/
void Usart3SendData(u8 *buf,u8 len);
/**
函数描述：复位串口3接收缓冲
*/
void Usart3_BuffReset(void);

void USART4_Configuration(void);


/**
函数描述：Uasrt4发送len个字节
*/
void Usart4SendData(u8 *buf,u8 len);

/**
函数描述：复位串口接收缓冲
*/
void Usart4_BuffReset(void);
/**
函数描述:接收数据的状态机
*/
void Usart4_ReceiveBate(uint8_t rx_data);
/**
函数描述:Uasrt4中断处理函数
*/
void UART4ReceiveBateByIT(void);

void USART5_Configuration(void);
/*
函数描述：Uasrt5发送一个字节数据
*/
void Usart5SendByte(uint8_t date );
void Usart5SendData(uint8_t *buf,uint8_t len);
void Usart5ReceiveData(uint8_t *buf,uint8_t *len);
void Usart5_BuffReset(void);
/**
函数描述:Uasrt5中断处理函数
*/
void Usart5ReceiveBateByIT(void);



/*************************兼容老版协议的变量和宏定义**************************************/

/**********视频信号源切换命令宏定义****************************/



/**********音频信号切换命令宏定义*************************/

#define  UsartFrameLenth_H1       8
#define  UsartFrameLenth_L1       9
#define  UsartFrameData1		  10
#define  UsartFrameCrc1           11

void Usart_rx_process(uint8_t rx_data);   


/**
函数描述：USART1配置
*/
void USART1_Configuration(void);

/**
函数描述:Uasrt1接收两个字节数据
*/
uint16_t Usart1_ReceiveByte(void);

/*
函数描述：Uasrt1发送一个字节数据
*/
void Usart1_SendByte(uint8_t date);

/**
函数描述:Uasrt1中断处理函数
*/
void Usart1_ReceiveBateByIT(void);

/*
函数描述：Uasrt2发送一个字节数据
*/
void Usart2SendByte(uint8_t date );
/**
函数描述：Uasrt2发送len个字节
*/
void Usart2SendData(u8 *buf,u8 len);

/**
函数描述:Uasrt2接收两个字节数据
*/
uint16_t Usart2ReceiveByte(void);

/**
函数描述:Uasrt2中断处理函数
*/
void Usart2ReceiveBateByIT(void);

void Usart_ReceiveBate(uint8_t rx_data);

/**
函数描述：复位串口接收缓冲
*/
void Usart_BuffReset(void);

#endif /*_USART_H*/


