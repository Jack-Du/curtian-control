#include "config.h"
#include "w5500_net_handle.h"

char BuildDataStr[]=__DATE__;   //编译时的日期
char BuildTimeStr[]=__TIME__;   //编译时的时间

char SoftVersion[]=SOFT_VERSION;
char BoardVersion[]=BOARD_VERSION;
char LoaderVersion[16] = {0x00};

/*初始化*/
void init_config_info(void)
{
	init_net_info();
}

/*从flash读取配置信息*/
void read_config_info(void);

/**将修改的信息存储到flash*/
void write_config_info(void);



