#ifndef ___HWLED_H__
#define   ___HWLED_H__

#include "stm32f10x.h"
#include "delay.h"	

#define HW_GPIO_RCC   RCC_APB2Periph_GPIOA
#define HW_GPIO_PORT  GPIOA
#define HW_GPIO_PIN   GPIO_Pin_0 
#define HW_1  GPIO_SetBits(HW_GPIO_PORT,HW_GPIO_PIN);
#define HW_0  GPIO_ResetBits(HW_GPIO_PORT,HW_GPIO_PIN);

#define LED_GPIO_RCC   RCC_APB2Periph_GPIOA
#define LED_GPIO_PORT  GPIOA
#define LED_GPIO_PIN   GPIO_Pin_3 
#define LED_1  GPIO_SetBits(LED_GPIO_PORT,LED_GPIO_PIN);
#define LED_0  GPIO_ResetBits(LED_GPIO_PORT,LED_GPIO_PIN);

/*美的空调的控制指令码类型*/
typedef enum
{
	MIDEA_BOOTUP = 0X00,
	MIDEA_WIND_DIRECTION_SWING = 0X01,
	MIDEA_SHUTDOWN = 0X02,
		
}STRUCT_MIDEA_AIRCONDITIONER_CODE_TYPE;

extern u8 FJPG_HW_TAB[4];
extern u8 GL_HW_TAB[9];
extern u8 Haier_HW_TAB[14];
extern u8 Glx_HW_TAB[18];
extern u8 Md_HW_TAB[18];

extern u8 FJPG_HW_Send_Bit; //风机盘管空调
extern u8 GL_HW_Send_Bit;   //格力空调
extern u8 Haier_HW_Send_Bit;//海尔空调
extern u8 GLx_HW_Send_Bit;//
extern u8 Md_HW_Send_Bit;//
extern u8 Md_HW_Send_Type;//


void Confing_HWLED(void);  
void Config_LED(void);     //配置成普通输出类型用来测试时间 
void TIM2_PWM_Init(u16 arr,u16 psc); //产生 38khz载波 
void SendData_hw(u8 *hwtab);     //发送红外数据
void Send_Start_Bit(void);  //发送引导数据
void HW_Send_Bit_1(void);   //红外发送1
void HW_Send_Bit_0(void);   //红外发送0 
void Send_Start_Bit(void);  //引导码
void Send_Over(void);     //发送一个结束码，因为最后一个位只有遇到下降沿才能读取（发射端的上升沿） 

void Haier_SendData_hw(u8 *hwtab);     //发送红外数据
void Haier_Send_Start_Bit(void);       //发送引导数据
void Haier_HW_Send_Bit_1(void);        //红外发送1
void Haier_HW_Send_Bit_0(void);        //红外发送0
void Haier_Send_Start_Bit(void);       //海尔引导码
void SendData_Fjpg(u8 *hwtab);         //风机盘管空调
void SendData_Gl(u8 *hwtab);           //格力空调
void SendData_Haier(u8 *hwtab);        //海尔空调 
void SendData_Glx(u8 *hwtab);          //格力空调 2 

//void Midea_SendData_Midea(u8 *hwtab);   //美的空调
void SendData_Midea(u8 *hwtab,u8 type);   //美的空调


#endif 


