#include "HWLED.H"


u8 FJPG_HW_TAB[4] = {0};
u8 GL_HW_TAB[9] = {0};
u8 Haier_HW_TAB[14] = {0};
u8 Glx_HW_TAB[18];
u8 Md_HW_TAB[18];


u8 FJPG_HW_Send_Bit= 0; //风机盘管空调
u8 GL_HW_Send_Bit= 0;   //格力空调
u8 Haier_HW_Send_Bit= 0;//海尔空调
u8 GLx_HW_Send_Bit= 0;//
u8 Md_HW_Send_Bit= 0;//
u8 Md_HW_Send_Type= 0;//


/********************************************************************
* Function Name  : TIM2_PWM_Init(u16 arr,u16 psc)
* Function       : TIM2的通道CHx的PWM模式初始化
* parameter      : arr - 自动重装值
				   psc - 时钟预分频数	
* Description    : 频率f = 72M/[(psc+1)*(arr+1)]               
* Return         : void  72000/38  =  1894   = 1894  =  (630+1) * (2+1)
*********************************************************************/
void TIM2_PWM_Init(u16 arr,u16 psc)  //产生 38khz载波
{
	/* 初始化结构体定义 */
	TIM_TimeBaseInitTypeDef	TIM_TimeBaseInitStructure;
	TIM_OCInitTypeDef 	TIM_OCInitStructure;
	/* 使能相应端口的时钟 */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);	  //使能定时器2时钟
	/* TIM2 初始化*/
	TIM_TimeBaseInitStructure.TIM_Period = arr;	    //下一个更新事件装入活动的自动重装载寄存器周期的值
	TIM_TimeBaseInitStructure.TIM_Prescaler = psc;	//作为TIMx时钟频率除数的预分频值 
	TIM_TimeBaseInitStructure.TIM_ClockDivision = 0;  //时钟分割:TDTS = Tck_tim
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;	//TIM向上计数模式
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);
	/* 定时器TIM2 Ch1PWM模式初始化 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;    //选择定时器模式:TIM PWM1
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;	//比较输出使能
  	TIM_OCInitStructure.TIM_Pulse = (arr+1)/2;	         //占空比 50%
  
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;	//输出极性:TIM输出比较极性低
	TIM_OC1Init(TIM2, &TIM_OCInitStructure);
	/* 使能TIM2在CCR1上的预装载寄存器 */
	TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable); 
	/* 使能定时器 */
	TIM_Cmd(TIM2, DISABLE);  //关闭定时器 	 
  
}
 
void Confing_HWLED(void)
{
 GPIO_InitTypeDef  GPIO_InitStructure;	 
 RCC_APB2PeriphClockCmd(HW_GPIO_RCC|RCC_APB2Periph_AFIO , ENABLE);  //

 GPIO_InitStructure.GPIO_Pin = HW_GPIO_PIN;    
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 		   //推挽输出
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;		 //IO口速度为10MHz
 GPIO_Init(HW_GPIO_PORT, &GPIO_InitStructure);			   //根据设定参数初始   
//	delay_init(72);	
//	delay_init();
 TIM2_PWM_Init(1894,0);	//72000/(1894+1) = 37.99K 	  	
}
void Config_LED(void)
{
 GPIO_InitTypeDef  GPIO_InitStructure;	 
 RCC_APB2PeriphClockCmd(LED_GPIO_RCC, ENABLE);  //
 GPIO_InitStructure.GPIO_Pin = LED_GPIO_PIN;    
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		   //推挽输出
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为10MHz
 GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);	
}
void HW_Send_Bit_1(void)  //红外发送1 
{
	
  TIM_Cmd(TIM2, ENABLE);           //开启载波定时器
  delay_us(560);
	TIM_Cmd(TIM2, DISABLE);          //关闭载波定时器
  TIM_SetCounter(TIM2,0);          //设置计数器的值为 0
  HW_1;
  delay_us(560);
  HW_1;
}

void Haier_HW_Send_Bit_1(void)  //  
{
	
  TIM_Cmd(TIM2, ENABLE);           //开启载波定时器
  delay_us(560);
	TIM_Cmd(TIM2, DISABLE);          //关闭载波定时器
  TIM_SetCounter(TIM2,0);          //设置计数器的值为 0
  HW_1;
  delay_us(560);
  HW_1;
}

void HW_Send_Bit_0(void)           //红外发送0 
{
 TIM_Cmd(TIM2, ENABLE);           //开启载波定时器
 delay_us(560);
 HW_1;
 TIM_Cmd(TIM2, DISABLE);          //开启载波定时器
 TIM_SetCounter(TIM2,0);          //设置计数器的值为 0
 delay_us(1690);
 HW_1;
}

void Haier_Send_Bit_0(void)       //海尔
{	
 TIM_Cmd(TIM2, ENABLE);           //开启载波定时器
 delay_us(560);
 HW_1;
 TIM_Cmd(TIM2, DISABLE);          //开启载波定时器
 TIM_SetCounter(TIM2,0);          //设置计数器的值为 0
 delay_us(1690);
 HW_1;
}
 
void Send_Start_Bit(void)
{
	TIM_Cmd(TIM2, ENABLE);          //开启载波定时器
	delay_us(9000);
  TIM_Cmd(TIM2, DISABLE);         //关闭载波定时器 
	TIM_SetCounter(TIM2,0);         //设置计数器的值为 0	
	HW_1;
	delay_us(4500);
	HW_1;	
}
void Haier_Send_Start_Bit(void)   //海尔
{
	TIM_Cmd(TIM2, ENABLE);          //开启载波定时器
	delay_us(3000);                 //3ms低电平
  TIM_Cmd(TIM2, DISABLE);         //关闭载波定时器 
	TIM_SetCounter(TIM2,0);         //设置计数器的值为 0	
	HW_1;
	delay_us(3000);                 //3ms 高电平
	HW_1;
  TIM_Cmd(TIM2, ENABLE);          //开启载波定时器
	delay_us(3000);
  TIM_Cmd(TIM2, DISABLE);         //关闭载波定时器 
	TIM_SetCounter(TIM2,0);         //设置计数器的值为 0	 
	HW_1;
	delay_us(4500);
  HW_1;	
}

 
void Send_Connect_Bit(void)
{
	TIM_Cmd(TIM2, ENABLE);          //开启载波定时器
	delay_us(560);                 //低电平延时
  TIM_Cmd(TIM2, DISABLE);         //关闭载波定时器 
	TIM_SetCounter(TIM2,0);    //设置计数器的值为 0	
	HW_1;
	delay_us(20000);   //20000us
	HW_1;	
}

void Send_Connect_Bit_40(void)
{
	TIM_Cmd(TIM2, ENABLE);          //开启载波定时器
	delay_us(560);                 //低电平延时
  TIM_Cmd(TIM2, DISABLE);         //关闭载波定时器 
	TIM_SetCounter(TIM2,0);    //设置计数器的值为 0	
	HW_1;
	delay_us(40000);   //40000us
	HW_1;	
}

void Send_Over(void)     //发送一个结束码，因为最后一个位只有遇到下降沿才能读取（发射端的上升沿）
{
	TIM_Cmd(TIM2, ENABLE);           //开启载波定时器
  delay_us(560); //0.500ms 1       //小于0.5ms 接收端很难识别到
	TIM_Cmd(TIM2, DISABLE);          //开启载波定时器	 
	TIM_SetCounter(TIM2,0);    //设置计数器的值为 0
  HW_1;
  delay_us(560); //0.500ms 1       //小于0.5ms 接收端很难识别到
  HW_1; 
}

void SendData_Fjpg(u8 *hwtab)   //风机盘管空调
{ 
	u8 i; 
	u8 j1,j2,j3,j4;
	j1 = *hwtab;
	j2 = *(hwtab+1);	
	j3 = *(hwtab+2);  
	j4 = *(hwtab+3);
	Send_Start_Bit(); //发送引导码 
//	数据 1     用户码
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j1 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	 j1=j1<<1;         
	}
// 数据 2      用户码
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j2 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j2=j2<<1;         
	}	
	//数据 3     数据码
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j3 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j3=j3<<1;         
	}
		//数据 4   数据反码
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j4 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j4=j4<<1;         
	}
	Send_Over();      //结束码
} 
  
void SendData_Gl(u8 *hwtab)   //格力空调
{ 
	u8 i; 
	u8 j1,j2,j3,j4,j5,j6,j7,j8,j9;
	j1 = *hwtab;
	j2 = *(hwtab+1);	
	j3 = *(hwtab+2);  
	j4 = *(hwtab+3);
	j5 = *(hwtab+4);
	j6 = *(hwtab+5);	
	j7 = *(hwtab+6);  
	j8 = *(hwtab+7);
	j9 = *(hwtab+8);
	Send_Start_Bit(); //发送引导码 
//	数据 1     用户码
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j1 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	 j1=j1<<1;         
	}
// 数据 2      用户码
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j2 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j2=j2<<1;         
	}	
	//数据 3     数据码
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j3 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j3=j3<<1;         
	}
		//数据 4   数据反码
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j4 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j4=j4<<1;         
	}
	 j5 =j5<<5; //先把高5位去掉
	    //数据 5  3位
	for(i=0;i<3;i++)  //先发射低位
	{
	 if(j5 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j5=j5<<1;         
	}
  Send_Connect_Bit();   //连接码 	
 //数据 6    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j6 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j6=j6<<1;         
	}
	 //数据7    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j7 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j7=j7<<1;         
	}
		 //数据8    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j8 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j8=j8<<1;         
	}
			 //数据9    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j9 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j9=j9<<1;         
	}
	Send_Over();      //结束码
}

void SendData_Haier(u8 *hwtab)   //海尔空调
{ 
	u8 i; 
	u8 j1,j2,j3,j4,j5,j6,j7,j8,j9,j10,j11,j12,j13,j14;
	j1 = *hwtab;
	j2 = *(hwtab+1);	
	j3 = *(hwtab+2);  
	j4 = *(hwtab+3);
	j5 = *(hwtab+4);
	j6 = *(hwtab+5);	
	j7 = *(hwtab+6);  
	j8 = *(hwtab+7);	
	j9 = *(hwtab+8);  
	j10 = *(hwtab+9);
	j11 = *(hwtab+10);
	j12 = *(hwtab+11);	
	j13 = *(hwtab+12); 	
	j14 = *(hwtab+13);
	Haier_Send_Start_Bit(); //发送引导码 
//	数据 1      
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j1 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	 j1=j1<<1;         
	}
// 数据 2      
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j2 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j2=j2<<1;         
	}	
	//数据 3      
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j3 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j3=j3<<1;         
	}
		//数据 4    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j4 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j4=j4<<1;         
	}  //数据 5 
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j5 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j5=j5<<1;         
	}	
 //数据 6    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j6 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j6=j6<<1;         
	}
	 //数据7    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j7 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j7=j7<<1;         
	}
		 //数据8    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j8 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j8=j8<<1;         
	}
	  //数据9    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j9 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j9=j9<<1;         
	}	
		//数据10    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j10 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j10=j10<<1;         
	}
		 //数据11    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j11 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j11=j11<<1;         
	}
	  //数据12   
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j12 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j12=j12<<1;         
	}	
		//数据13   
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j13 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j13=j13<<1;         
	}		
		//数据14   
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j14 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j14=j14<<1;         
	}		
	 Send_Over();    //结束码
	
	
} 



void SendData_Glx(u8 *hwtab)   //格力空调 2 
{ 
	u8 i; 
	u8 j1,j2,j3,j4,j5,j6,j7,j8,j9,j10,j11,j12,j13,j14,j15,j16,j17,j18;
	j1 = *hwtab;
	j2 = *(hwtab+1);	
	j3 = *(hwtab+2);  
	j4 = *(hwtab+3);
	j5 = *(hwtab+4);
	j6 = *(hwtab+5);	
	j7 = *(hwtab+6);  
	j8 = *(hwtab+7);
  j9 = *(hwtab+8);	
	j10 = *(hwtab+9);  
	j11 = *(hwtab+10);
	j12 = *(hwtab+11);
	j13 = *(hwtab+12);	
	j14 = *(hwtab+13);  
	j15 = *(hwtab+14);
  j16 = *(hwtab+15);
	j17 = *(hwtab+16);
  j18 = *(hwtab+17);
	Send_Start_Bit();   //发送引导码 
//	数据 1     用户码
	for(i=0;i<8;i++)    //先发射低位
	{
	 if(j1 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	 j1=j1<<1;         
	}
// 数据 2      用户码
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j2 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j2=j2<<1;         
	}	
	//数据 3     数据码
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j3 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j3=j3<<1;         
	}
		//数据 4    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j4 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j4=j4<<1;         
	}
 j5 =j5<<5; //先把高5位去掉
	    //数据 5  3位
	for(i=0;i<3;i++)  //先发射低位
	{
	 if(j5 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j5=j5<<1;         
	}
  Send_Connect_Bit();   //连接码 	
 //数据 6    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j6 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j6=j6<<1;         
	}
	 //数据7    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j7 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j7=j7<<1;         
	}
		 //数据8    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j8 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j8=j8<<1;         
	}
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j9 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j9=j9<<1;         
	}
  Send_Connect_Bit_40();   //连接码  20ms	
	Send_Start_Bit();        //发送引导码	
 //数据 10   
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j10 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j10=j10<<1;         
	}
	 //数据11    
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j11 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j11=j11<<1;         
	}
		 //数据12   
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j12 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j12=j12<<1;         
	}	   	
 //数据 13  
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j13 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j13=j13<<1;         
	}
	 //数据14   
   j14 =j14<<5; //先把高5位去掉
	    //数据 5  3位
	for(i=0;i<3;i++)  //先发射低位
	{
	 if(j14 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j14=j14<<1;         
	}
	 Send_Connect_Bit();   //连接码
   //数据15   
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j15 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j15=j15<<1;         
	}	
		 //数据16   
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j16 & 0x80)        
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j16=j16<<1;         
	}	
		 //数据17   
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j17 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j17=j17<<1;         
	}	
		 //数据18   
	for(i=0;i<8;i++)  //先发射低位
	{
	 if(j18 & 0x80)
		HW_Send_Bit_0();
	 else
		HW_Send_Bit_1();
	  j18=j18<<1;         
	}	
    Send_Over();      //结束码  
}


/*美的空调红外控制码
*
*R05D电控红外协议
*
*
* 引导码L ：4.4ms低电平 4.4ms高电平
* 分割码S : 0.54ms低电平 5.22ms高电平
*
*	数字 '1':0.54ms低电平 1.62ms高电平
*	数字 "0":0.54ms低电平 0.54ms高电平
*   
*   一次发码的终结符 0.54ms低电平
*   
*****************
*/
void Midea_HW_Send_Bit_0(void)  //红外发送0 
{
	
  TIM_Cmd(TIM2, ENABLE);		   //开启载波定时器
  delay_us(540);
	TIM_Cmd(TIM2, DISABLE); 		 //关闭载波定时器
  TIM_SetCounter(TIM2,0);		   //设置计数器的值为 0
  HW_1;
  delay_us(1620);
  HW_1;
}

void Midea_HW_Send_Bit_1(void)		   //红外发送1
{
 TIM_Cmd(TIM2, ENABLE); 		  //开启载波定时器
 delay_us(540);
 HW_1;
 TIM_Cmd(TIM2, DISABLE);		  //开启载波定时器
 TIM_SetCounter(TIM2,0);		  //设置计数器的值为 0
 delay_us(540);
 HW_1;
}


void Midea_Send_Start_Bit(void)
{
	TIM_Cmd(TIM2, ENABLE);			//开启载波定时器
	delay_us(4400);
  TIM_Cmd(TIM2, DISABLE);		  //关闭载波定时器 
	TIM_SetCounter(TIM2,0); 		//设置计数器的值为 0	
	HW_1;
	delay_us(4400);
	HW_1;	
}
 
void Midea_Send_Connect_Bit(void)
{
	TIM_Cmd(TIM2, ENABLE);			//开启载波定时器
	delay_us(540);				   //低电平延时
  TIM_Cmd(TIM2, DISABLE);		  //关闭载波定时器 
	TIM_SetCounter(TIM2,0);    //设置计数器的值为 0 
	HW_1;
	delay_us(5220);   //20000us
	HW_1;	
}

void Midea_Send_Over(void)	 //发送一个结束码，因为最后一个位只有遇到下降沿才能读取（发射端的上升沿）
{
	TIM_Cmd(TIM2, ENABLE);			 //开启载波定时器
  	delay_us(540); //0.500ms 1	   //小于0.5ms 接收端很难识别到
	TIM_Cmd(TIM2, DISABLE); 		 //开启载波定时器	 
	TIM_SetCounter(TIM2,0);    //设置计数器的值为 0
  HW_1;
  	delay_us(540); //0.500ms 1	   //小于0.5ms 接收端很难识别到
  HW_1; 
}

/*
*
*报文格式：
*	起始 + 数据 + 间隔 +数据
*
*	“ L,A,A’,B,B’,C,C’, S, L,A,A’,B,B’,C,C’ ”
*
*
*/
//void SendData_Midea(u8 *hwtab)   //美的空调
//{ 

//	u8 i; 
//	u8 j1,j2,j3,j4,j5,j6,j7,j8,j9,j10,j11,j12;
//	j1 = *hwtab;
//	j2 = *(hwtab+1);	
//	j3 = *(hwtab+2);  
//	j4 = *(hwtab+3);
//	j5 = *(hwtab+4);
//	j6 = *(hwtab+5);	
//	j7 = *(hwtab+6);  
//	j8 = *(hwtab+7);
//	j9 = *(hwtab+8);

//	j10 = *(hwtab+9);  
//	j11 = *(hwtab+10);
//	j12 = *(hwtab+11);
//	
//	Midea_Send_Start_Bit(); //发送引导码 
////	数据 1	 用户码
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j1 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	 j1=j1<<1;		   
//	}
//// 数据 2 	 用户码
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j2 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j2=j2<<1; 		
//	}	
//	//数据 3	   数据码
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j3 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j3=j3<<1; 		
//	}
//		//数据 4	 数据反码
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j4 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j4=j4<<1; 		
//	}

//		//数据 5	   数据码
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j5 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j5=j5<<1; 		
//	}
//		//数据 6	 数据反码
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j6 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j6=j6<<1; 		
//	}

//  	Midea_Send_Connect_Bit();	//连接码 	
//  	Midea_Send_Start_Bit(); //发送引导码 

//	 //数据7	  
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j7 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j7=j7<<1; 		
//	}
//		 //数据8	  
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j8 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j8=j8<<1; 		
//	}
//			 //数据9	  
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j9 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j9=j9<<1; 		
//	}
//				 //数据10  
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j10 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j10=j10<<1; 		
//	}

//					 //数据11 
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j11 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j11=j11<<1; 		
//	}

//					 //数据12  
//	for(i=0;i<8;i++)  //先发射低位
//	{
//	 if(j12 & 0x80)
//		Midea_HW_Send_Bit_0();
//	 else
//		Midea_HW_Send_Bit_1();
//	  j12=j12<<1; 		
//	}
//	
//	Midea_Send_Over();	  //结束码
//}

/*
*
*报文格式：
*	起始 + 数据 + 间隔 +数据
*
*	“ L,A,A’,B,B’,C,C’, S, L,A,A’,B,B’,C,C’ ”
*
*   红外码长度 : 6、12、18
*/
void SendData_Midea(u8 *hwtab,u8 type)   //美的空调
{ 

	u8 i,j,k;
	u8 len = 0x00;
	u8 tmp = 0x00;
	u8 code[18] = {0x00};

	switch(type)
	{
		case MIDEA_BOOTUP:
			len = 0x0c;
			break;

		case MIDEA_WIND_DIRECTION_SWING:
			len = 0x06;
			break;
		case MIDEA_SHUTDOWN:
			len = 0x12;
			break;

		default:
			return;
	}
	
	for(i = 0x00;i < len;i ++)
	{
		code[i] = hwtab[i];
	}
	
	Midea_Send_Start_Bit(); //发送引导码 

	for (i = 0x00;i< len;)
	{
		for(j = 0x00;j< 0x06;j ++)
		{
			tmp = code[i + j];
			for(k=0;k<8;k++)  //先发射高位
			{
			 if(tmp & 0x80)
				Midea_HW_Send_Bit_0();
			 else
				Midea_HW_Send_Bit_1();
			 	tmp=tmp<<1;		   
			}
		}

		i += j;
		if (i == len)
		{
			break;
		}

		Midea_Send_Connect_Bit();	//连接码 	
		Midea_Send_Start_Bit(); 	//发送引导码 
		
	}
	
	Midea_Send_Over();	  //结束码		
}

