#ifndef UDP_NET_PROTOCOL_H
#define UDP_NET_PROTOCOL_H

#include <stdint.h>
#include "bsp_usart3.h"

#define RECEIVE_FRAME_LENGTH 0x100
#define DEBUG_UDP_PROCOTOL_ENABLE 0x01

#if DEBUG_UDP_PROCOTOL_ENABLE == 0x01
	#define udp_protocol_usart_printf_string(a)   usart_printf_string(a)
	#define udp_protocol_usart_printf_int(a)   usart_printf_int(a)
	#define udp_protocol_usart_printf(a,b)   usart_printf(a,b)
#else	
	#define udp_protocol_usart_printf_string(a) 
	#define udp_protocol_usart_printf_int(a) 
	#define udp_protocol_usart_printf(a,b)   
#endif

/*******************************************************udp协议******************************************************************************/
/*
（1）(cmd[0] == 'a') && (cmd[1] == 'z')
	查询主控板IP，确定板子运行状态。网管给板子发送“az+11位校验”，板子给网管返回“azx”x为设备的类型号
（2）(cmd[0] == 'b') && (cmd[1] == 'z')
	设置主控板网络参数。网管发送“bz+数据+11位校验”，从第3个字节到第17个字节为IP地址，第18个字节到第32个字节为子网掩码地址，第33个字节到第47个字节为网关地址，第48个字节到第59个字节为MAC地址。
（3）(cmd[0] == 'c') && (cmd[1] == 'z')
	保存教室PC和网管计算机的IP。网管发送“cz+数据+11位校验”，从第3个字节到第17个字节为教室PC的IP地址，第18个字节到第32个字节为网管计算机的IP地址。
（4）(cmd[0] == 'c') && (cmd[1] == 'r')
	获取教室PC 机和网管IP 地址，返回“cr+数据+11 位校验”，从第3 个字节到第17 个字节为教室PC 的IP 地址，第18 个字节到第32 个字节为网管计算机的IP 地址。
（5）(cmd[0] == 'd') && (cmd[1] == 'z')
	网络授时。网管发送“dz+数据+11位校验”，数据为19个字节的字符串，例如：
	“2013-09-25 17：52：00”（注意在25日和17时之间有一个空格字符）。
（6）(cmd[0] == 'u') && (cmd[1] == 'z')
	查询主控板的子网掩码、网关和MAC地址。网管给板子发“uz+11位校验”，板子返回“uz+子网掩码+‘，’+网关地址+‘，’+MAC地址”。
（7）(cmd[0] == 't') && (cmd[1] == 'z')
	远程控制命令。网管发送“tz+数据+11位校验”。第3个字节为长度，第4个字节开始为控制命令。板子将第4个字节开始并以第3个字节为长度的控制命令发送到串口0待处理的命令队列中。
（8）(cmd[0] == 'o') && (cmd[1] == 'z')
	升级程序。网管发送“oz+11位校验”，作为服务端，监听自己的9110端口。板子通过TCP方式连接网管并接收数据。
	收到“1”后，收取文件大小（4个字节），接着收取文件名称大小（4个字节），再收取文件名，最后收取文件的具体内容。
	收取完毕后，写flash。写成功后，给网管发送“1”，若收数据出错或写flash不成功，给网管发送“-1”。更新完成后重启板子。
	收到“2”后，接收网管发来的命令。首先收命令大小，然后收命令的具体内容。之后，没有进行操作。成功后返回“1”。
（9）、(cmd[0] == 'i') && (cmd[1] == 'z')
 (10)、(cmd[0] == 'r') && (cmd[1] == 'i')
	查询设备运行时间和上课次数，设置返回 ri,xxxx,yyyy  ，xxxx表示时间，单位小时；yyyy 表示上课次数  
 (11) (cmd[0] == 'w') && (cmd[1] == 'g')
	设置网管的IP地址， wgxxxx,yyyy  ，xxxx表示网管1地址；yyyy 表示网管2IP 
 (12) (cmd[0] == 'r') && (cmd[1] == 'g')
	获取网管的IP地址，设备返回 rg,xxxx,yyyy  ，xxxx表示网管1地址；yyyy 表示网管2IP ；
 (13)(cmd[0] == 'v') && (cmd[1] == 't')，
 (14)(cmd[0] == 'w') && (cmd[1] == 'i')
 	wi,xxxx,yyyy ，xxxx 表示运行时间，单位小时；yyyy 表示上课次数，
 (15) (cmd[0] == 'h') && (cmd[1] == 'z')
（16）(cmd[0] == 'h') && (cmd[1] == 'r')
 (17)(cmd[0] == 'w') && (cmd[1] == 'g')
    清除设备BootLoad 标志位，并复位，使设备进入升级模式
 (18)(cmd[0] == 'w') && (cmd[1] == 'a')

 (19)(cmd[0] == 'g') && (cmd[1] == 'v')
 	读取版本信息 回复 gvom_v:3.0,loader:1.2,app:1.2,buildtime:Sep 17 2021 15:21:39
 (20)(cmd[0] == 'g') && (cmd[1] == 's')
    查询本地网络修改标记:0:默认未修改 1:网络区参数未设置(全部ff) 2：冷启动手动恢复默认 3:远程配置 4:未知异常 5:预留

 (21)(cmd[0] == 's') && (cmd[1] == 'h')
	使能手动复位IP地址功能

 (22)(cmd[0] == 'g') && (cmd[1] == 'c')
	查询网络接收包信息
*/
/********************************************************************************end*************************************************************************/

/*udp接收帧数据定义标识 */
typedef enum
{
	UDP_REV_HEAD = 0X00,
	UDP_REV_TYPE = 0X01,
	UDP_REV_LENGTH_H = 0X02,
	UDP_REV_LENGTH_L = 0X03,
	
}UDP_RECEIVE_FRAME_DEFINE;
	
/*配置工具 :配置信息*/
/*帧标识*/
/*帧头*/
/*帧长度*/
typedef struct 
{
	uint8_t framehead;
	uint8_t frametype;

	uint16_t framelen;
	uint8_t content[RECEIVE_FRAME_LENGTH];
	
}UDP_RECEIVE_FRAME_STRUCT;


/*定义接收到的指令类型*/
typedef enum
{
	TYPE_OTHER,
}UDP_RECEIVE_CMDLINE_TYPE;

extern UDP_RECEIVE_FRAME_STRUCT UdpReceiveFrameStruct;

extern void init_udpreceive(void);
extern uint8_t anylise_udp_recevice(uint8_t *pRev,uint8_t * pSend,uint8_t revLen,uint8_t * sendLen);
extern void send_udpdata(void);
extern uint8_t udp_receive_frame_crc(uint8_t * data,uint8_t len);
static	u8 delay_xs ( int timecnt );

#endif
