#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "udp_net_protocol.h"
#include "w5500_net_handle.h"
#include "system_start_info.h"
#include "delay.h"
#include "common.h"
#include "wdg.h"
#include "out.h"

UDP_RECEIVE_FRAME_STRUCT UdpReceiveFrameStruct = 0x00;

/*
*timecnt :x(s)延时
*
*
*
*/
static	u8 delay_xs ( int timecnt )
{
	do
	{
		delay_ms ( 1000 );
#if SYSTEM_SUPPORT_OS == 0x01
		SoftWdtFeed(ETHERNET_COMMUNICATION_TASK_ID);
#else
		FeedDog();	   //喂狗
#endif

	}
	while ( ( -- timecnt )> 0);

	return 1;
}

/******
*
*初始化结构体
*
*
***************/
void init_udpreceive()
{
	memset(&UdpReceiveFrameStruct,0,sizeof (UDP_RECEIVE_FRAME_STRUCT));
	return;
}

/******
*
*解析接收到的udp数据帧 
* 如果能够处理 ：返回 接收到的数据帧长度
*无法处理		:	返回 0
*
***************/
uint8_t anylise_udp_recevice(uint8_t *pRev,uint8_t * pSend,uint8_t revLen,uint8_t * sendLen)
{

	uint8_t cnt = 0x00;
	uint8_t len = 0x00;
	
	uint8_t tmp[256] = {0x00};
	
	if (revLen == 0x00)
	{
		return 0x00;
	}

	/*校验*/
	udp_receive_frame_crc(pRev,revLen);
	
	/*查询板子IP地址*/
	if((pRev[UDP_REV_HEAD]=='a') && (pRev[UDP_REV_TYPE]=='z'))	  
	{ 
		pSend[cnt ++] = pRev[UDP_REV_HEAD]; 
		pSend[cnt ++] = pRev[UDP_REV_TYPE];
		pSend[cnt ++] = 7;

	}

	/*升级指令 :复位系统 ，进入升级程序*/
	if((pRev[UDP_REV_HEAD]=='g') && (pRev[UDP_REV_TYPE]=='w'))	  
	{

		memcpy(pSend,pRev,revLen);
		cnt = revLen;
		
		//清除BootLoad标志位，复位
		udp_protocol_usart_printf_string("\r\n System Updata \r\n");
		FlashWriteWord(BOARD_START_MODE_ADDR,BOOT_MODE);

		/*复位系统*/
#if SYSTEM_START_INFO_CHECK_ENABLE == 0X01
		SystemStartInfo.SystemResetInfo.system_reset_bit.system_update = 0x00;
		SystemStartInfo.SystemResetInfo.system_reset_bit.system_update = 0x01;
#endif	

#if 0
		/*系统使能	*/
#if SYSTEM_SUPPORT_OS == 0x01
			SoftWDTRst();
#else
			delay_xs(1);
	
			/*软件复位*/
			__set_FAULTMASK(1);
			NVIC_SystemReset();
#endif
#endif
		/*软件复位*/
		__set_FAULTMASK(1);
		NVIC_SystemReset();


	}

	else if((pRev[UDP_REV_HEAD]=='u') && (pRev[UDP_REV_TYPE]=='z'))	  
	{

		pSend[cnt ++] = pRev[UDP_REV_HEAD]; 
		pSend[cnt ++] = pRev[UDP_REV_TYPE];
		//pSend[cnt ++] = 7;
		
		/*子网掩码*/
		sprintf(pSend+ cnt,"%d.%d.%d.%d,",(uint8_t)NetInfoStruct.netmask.byte[0],(uint8_t)NetInfoStruct.netmask.byte[1],\
										  (uint8_t)NetInfoStruct.netmask.byte[2],(uint8_t)NetInfoStruct.netmask.byte[3]);
		cnt = strlen(pSend);
		
		/*网关*/
		sprintf(pSend+ cnt,"%d.%d.%d.%d,",(uint8_t)NetInfoStruct.gateway.byte[0],(uint8_t)NetInfoStruct.gateway.byte[1],\
										  (uint8_t)NetInfoStruct.gateway.byte[2],(uint8_t)NetInfoStruct.gateway.byte[3]);
		cnt = strlen(pSend);

		/*mac地址*/
		sprintf(pSend+ cnt,"%02x.%02x.%02x.%02x.%02x.%02x",(uint8_t)NetInfoStruct.mac[0].byte[0],(uint8_t)NetInfoStruct.mac[0].byte[1],(uint8_t)NetInfoStruct.mac[0].byte[2],\
														   (uint8_t)NetInfoStruct.mac[0].byte[3],NetInfoStruct.mac[1].byte[0],NetInfoStruct.mac[1].byte[1]);
		cnt = strlen(pSend);
		
	}

	/*设置网络参数*/
	else if((pRev[UDP_REV_HEAD]=='b') && (pRev[UDP_REV_TYPE]=='z'))
	{
		pSend[cnt++] = pRev[UDP_REV_HEAD];
		pSend[cnt++] = pRev[UDP_REV_TYPE];

		SringToOx((char*)(pRev+2),15,(uint8_t *)NetInfoStruct.ipaddr.byte,4);

		//获取待设置的网络掩码
		SringToOx((char*)(pRev+17),15,(uint8_t *)NetInfoStruct.netmask.byte,4);

		//获取网关
		SringToOx((char*)(pRev+32),15,(uint8_t *)NetInfoStruct.gateway.byte,4);

		//获取MAC地址
		Sring_Mask_ToOx((char*)(pRev+47),12,(uint8_t *)NetInfoStruct.mac,6);

		set_net_modify_model((uint8_t)NET_REMOTE_CONFIG);		
		write_net_info();
		
	}
	else if((pRev[UDP_REV_HEAD]=='s') && (pRev[UDP_REV_TYPE]=='c'))
	{
		pSend[cnt++] = pRev[UDP_REV_HEAD];
		pSend[cnt++] = pRev[UDP_REV_TYPE];
	}	
	else if((pRev[UDP_REV_HEAD]=='h') && (pRev[UDP_REV_TYPE]=='z'))
	{
	}
	else if((pRev[UDP_REV_HEAD]=='h') && (pRev[UDP_REV_TYPE]=='r'))
	{
	}
	else if((pRev[UDP_REV_HEAD]=='r') && (pRev[UDP_REV_TYPE]=='i'))
	{
	}
	else if((pRev[UDP_REV_HEAD]=='w') && (pRev[UDP_REV_TYPE]=='a'))
	{
	}
	else if((pRev[UDP_REV_HEAD]=='w') && (pRev[UDP_REV_TYPE]=='i'))
	{
	}
	else if((pRev[UDP_REV_HEAD]=='c') && (pRev[UDP_REV_TYPE]=='z'))
	{
	}
	else if((pRev[UDP_REV_HEAD]=='c') && (pRev[UDP_REV_TYPE]=='r'))
	{
	}
	else if((pRev[UDP_REV_HEAD]=='r') && (pRev[UDP_REV_TYPE]=='g'))
	{
	}
	else if((pRev[UDP_REV_HEAD]=='w') && (pRev[UDP_REV_TYPE]=='g'))
	{
	}
	else if((pRev[UDP_REV_HEAD]=='t') && (pRev[UDP_REV_TYPE]=='z'))
	{

	}
	else if((pRev[UDP_REV_HEAD]=='r') && (pRev[UDP_REV_TYPE]=='s'))
	{

		pSend[cnt++] = pRev[UDP_REV_HEAD];
		pSend[cnt++] = pRev[UDP_REV_TYPE];
		
		/*重启设备*/
		udp_protocol_usart_printf_string("\n等待5s之后，重启设备。。。\n");

#if SYSTEM_START_INFO_CHECK_ENABLE == 0X01
		SystemStartInfo.SystemResetInfo.system_reset_bit.system_update = 0x00;
		SystemStartInfo.SystemResetInfo.system_reset_bit.remote_ctrl_reset = 0x01;
#endif	
		
/*系统使能  */
#if SYSTEM_SUPPORT_OS == 0x01
		SoftWDTRst();
#else
		delay_xs(1);

		/*软件复位*/
		__set_FAULTMASK(1);
		NVIC_SystemReset();
#endif
	}

	/*读取版本信息 回复 gvom_v:3.0,loader:1.2,app:1.2,buildtime:Sep 17 2021 15:21:39*/
	else if((pRev[UDP_REV_HEAD]=='g') && (pRev[UDP_REV_TYPE]=='v'))
	{
		
		pSend[cnt++] = pRev[UDP_REV_HEAD];
		pSend[cnt++] = pRev[UDP_REV_TYPE];

		/*版本信息*/
		sprintf (pSend + cnt,"%s,loader:%s,app:%s,buildtime:%s %s",BoardVersion,LoaderVersion,SoftVersion,BuildDataStr,BuildTimeStr);
		cnt = strlen(pSend);
	}

	
	/*读取网络参数修改状态标记 :*/
	else if((pRev[UDP_REV_HEAD]=='g') && (pRev[UDP_REV_TYPE]=='s'))
	{
		
		pSend[cnt++] = pRev[UDP_REV_HEAD];
		pSend[cnt++] = pRev[UDP_REV_TYPE];

		/*版本信息*/
		sprintf (pSend + cnt,"net modify :%02x,net modify enable :%02x",get_net_modify_model(),get_system_start_info_recover_net_enable_state());
		cnt = strlen(pSend);
	}
	

	/*手动复位IP地址标记 :*/
	else if((pRev[UDP_REV_HEAD]=='s') && (pRev[UDP_REV_TYPE]=='h'))
	{
		
		pSend[cnt++] = pRev[UDP_REV_HEAD];
		pSend[cnt++] = pRev[UDP_REV_TYPE];

		set_system_start_info_recover_net_enable_state(pRev[cnt] == 0x31?1:0);
	}

	
	/*网络接收包信息 :*/
	else if((pRev[UDP_REV_HEAD]=='g') && (pRev[UDP_REV_TYPE]=='c'))
	{
		pSend[cnt++] = pRev[UDP_REV_HEAD];
		pSend[cnt++] = pRev[UDP_REV_TYPE];

		/*返回当前接收包总数
		*接收到的最新一条数据
		*接收到数据帧时间
		*/
		len = GetRecvMessage(tmp);		
		sprintf (pSend + cnt,"receive frame counter :%d\t",GetRecvMessageCounter());
		cnt = strlen(pSend);
		sprintf (pSend + cnt,"receive frame  :time:%s\n",tmp);
		cnt = strlen(pSend);
		
	}

	return (*sendLen = cnt);

}


/******
*
*发送udp 数据帧
*
***************/
void send_udpdata()
{

}

/*****
*udp 数据帧校验
*
*检验和:C（X） = （X1+X2+X3+X4）% 0x100。
*帧长度是指数据的长度：高字节在前，低字节在后
*检验和:1位
*
*
**************************************/

uint8_t udp_receive_frame_crc(uint8_t * data,uint8_t len)
{
	uint8_t ret = 0x00;
	uint8_t i = 0x00;

	uint16_t sum = 0x00;

	for (i = 0x00;i < len-1;i ++)
	{
		sum += data[i];
	}

	return (sum & 0x00ff);
	
}

