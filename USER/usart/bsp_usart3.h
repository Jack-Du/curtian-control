#ifndef __USART3_H
#define	__USART3_H

#include "stm32f10x.h"

#define ENABLE_USART_PRINT 0X01
#define COMMON_BAUTRATE	9600


extern void USART3_Config(void);

extern void Usart3_SendByte(uint8_t ch);
extern void Usart3_SendStr_length(uint8_t *str,uint32_t strlen);
extern void Usart3_SendString(uint8_t *str);

extern void usart_printf(uint8_t *str,uint32_t strlen);
extern void usart_printf_string(uint8_t *str);
extern void usart_printf_int(uint32_t data);

#endif /* __USART2_H */
