#include "bsp_usart5.h"
#include <stdarg.h>

/*
 * 函数名：USART5_Config
 * 描述  ：USART5 GPIO 配置,工作模式配置
 * 输入  ：无
 * 输出  : 无
 * 调用  ：外部调用
 */
void USART5_Config(void)
{ 
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	/* config USART5 clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC|RCC_APB2Periph_GPIOD, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);
	/* USART5 GPIO config */
   /* Configure USART5 Tx (PC.12) as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);    
    /* Configure USART5 Rx (PD.02) as input floating */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
	  
	/* USART5 mode config */
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	
	USART_Init(UART5, &USART_InitStructure); 
	/* 使能串口5接收中断 */
	USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);

	USART_Cmd(UART5, ENABLE);
	USART_ClearFlag(UART5, USART_FLAG_TC);
}

/***************** 发送一个字符  **********************/
void Usart5_SendByte(uint8_t ch)
{
	
	USART_SendData(UART5,ch);
	while (USART_GetFlagStatus(UART5, USART_FLAG_TXE) == RESET);	
}
/*****************  发送指定长度的字符串 **********************/
void Usart5_SendStr_length(uint8_t *str,uint32_t strlen)
{
	unsigned int k=0;
    do 
    {
        Usart5_SendByte(*(str + k));
        k++;
    } while(k < strlen);
}

/*****************  发送字符串 **********************/
void Usart5_SendString(uint8_t *str)
{
	unsigned int k=0;
    do 
    {
        Usart5_SendByte(*(str + k));
        k++;
    } while(*(str + k)!='\0');
}
/******************* (C) COPYRIGHT 2012 WildFire Team *****END OF FILE************/
