#ifndef __USART3_H
#define	__USART3_H

#include "stm32f10x.h"
#include <stdio.h>

void USART5_Config(void);

void Usart5_SendByte(uint8_t ch);  //发送一个字节数据
void Usart5_SendStr_length(uint8_t *str,uint32_t strlen);//发送字符串
void Usart5_SendString(uint8_t *str);//发送字符
#endif /* __USART3_H */
