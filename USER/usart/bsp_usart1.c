#include "bsp_usart1.h"

/*******************************************************
 * 硬件连接：---------------------
 *          | 					  |
 *          | PB6  - USART1(Tx)   |
 *          | PB7  - USART1(Rx)   |
 *          |                     |
 *           ---------------------
*********************************************************/
 /**
  * @brief  USART1 GPIO 配置,工作模式配置。9600 8-N-1
  * @param  无
  * @retval 无
  */
  
void USART1_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;	
	/* config USART1 clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOB, ENABLE); 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
	/* USART1 GPIO config */
	/* Configure USART1 Tx (PB6) as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);    
	/* Configure USART1 Rx (PB7) as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GPIO_PinRemapConfig(GPIO_Remap_USART1,ENABLE);   //改变指定管脚的映射  
	/* USART1 mode config */
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);	
	/* 使能串口1接收中断 */
	//USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);
}
 
/// 重定向c库函数printf到USART1
int fputc(int ch, FILE *f)
{
		/* 发送一个字节数据到USART1 */
		USART_SendData(USART1, (uint8_t) ch);	
		/* 等待发送完毕 */
		while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);		
		return (ch);
}

/// 重定向c库函数scanf到USART1
int fgetc(FILE *f)
{
		/* 等待串口1输入数据 */
		while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);
		return (int)USART_ReceiveData(USART1);
}

/***************** 发送一个字符  **********************/
void Usart1_SendByte(uint8_t ch)
{
	
	USART_SendData(USART1,ch);
		
	while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);	
}
/*****************  发送指定长度的字符串 **********************/
void Usart1_SendStr_length(uint8_t *str,uint32_t strlen)
{
	unsigned int k=0;
    do 
    {
        Usart1_SendByte(*(str + k));
        k++;
    } while(k < strlen);
}

/*****************  发送字符串 **********************/
void Usart1_SendString(uint8_t *str)
{
	unsigned int k=0;
    do 
    {
        Usart1_SendByte(*(str + k));
        k++;
    } while(*(str + k)!='\0');
}


/*********************************************END OF FILE**********************/
