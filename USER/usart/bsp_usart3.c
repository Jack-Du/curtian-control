/*******************************************************
 * 硬件连接：---------------------
 *          | 					  |
 *          | PB10  - USART2(Tx)   |
 *          | PB11  - USART2(Rx)   |
 *          |                     |
 *           ---------------------
*********************************************************/
#include <stdio.h>
#include "stm32f10x.h"
#include <stdarg.h> 
#include "bsp_usart3.h"

/**************************************打印输出重定向*************************************/
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

/* Use no semihosting */

//#pragma import(__use_no_semihosting)
#pragma __asm(".global __use_no_semihosting");
	
struct __FILE
{  
	int handle;
};
FILE __stdout;

_sys_exit(int x)
{
	x = x;
}

//_ttywrch(int ch)
//{
//	ch=ch;
//}

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
	
	/* Place your implementation of fputc here */
//	#ifdef VIDEO_DEBUG_CONFIG_ITEM
  USART_SendData(USART3, (uint8_t) ch);
  /* Loop until the end of transmission */
  while (USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET)
  {}	
  return ch;

}

/**************************************end*************************************/

/*
 * 函数名：USARTE_Config
 * 描述  ：USARTE GPIO 配置,工作模式配置
 * 输入  ：无
 * 输出  : 无
 * 调用  ：外部调用
 */
void USART3_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	/* config USART3 clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

	/* USART3 GPIO config */
   /* Configure USART3 Tx (PB.10) as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
	    
  /* Configure USART3 Rx (PB.11) as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	  
	/* USART3 mode config */
	USART_InitStructure.USART_BaudRate = COMMON_BAUTRATE;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART3, &USART_InitStructure); 
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART3, ENABLE);
	//USART_ClearFlag(USART3, USART_FLAG_TC);
}



/***************** 发送一个字符  **********************/
void Usart3_SendByte(uint8_t ch)
{
 	
	USART_SendData(USART3,ch);		
	while (USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);	
}
/*****************  发送指定长度的字符串 **********************/
void Usart3_SendStr_length(uint8_t *str,uint32_t strlen)
{
	unsigned int k=0;
    do 
    {
        Usart3_SendByte(*(str + k));
        k++;
    } while(k < strlen);
}

/*****************  发送字符串 **********************/
void Usart3_SendString(uint8_t *str)
{
	unsigned int k=0;
    do 
    {
        Usart3_SendByte(*(str + k));
        k++;
    } while(*(str + k)!='\0');
}


/********************************************串口打印输出*************************************************************/
#if ENABLE_USART_PRINT == 0x01
void usart_printf(uint8_t *str,uint32_t strlen)
{
	Usart3_SendStr_length(str,strlen);
}

void usart_printf_string(uint8_t *str)
{
	Usart3_SendString(str);
}

void usart_printf_int(uint32_t data)
{
	uint16_t i = 0x00;
	uint8_t cnt = 0x00;

	uint8_t tmp[64] = {0x00};

	if (data == 0x00)
	{
		Usart3_SendString("0");
		return;
	}
			
	/**/
	for (i = 0x00;i < 64;i ++)
	{
		
		tmp[cnt] = data % 10;
		tmp[cnt] += '0';
		
		data /= 10;	
		cnt ++;

		if (data == 0x00)
		{
			break;
		}
	}

	for (i = cnt;i > 0x00;i --)
	{
		Usart3_SendByte(tmp[i-1]);
	}

	return ;
}

#else
void usart_printf(uint8_t *str,uint32_t strlen){}
void usart_printf_string(uint8_t *str){}
void usart_printf_int(uint32_t data){}
#endif


/*! @function
********************************************************************************
<PRE>
函数名   :DebugOut
功能     :应用层调试开关函数
----------------------------------------------------------------------------------
参数     :U8 DebugRank ------ 输出等级0-15
返回值   :
---------------------------------------------------------------------------------
使用的全局变量：APP_Debug_Config_Value
注意事项 ：输出字符串长度最大不可超过128字节，否则会出现内存错误
----------------------------------------------------------------------------------
作者     : 
</PRE>
*******************************************************************************/
void DebugOut(u8 DebugRank, const char *Fmt, ...) 
{
    va_list arg_ptr;
	u8 DebugValue = 0x00;
	
	if(DebugRank>=20)
	{
	   DebugRank=DebugRank-20;
		if (DebugRank<=DebugValue)			//比较高半字节，用于应用层		 
			{ 	   
				va_start(arg_ptr, Fmt);
				vprintf("\033[1m ",arg_ptr);
				vprintf(Fmt,arg_ptr); 
				vprintf("\033[0m ",arg_ptr);
				va_end(arg_ptr);
			}	
	}
 	else if (DebugRank<=DebugValue)          //比较高半字节，用于应用层       
    {              
        va_start(arg_ptr, Fmt);
        vprintf(Fmt,arg_ptr); 
        va_end(arg_ptr);
    } 
}
