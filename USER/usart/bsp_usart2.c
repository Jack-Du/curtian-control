/*******************************************************
 * 硬件连接：---------------------
 *          | 					  |
 *          | PD5  - USART2(Tx)   |
 *          | PD6  - USART2(Rx)   |
 *          |                     |
 *           ---------------------
*********************************************************/
#include "bsp_usart2.h"
 
/*
 * 函数名：USART2_Config
 * 描述  ：USART2 GPIO 配置,工作模式配置
 * 输入  ：无
 * 输出  : 无
 * 调用  ：外部调用
 */
void USART2_Config(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
		USART_InitTypeDef USART_InitStructure;
		/* config USART2 clock */
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO , ENABLE); 
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

		/* USART2 GPIO config */
		/* Configure USART2 Tx (PA2) as alternate function push-pull */
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  //复用输出
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

		/* Configure USART2 Rx (PA3) as input floating */
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
  
		/* USART2 mode config */
		USART_InitStructure.USART_BaudRate = 9600;
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_No ;
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
		USART_Init(USART2, &USART_InitStructure); 
		/* 使能串口2接收中断 */
		USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
		USART_Cmd(USART2, ENABLE);
		//USART_ClearFlag(USART2, USART_FLAG_TC);
		//USART_ClearFlag(USART2, USART_FLAG_RXNE); //清除中断标志	  

}



/***************** 发送一个字符  **********************/
void Usart2_SendByte(uint8_t ch)
{
	
	USART_SendData(USART2,ch);		
	while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);	
}
/*****************  发送指定长度的字符串 **********************/
void Usart2_SendStr_length(uint8_t *str,uint32_t strlen)
{
	unsigned int k=0;
    do 
    {
        Usart2_SendByte(*(str + k));
        k++;
    } while(k < strlen);
}

/*****************  发送字符串 **********************/
void Usart2_SendString(uint8_t *str)
{
	unsigned int k=0;
    do 
    {
        Usart2_SendByte(*(str + k));
        k++;
    } while(*(str + k)!='\0');
}

/******************************/
