#ifndef __USART1_H
#define	__USART1_H

#include "stm32f10x.h"
#include <stdio.h>

void USART1_Config(void);
void Usart1_SendByte(uint8_t ch);      //发送一个字节
void Usart1_SendStr_length(uint8_t *str,uint32_t strlen); //发送一个字符串
void Usart1_SendString(uint8_t *str);  //发送一个字符
int fputc(int ch, FILE *f);
int fgetc(FILE *f); 
#endif /* __USART1_H */


