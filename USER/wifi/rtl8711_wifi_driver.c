#include <string.h>
#include <stdio.h>

#include "rtl8711_wifi_driver.h"

#include "delay.h"
#include "bsp_usart3.h"
#include "serial_comm.h"
#include "wdg.h"
#include "os.h"
#include "config.h"

/*声明结构体变量*/
RTL8711_WIFI_STRUCT Rtl8711WifiStru;

u8 systemrestflag = 0x00;
u32 bautrateswitchcnt = 0x00;

u32 nodatarecvcnt = 0x00;

void uart_printf(unsigned char *data,unsigned char len);
void ttl_default_config(u32 baudrate);

/*
*timecnt :x(s)延时
*
*
*
*/
static	u8 delay_xs ( int timecnt )
{
	do
	{
		delay_ms ( 1000 );

#if SYSTEM_SUPPORT_OS == 0x01
		SoftWdtFeed(WIFI_COMMUNICATION_TASK_ID);
#else
		FeedDog();	   //喂狗
#endif
		
	}
	while ( ( -- timecnt )> 0);

	return 1;
}

/**
*函数描述:以指定的字符分割字符串
*
*
*/
static int split(char *src, const char *separator, char **dest)
{
    char *pNext;
    int count = 0;
    
    if (src == NULL || strlen(src) == 0) return 0;
    if (separator == NULL || strlen(separator) == 0) return 0; 

    pNext = strtok(src,separator);
    
    while(pNext != NULL)
    {
        *dest++ = pNext;
        ++count;
        pNext = strtok(NULL,separator);
    }

    return count;
}

/*
*从字符数组中查找指定字符串
*
*返回值:成功为0 ，失败为1
*
*
*
*/
static u8 find_string(u8 * dataarray,u8 * dstchar,u8 datalen)
{

	u8 i = 0x00;
	u8 j = 0x00;

	u8 dstcharlen = strlen(dstchar);

	for (i = 0;i < datalen;i ++)
	{
	
		if (dataarray[i] == dstchar[j])
		{
			if ((++ j) == dstcharlen)
			{
				/*查找到指定字符串*/
				return 0;
			}
		}
		else
		{
			j = 0;
		}

	}

	return 1;
}

#if RTL8711_WIFI_MONITOR_ENABLE == 0x01

void rt8711_wifi_ctrl_pin_init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	/*配置gpio 管脚*/
	RCC_APB2PeriphClockCmd ( RCC_APB2Periph_GPIOC, ENABLE );
	GPIO_InitStructure.GPIO_Pin = RTL8711_WIFI_RESET_PIN|RTL8711_WIFI_RELOAD_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;		//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;		//IO口速度为10MHz
	GPIO_Init ( RTL8711_WIFI_RELOAD_PORT, &GPIO_InitStructure );					//根据设定参数初始

	GPIO_SetBits ( RTL8711_WIFI_RESET_PORT,RTL8711_WIFI_RESET_PIN );
	GPIO_SetBits ( RTL8711_WIFI_RELOAD_PORT,RTL8711_WIFI_RELOAD_PIN );

	return;

}
/**函数接口**/

/*
*
*初始化wifi模块
*
*参数:无
*返回值:无
*
*/
void rtl8711_wifi_module_init ( void )
{

	/*初始化超时默认时间*/
	memset ( &Rtl8711WifiStru,0,sizeof ( RTL8711_WIFI_STRUCT) );

	Rtl8711WifiStru.apconnstate = RTL8711_WIFI_APINIT;
	Rtl8711WifiStru.apwaitconntimeout = 30;

	Rtl8711WifiStru.moduleid = 123456789;
	Rtl8711WifiStru.networkprotocol = RTL8711_WIFI_TCPSERVER;

	Rtl8711WifiStru.tcpconnstate = RTL8711_WIFI_TCPINIT;
	Rtl8711WifiStru.tcpwaitconntimeout = 300;
	Rtl8711WifiStru.tcpwaitrecvtimeout = 600;//200ms *5 *60 *2 : 2分钟

	Rtl8711WifiStru.tcprecvdataflag = 0x00;

	Rtl8711WifiStru.passthroughflag = 0x00;
	Rtl8711WifiStru.workmode = RTL8711_WIFI_APSTA;

	Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;

	Rtl8711WifiStru.currentbaudrate = COMMON_BAUTRATE;
	Rtl8711WifiStru.resetflag = 0x01;

	rt8711_wifi_ctrl_pin_init();

	return ;

}

/*
*
*作用:模块硬件复位(拉低reset引脚、然后再拉高)
*
*
*
*
**/
void rtl8711_wifi_module_hard_reset( void )
{

	/*当模块上电时或者出现故障时， MCU 需要对模块做复位操作， 
	引脚拉低至少 0.5S， 然后拉高或悬空复位*/

	/*复位WiFi模块:低电平有效*/
	GPIO_ResetBits ( RTL8711_WIFI_RESET_PORT,RTL8711_WIFI_RESET_PIN);

	/*增加延时，等待模块重启成功*/
	delay_xs ( RTL8711_WIFI_MODULE_RESET_DELAY_TIME );
	
	GPIO_SetBits ( RTL8711_WIFI_RESET_PORT,RTL8711_WIFI_RESET_PIN );

	/*增加延时，等待模块重启成功*/
	delay_xs ( RTL8711_WIFI_MODULE_RESET_DELAY_TIME );

	Rtl8711WifiStru.resetflag = 0x00;
	Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_CONFIG;

	return ;
}


/*
*
*作用:通过复位命令 重启WiFi模块 AT+Z
*
*
*
*
**/
void rtl8711_wifi_module_soft_reset ( void )
{
	
	IWDG_ReloadCounter();	   //喂狗
	
	ttl_send_data ( "AT+Z\r",strlen ("AT+Z\r") );
	delay_xs ( 1 );
	
	if ( Rtl8711WifiStru.recvbufflen > 0 )
	{
	
		/*未连接*/
		if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok" ) != NULL )
		{
			Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_CONFIG;
		}
		else
		{
			Rtl8711WifiStru.resetflag = 0x01;
			Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;
		}
	}

	return ;
}

void rtl8711_wifi_module_reset( void )
{
	/*硬复位：直接控制复位引脚*/
	/*软复位：通过at指令控制复位*/

	switch(Rtl8711WifiStru.resetflag)
	{
		case 0x00:
		{
			rtl8711_wifi_module_soft_reset();
		}
		break;

		case 0x01:
		{
			rtl8711_wifi_module_hard_reset();
		}
		break;

		default:
		{
			rtl8711_wifi_module_hard_reset();
		}
		break;
		
	}

	memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen);
	Rtl8711WifiStru.recvbufflen = 0x00;
	Rtl8711WifiStru.recvbuffbusy = 0x00;
	
	return ;
}

/**函数接口**/

/*
*
*回复wifi模块出厂设置
*
*参数:无
*返回值:无
*
*将 PC14引脚拉低1-3s是启动simplelink配置 3s以上是恢复出厂设置
*/
void rtl8711_wifi_module_reload ( void )
	{
	
		GPIO_ResetBits ( RTL8711_WIFI_RELOAD_PORT,RTL8711_WIFI_RELOAD_PIN);
	
		/*等待恢复出厂设置成功*/
		delay_xs ( RTL8711_WIFI_MODULE_RELOAD_DELAY_TIME );
		
		GPIO_SetBits ( RTL8711_WIFI_RELOAD_PORT,RTL8711_WIFI_RELOAD_PIN );
		delay_xs (1);
	
		return ;
	}


/*
*
*作用:查询模块默认参数
*
*
*
*
**/
u32 rtl8711_wifi_module_get_default_param ( void )
{	
	int retrycnt = 0x00;
	int noackcnt = 0x00;

	/*系统重新上电，查询WiFi模块的工作模块，之后不在进行WiFi模块工作模式的查询*/
	if (systemrestflag == 0x00)
	{
		/*监测WiFi模块当前的工作模式:ap apsta sta，当WiFi模式未配置时处于ap模式，等待直到配置成功*/
			do
			{
		
			/*
			*
			*AT+WMODE
			*
			*+ok=APSTA
			*AP :模式下，每90分钟进行一次模块复位
			*
			*/
				
				/*AT+WMODE : AP /APSTA /STA */		
				ttl_send_data ( "AT+WMODE\r",strlen ( "AT+WMODE\r" ));
				delay_xs ( 1 );
		
				if ( Rtl8711WifiStru.recvbufflen > 0 )
				{
					/*未连接*/
					if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok=AP\r\n" ) != NULL )
					{
					
						Rtl8711WifiStru.workmode = RTL8711_WIFI_AP; 				
						
						retrycnt = 0x00;
						delay_xs ( 20 );
					}
				 
					else if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok=APSTA\r\n" ) != NULL )
					{
						Rtl8711WifiStru.workmode = RTL8711_WIFI_APSTA;					
						break;
					}
					else if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok=STA\r\n" ) != NULL )
					{
						Rtl8711WifiStru.workmode = RTL8711_WIFI_STA;
						break;
					}
					else if ( strstr ( Rtl8711WifiStru.recvbuff,"+ERR=-1\r\n" ) != NULL )
					{
						retrycnt = 0x00;
						delay_xs ( 2 );
					}

					memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
					Rtl8711WifiStru.recvbufflen = 0x00; 	
					Rtl8711WifiStru.recvbuffbusy = 0x00;
					
				}
		
			}
			while ( (retrycnt ++) <= 10 );
			
			if ( retrycnt > 10 )
			{
				return 1;
			}
			
			memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
			Rtl8711WifiStru.recvbufflen = 0x00;
			Rtl8711WifiStru.recvbuffbusy = 0x00;
		
			return 0;

	}
	
	return 0x00;
}


/*
*
*作用:查询网路链接状态:ap 和tcp
*
*
*
*
**/
int rtl8711_wifi_module_get_network_state ( void )
{

	u8 count = 0x00;
	u8 i = 0x00;
	
	int retrycnt = 0x00;
	int noackcnt = 0x00;

	u8 cmdbuff[64] = {0x00};

	/**/
	do
	{

		/*ap 链接:Disconnected / ssid /RF Off */
		ttl_send_data ( "AT+WSLK\r",strlen ( "AT+WSLK\r" ) );
		delay_xs ( 1 );

		if ( Rtl8711WifiStru.recvbufflen > 0 )
		{

			/*未连接*/
			if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok=DISCONNECTED\r\n" ) != NULL )
			{
				Rtl8711WifiStru.apconnstate = RTL8711_WIFI_APCONNERR;
			}
			else if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok=RF Off\r\n" ) != NULL )
			{
				Rtl8711WifiStru.apconnstate = RTL8711_WIFI_APCONNERR;
			}

			/*链接成功*/
			else
			{
				Rtl8711WifiStru.apconnstate = RTL8711_WIFI_APCONNOK;
				break;
			}

			noackcnt = 0x00;
			
			memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
			Rtl8711WifiStru.recvbufflen = 0x00;
			Rtl8711WifiStru.recvbuffbusy = 0x00;

		}
		else
		{
			/*发送指令，模块无响应计数*/
			if ( ( noackcnt ++ ) > 5 )
			{
				/*重启WiFi模块*/
				Rtl8711WifiStru.resetflag = 0x01;
				Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;
				
				return 1;
			}
		}
	}
	while ( (retrycnt ++) <= Rtl8711WifiStru.apwaitconntimeout );

	if ((retrycnt > Rtl8711WifiStru.apwaitconntimeout))
	{

		/*查找模块关联ap信息*/

		/*复位WiFi模块*/
		Rtl8711WifiStru.resetflag = 0x01;
		Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;

		return 1;
	}

	retrycnt = 0x00;
	noackcnt = 0x00;
	
	memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
	Rtl8711WifiStru.recvbufflen = 0x00;
	Rtl8711WifiStru.recvbuffbusy = 0x00;
	

	/*tcp 客户端链接状态*/
	do
	{
		/*tcp 链接:on /off */
		ttl_send_data ( "AT+TCPLK\r",strlen ( "AT+TCPLK\r" ) );
		delay_xs ( 1 );

		if ( Rtl8711WifiStru.recvbufflen > 0 )
		{

			/*未连接*/
			if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok=on\r\n" ) != NULL )
			{

				Rtl8711WifiStru.tcpconnstate = RTL8711_WIFI_TCPCONNOK;
				break;
			}
			else if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok=off\r\n" ) != NULL )
			{
				Rtl8711WifiStru.tcpconnstate = RTL8711_WIFI_TCPWAITCONN;
			}

			noackcnt = 0x00;

			memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
			Rtl8711WifiStru.recvbufflen = 0x00;
			Rtl8711WifiStru.recvbuffbusy = 0x00;

		}
		else
		{
			/*发送指令，模块无响应计数*/
			if ( ( noackcnt ++ ) > 5 )
			{
				/*重启WiFi模块*/
			
				Rtl8711WifiStru.resetflag = 0x01;
				Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;

				return 1;
			}
		}
	}
	while ( ( retrycnt ++ ) <= Rtl8711WifiStru.tcpwaitconntimeout);

	/*超时客户端未连接，重启模块*/
	if ( (retrycnt > Rtl8711WifiStru.tcpwaitconntimeout))
	{
		/*重启WiFi模块*/
	
	    Rtl8711WifiStru.resetflag = 0x00;
		Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;

		return 1;
	}

	noackcnt = 0x00;
	retrycnt = 0x00;
	
	memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
	Rtl8711WifiStru.recvbufflen = 0x00;
	Rtl8711WifiStru.recvbuffbusy = 0x00;

#if 1

	/*系统重新复位*/
	if (systemrestflag == 0x00)
	{
		do
		{
	
			/*AT+TCPTO=<time><CR>*/
			sprintf (cmdbuff,"AT+TCPTO=%u\r",RTL8711_WIFI_MODULE_AUTO_DISCONNECT_TIMEOUT);
			
			ttl_send_data ( cmdbuff,strlen (cmdbuff) );
			delay_xs ( 1 );
		
			if ( Rtl8711WifiStru.recvbufflen > 0 )
			{
	
				/*命令设置成功*/
				if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok\r\n" ) != NULL )
				{
					break;
				}
	
				noackcnt = 0x00;
				
				memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
				Rtl8711WifiStru.recvbufflen = 0x00;
				Rtl8711WifiStru.recvbuffbusy = 0x00;
			}
			else
			{
				/*发送指令，模块无响应计数*/
				if ( ( noackcnt ++ ) > 5 )
				{
					/*重启WiFi模块*/
					Rtl8711WifiStru.resetflag = 0x01;
					Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;
					
					return 1;
				}
			}
		}
		while ( ( retrycnt ++ ) <= 10 );
	
		/*尝试设置参数失败*/
		if ((retrycnt > 10))
		{
			/*重启WiFi模块*/
			Rtl8711WifiStru.resetflag = 0x00;
			Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;
			return 1;
		}
	
		noackcnt = 0x00;
		retrycnt = 0x00;
	
		memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
		Rtl8711WifiStru.recvbufflen = 0x00;
		Rtl8711WifiStru.recvbuffbusy = 0x00;


		/*关闭WiFi模块异常处理功能*/
		do
		{
			/*AT+MDCH=OFF<CR>*/
			ttl_send_data ( "AT+MDCH=OFF\r",strlen ("AT+MDCH=OFF\r") );			
			delay_xs ( 1 );	

			if ( Rtl8711WifiStru.recvbufflen > 0 )
			{
	
				/*命令设置成功*/
				if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok\r\n" ) != NULL )
				{
					break;
				}
	
				noackcnt = 0x00;
				
				memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
				Rtl8711WifiStru.recvbufflen = 0x00;
				Rtl8711WifiStru.recvbuffbusy = 0x00;
			}
			else
			{
				/*发送指令，模块无响应计数*/
				if ( ( noackcnt ++ ) > 5 )
				{
					/*重启WiFi模块*/
					Rtl8711WifiStru.resetflag = 0x01;
					Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;
					
					return 1;
				}
			}
		}
		while ( ( retrycnt ++ ) <= 10 );
	
		/*尝试设置参数失败*/
		if ((retrycnt > 10))
		{
			/*重启WiFi模块*/
			Rtl8711WifiStru.resetflag = 0x00;
			Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;
			
			return 1;
		}
	
		noackcnt = 0x00;
		retrycnt = 0x00;
	
		memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
		Rtl8711WifiStru.recvbufflen = 0x00;
		Rtl8711WifiStru.recvbuffbusy = 0x00;

		systemrestflag = 0x01;
	}
#endif


	/*退出 at 模式，进入透出模式*/
	/*WiFi模块接收到的网络数据帧只有进入到透传模式才会通过串口发送到mcu*/
	do
	{
		ttl_send_data ( "AT+ENTM\r",strlen ( "AT+ENTM\r" ));
		delay_xs ( 1 );

		if ( Rtl8711WifiStru.recvbufflen > 0 )
		{

			noackcnt = 0x00;

			if (find_string(Rtl8711WifiStru.recvbuff,"+ok\r\n\r\n",Rtl8711WifiStru.recvbufflen) == 0x00)
			{
				break;
			}
			
			memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
			Rtl8711WifiStru.recvbufflen = 0x00;
			Rtl8711WifiStru.recvbuffbusy = 0x00;

		}
		else
		{
			/*发送指令，模块无响应计数*/
			if ( ( noackcnt ++ ) > 5 )
			{
				/*重启WiFi模块*/
			
				Rtl8711WifiStru.resetflag = 0x01;
				Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;

				return 1;
			}
		}
	}
	while ( ( retrycnt ++ ) <= 10 );

	/*尝试设置参数失败*/
	if ((retrycnt > 10))
	{
		/*重启WiFi模块*/
		Rtl8711WifiStru.resetflag = 0x00;
		Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;
		
		return 1;
	}

	noackcnt = 0x00;
	retrycnt = 0x00;
	
	memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
	Rtl8711WifiStru.recvbufflen = 0x00;
	Rtl8711WifiStru.recvbuffbusy = 0x00;

	return 0;



}

void rtl8711_wifi_module_config ( void )
{

	int retrycnt = 0x00;

	/*清除透传标记*/
	Rtl8711WifiStru.passthroughflag = 0x00;

	memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen);
	Rtl8711WifiStru.recvbufflen = 0x00;
	Rtl8711WifiStru.recvbuffbusy = 0x00;
	

	/*进入at模式*/
	do
	{
		/*在发送+++之前的200ms不发送任何数据*/
		delay_ms ( 500 );
		
		ttl_send_data ( "+++",strlen ( "+++" ) );

		/*等待时间最长小于3s*/
		delay_xs ( 2 );

		if ( Rtl8711WifiStru.recvbufflen > 0 )
		{
			/*解析接收到的数据*/
			if ( strstr ( Rtl8711WifiStru.recvbuff,"a" ) != NULL )
			{
			
				memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen);
				Rtl8711WifiStru.recvbufflen = 0x00;
				Rtl8711WifiStru.recvbuffbusy = 0x00;

				ttl_send_data ( "a",strlen ( "a" ) );
				delay_xs ( 2 );

				if ( Rtl8711WifiStru.recvbufflen > 0 )
				{

					/*解析接收到的数据*/
					if ( strstr ( Rtl8711WifiStru.recvbuff,"+ok" ) != NULL )
					{
					
						memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
						Rtl8711WifiStru.recvbufflen = 0x00;
						Rtl8711WifiStru.recvbuffbusy = 0x00;

						break;
					}
					
					memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
					Rtl8711WifiStru.recvbufflen = 0x00;
					Rtl8711WifiStru.recvbuffbusy = 0x00;
				}
			}

			memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
			Rtl8711WifiStru.recvbufflen = 0x00;
			Rtl8711WifiStru.recvbuffbusy = 0x00;
		}
	}
	while ( ( retrycnt ++ ) <= RTL8711_WIFI_MODULE_SWITCH_ATCMD_TIMEOUT );

	/*串口数据接收超时*/
	if ( retrycnt > RTL8711_WIFI_MODULE_SWITCH_ATCMD_TIMEOUT )
	{
	
#if RTL8711_WIFI_MOUDLE_BAUTRATE_SWITCH_EN == 0x01

		if (Rtl8711WifiStru.currentbaudrate == 9600)
		{
			Rtl8711WifiStru.currentbaudrate = 115200;
		}
		else
		{
			Rtl8711WifiStru.currentbaudrate = 9600;
		}

		ttl_default_config(Rtl8711WifiStru.currentbaudrate);
		
		bautrateswitchcnt ++;
#else	
		bautrateswitchcnt = 0x02;
#endif
	
		/*两种波特率通信都不正常*/
		if (bautrateswitchcnt >= 0x02)
		{

			bautrateswitchcnt = 0x00;
			Rtl8711WifiStru.resetflag = 0x01;
			
			goto RECEIVE_ACK_TIMEOUT;
		}
		else
		{
			/*切换波特率进行再次尝试*/
			Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_CONFIG;
			return ;
		}
		
	}

	retrycnt = 0x00;

	/*基本参数:*/
	if ( 1 )
	{
		/*ttl 通信失败*/
		if (rtl8711_wifi_module_get_default_param() != 0x00)
		{
			/*重启WiFi模块*/
			Rtl8711WifiStru.resetflag = 0x01;
			goto RECEIVE_ACK_TIMEOUT;
		
			return ;
		}
	}

	/*链接:ap 链接、tcp */
	if ( 1 )
	{
		if ( rtl8711_wifi_module_get_network_state() == 0x00 )
		{

#if DEBUG_NET_CONNECT== 1
		
			/*打印进入透传状态*/
			Usart3_SendByte('n');
			Usart3_SendByte('e');
			Usart3_SendByte('t');
			
			Usart3_SendByte(' ');
			Usart3_SendByte('o');
			Usart3_SendByte('k');
			Usart3_SendByte(Rtl8711WifiStru.passthroughflag + 0x30);
			Usart3_SendByte(Tim_Out + 0x30);
			Usart3_SendByte('\n');

			Usart3_SendByte(nodatarecvcnt >> 24);
			Usart3_SendByte(nodatarecvcnt >> 16);
			Usart3_SendByte(nodatarecvcnt >> 8);
			Usart3_SendByte(nodatarecvcnt >> 0);

			Usart3_SendByte(Rtl8711WifiStru.tcprecvdataflag + 0x30);
			Usart3_SendByte(Rtl8711WifiStru.modulecurrentstate + 0x30);
			
			Usart3_SendByte('\n');
#endif	

			/*客户端链接正常，进行数据接收处理*/
			Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_WORKING;

			/*设置透传标记*/
			Rtl8711WifiStru.passthroughflag = 0x01;
			
			return;
		}
	}

RECEIVE_ACK_TIMEOUT:


#if	DEBUG_NET_CONNECT == 0x01
	uart_printf("串口通讯无响应，复位WiFi通讯模块",strlen("串口通讯无响应，复位WiFi通讯模块"));
#endif

	/*重启WiFi模块*/
	Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_RESET;

	return ;
}

void rtl8711_wifi_module_net_monitor ( void )
{

		/*接收到数据*/
		if ( Rtl8711WifiStru.tcprecvdataflag == 0x01 )
		{
			Rtl8711WifiStru.tcprecvdataflag = 0x00;
			nodatarecvcnt = 0x00;
		}
	
		/*记录接收超时时间*/
		else if ( ( nodatarecvcnt ++ ) >= ( Rtl8711WifiStru.tcpwaitrecvtimeout))
		{
			nodatarecvcnt = 0x00;
			Rtl8711WifiStru.modulecurrentstate = RTL8711_WIFI_MODULE_CONFIG;
		}

	return	;
}

/*
*
*作用:解析接收到的数据
*
*
*
*
*
*
*********/
void receive_data_anaylise ( void )
{

}

/*
*
*WiFi模块数据处理接口
*
*
*
*
*/
void rtl8711_wifi_module_handle ( void )
{

	switch ( Rtl8711WifiStru.modulecurrentstate )
	{

		case RTL8711_WIFI_MODULE_RESET:
		{
			rtl8711_wifi_module_reset();
		}
		break;

		case RTL8711_WIFI_MODULE_CONFIG:
		{
			rtl8711_wifi_module_config();
		}
		break;

		case RTL8711_WIFI_MODULE_WORKING:
		{
			rtl8711_wifi_module_net_monitor();
		}
		break;

		default:
			break;

	}
	return ;
}

/************************************************************************************************************************/
/************************************************************************************************************************/

/********串口数据打印*****/
#if (DEBUG_NET_CONNECT== 1) && (SERIAL_WIFI_COMMUNICATION_ENABLE == 0x01)
void uart_printf(unsigned char *data,unsigned char len)
{
	unsigned char i = 0x00;

	for (i = 0x00;i < len;i ++)
	{
		Usart3_SendByte(data[i]);
	}

	return ;
}
#else
void uart_printf(unsigned char *data,unsigned char len){}
#endif

#if SERIAL_WIFI_COMMUNICATION_ENABLE == 0x01

/*数据接收发送:串口*/
void ttl_recv_data ( u8* buf,u32 len )
{

	/*判断已经接到的数据是否已经处理*/
	//if (Rtl8711WifiStru.recvbuffbusy == 0x00)
	{
		if ((Rtl8711WifiStru.recvbufflen + len) > sizeof(Rtl8711WifiStru.recvbuff))
		{
			memset ( Rtl8711WifiStru.recvbuff,0,Rtl8711WifiStru.recvbufflen );
			Rtl8711WifiStru.recvbufflen = 0x00;
		}

		memcpy ( Rtl8711WifiStru.recvbuff + Rtl8711WifiStru.recvbufflen,buf,len );
		Rtl8711WifiStru.recvbufflen += len;
		
		//Rtl8711WifiStru.recvbuffbusy = 0x01;
	}

	return ;
}

void ttl_send_data ( u8* buf,u32 len )
{

	Rtl8711WifiStru.recvbuffbusy = 0x00;

	Send_Num = len;
	memcpy ( PointToSendBuf ,buf,Send_Num);

	USART_ITConfig ( USART3, USART_IT_TC, ENABLE );
	return ;
}

/*
 * 函数名：ttl_default_config
 * 描述  ：重新配置串口波特率
 * 输入  ：无
 * 输出  : 无
 * 调用  ：外部调用
 */
void ttl_default_config(u32 baudrate)
{
	
	USART_InitTypeDef USART_InitStructure;

	USART_DeInit(USART3);	
	
	/* USART3 mode config */
	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART3, &USART_InitStructure); 
	
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART3, ENABLE);
}
#else
/*数据接收发送:串口*/
void ttl_recv_data ( u8* buf,u32 len ){}
void ttl_send_data ( u8* buf,u32 len ){}
void ttl_default_config(u32 baudrate){}

#endif

#else

/**函数接口**/
void rtl8711_wifi_module_init(void){}
void rtl8711_wifi_module_handle(void){}

/*数据接收发送:串口*/
void ttl_recv_data(u8 *buf,u32 len){}
void ttl_send_data(u8 *buf ,u32 len){}

#endif
