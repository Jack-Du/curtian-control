#ifndef _RTL8711_WIFI_DRIVER_H
#define _RTL8711_WIFI_DRIVER_H

#include "stm32f10x.h"
#include "config.h"

#define DEBUG_NET_CONNECT 0x00

/*数据结束符号*/
#define MCU_ENDCHAR "\r\n"
#define RTL8711_WIFI_ENDCHAR "\r\n\r\n"

/*使能波特率切换:在115200 和9600 之间进行切换*/
#define RTL8711_WIFI_MOUDLE_BAUTRATE_SWITCH_EN 0x00

/*RTL8711 控制管脚定义*/
#define RTL8711_WIFI_RESET_PIN 	GPIO_Pin_13//PC13
#define RTL8711_WIFI_RESET_PORT GPIOC//PC13
#define RTL8711_WIFI_RELOAD_PIN GPIO_Pin_14//PC14
#define RTL8711_WIFI_RELOAD_PORT GPIOC//PC14

/*rtl8711 通信管脚定义*/
#define RTL8711_WIFI_TTL_TX_PIN GPIO_Pin_10//PB10 /UART3_TX
#define RTL8711_WIFI_TTL_TX_PORT GPIOB

#define RTL8711_WIFI_TTL_RX_PIN GPIO_Pin_11//PB11 /UART3_RX
#define RTL8711_WIFI_TTL_RX_PORT GPIOB

#define RTL8711_WIFI_MODULE_SWITCH_ATCMD_TIMEOUT 30
#define RTL8711_WIFI_MODULE_RESET_DELAY_TIME 2
#define RTL8711_WIFI_MODULE_RELOAD_DELAY_TIME 5

#define RTL8711_WIFI_MODULE_AUTO_DISCONNECT_TIMEOUT 0 //单位:s(60 - 600 0:关闭) (无数据接收等待超时时间)

#define TX_RX_BUFF_MAX_LEN 0X100

/*rtl8711 数据存储结构*/
typedef struct
{

	u8 recvbuff[TX_RX_BUFF_MAX_LEN];
	u8 sendbuff[TX_RX_BUFF_MAX_LEN];
	
	u32 recvbufflen;
	u32 sendbufflen;

	u8 recvbuffbusy ;
	u8 sendbuffbusy ;
	
	u32 currentbaudrate;
	
	/*基本参数*/
	u8 workmode ;		/*工作模式:ap/sta/apsta*/
	u8 version[32] ;	/*固件版本号*/
	u8 networkprotocol;	/*网络协议tcpclient /tcpserver/udpclient/udpserver*/
	u32 moduleid;		/*模块id*/
	u8 ssid[32];			/*ssid:最长32字节*/
	u8 passwd[64];			/*密码:最长64字节:密码空:NONE*/

	/*tcp 相关:socka 参数,sockb 未使用*/
	u32 tcpwaitrecvtimeout;	/*链接无数据接收超时时间:超时之后tcpserver 主动断开当前的链接*/
							/*tcpclient 客户端重新链接server*/

	u8 tcprecvdataflag ;
	u32 tcpwaitconntimeout;	/*等待客户端链接超时*/
	u8	tcpconnstate;		/*tcp 链接状态:已连接、等待链接、等待链接超时、已断开*/	
	
	/*WiFi模块ap相关*/
	u8 apconnstate;			/*链接ap状态:等待链接、已连接、初始化状态*/
	u32 apwaitconntimeout ;	/*链接ap超时计数*/

	u8 modulecurrentstate;			/*模块当前状态 :自检、正常运行*/		
	u8 passthroughflag ;			/*透出标记*/
	u8 resetflag;					/*复位标记:标志是硬复位还是软复位: 0 :软复位 1:硬复位*/
	
}RTL8711_WIFI_STRUCT;

/*工作模式*/
typedef enum
{

	RTL8711_WIFI_AP = 0X01,
	RTL8711_WIFI_STA = 0X02,
	RTL8711_WIFI_APSTA = 0X03,
	
}RTL8711_WIFI_WORK_MODE;

/*网络协议*/
typedef enum
{

	RTL8711_WIFI_TCPCLIENT = 0X01,
	RTL8711_WIFI_TCPSERVER = 0X02,
	RTL8711_WIFI_UDPCLIENT = 0X03,
	RTL8711_WIFI_UDPSERVER = 0X04,
	
}RTL8711_WIFI_NET_PROT;

/*tcp 链接状态*/
typedef enum
{

	RTL8711_WIFI_TCPINIT = 0X01,
	RTL8711_WIFI_TCPWAITCONN = 0X02,
	RTL8711_WIFI_TCPCONNOK = 0X03,
	RTL8711_WIFI_TCPCONNERR = 0X04,
	RTL8711_WIFI_TCPDISCONN = 0X05,
	
}RTL8711_WIFI_TCPCONN_STATE;


/*ap 链接状态*/
typedef enum
{

	RTL8711_WIFI_APINIT = 0X01,
	RTL8711_WIFI_APWAITCONN = 0X02,
	RTL8711_WIFI_APCONNOK = 0X03,	
	RTL8711_WIFI_APCONNERR = 0X04,
	RTL8711_WIFI_APDISCONN = 0X05,
	
}RTL8711_WIFI_APCONN_STATE;

/*模块状态*/
typedef enum
{
	RTL8711_WIFI_MODULE_RESET = 0X01,
	RTL8711_WIFI_MODULE_CONFIG = 0X02,
	RTL8711_WIFI_MODULE_WORKING = 0X03,
	
}RTL8711_WIFI_MODULE_CURRENT_STATE;


/*声明结构体变量*/
extern RTL8711_WIFI_STRUCT Rtl8711WifiStru;

/**函数接口**/
extern void rtl8711_wifi_module_init(void);
extern void rtl8711_wifi_module_handle(void);

/*数据接收发送:串口*/
extern void ttl_recv_data(u8 *buf,u32 len);
extern void ttl_send_data(u8 *buf ,u32 len);

static	u8 delay_xs ( int timecnt );
#endif
