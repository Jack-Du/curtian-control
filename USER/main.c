/*例程网络参数*/
//网关：192.168.1.1
//掩码:	255.255.255.0
//物理地址：0C 29 AB 7C 00 01
//本机IP地址:192.168.1.199
//端口0的端口号：5000
//端口0的目的IP地址：192.168.1.1
//端口0的目的端口号：8899

#include "stm32f10x.h"		
#include "W5500.h"			
#include <string.h>
#include "RTU.h" 
#include "out.h"    //控制继电器  
#include "HWLED.H"  
#include "ext_in.h" 
#include "config.h"
#include "bsp_usart3.h"
#include "w5500_net_handle.h"
#include "serial_comm.h"
#include "system_start_info.h"
#include "rtl8711_wifi_driver.h"
#include "os.h"
#include "task_function.h"
#include "wdg.h"
#include "rtc_timer.h"

void RCC_Configuration(void);		//设置系统时钟为72MHZ(这个可以根据需要改)
void System_Initialization(void);	//STM32系统初始化函数(初始化STM32时钟及外设)

void  test_flash(void);

/*******************************************************************************
* 函数名  : W5500_Initialization
* 描述    : W5500初始货配置
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 无
*******************************************************************************/
void W5500_Initialization(void)
{
	W5500_Init();		  //初始化W5500寄存器函数 定义了W5500的中断reg mac ip 等等
	Detect_Gateway();	//检查网关服务器就是获取 目标的MAC地址
	Socket_Init(0);		//指定Socket(0~7)初始化,初始化端口0
}

/*******************************************************************************
* 函数名  : Load_Net_Parameters
* 描述    : 装载网络参数
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 网关、掩码、物理地址、本机IP地址、端口号、目的IP地址、目的端口号、端口工作模式
*******************************************************************************/
void Load_Net_Parameters(void)
{
	
	Gateway_IP[0] = DEFAULT_GATEWAY_1;//加载网关参数
	Gateway_IP[1] = DEFAULT_GATEWAY_2;
	Gateway_IP[2] = DEFAULT_GATEWAY_3;
	Gateway_IP[3] = DEFAULT_GATEWAY_4;

	Sub_Mask[0]=DEFAULT_MASK_1;//加载子网掩码
	Sub_Mask[1]=DEFAULT_MASK_2;
	Sub_Mask[2]=DEFAULT_MASK_3;
	Sub_Mask[3]=DEFAULT_MASK_4;

	Phy_Addr[0]=0x0c;//加载物理地址
	Phy_Addr[1]=0x29;
	Phy_Addr[2]=0xab;
	Phy_Addr[3]=0x7c;
	Phy_Addr[4]=0x00;
	Phy_Addr[5]=0x03;

	IP_Addr[0]=DEFAULT_IP_1;//加载本机IP地址
	IP_Addr[1]=DEFAULT_IP_2;
	IP_Addr[2]=DEFAULT_IP_3;
	IP_Addr[3]=DEFAULT_IP_4;

	S0_Port[0] = 0x22;//加载端口0的端口号8899 
	S0_Port[1] = 0xc3;

//	S0_DIP[0]=192;//加载端口0的目的IP地址
//	S0_DIP[1]=168;
//	S0_DIP[2]=1;
//	S0_DIP[3]=190;
//	
//	S0_DPort[0] = 0x17;//加载端口0的目的端口号6000
//	S0_DPort[1] = 0x70;

	S0_Mode=TCP_SERVER;//加载端口0的工作模式,TCP服务端模式

	
}

/*******************************************************************************
* 函数名  : W5500_Socket_Set
* 描述    : W5500端口初始化配置
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 分别设置4个端口,根据端口工作模式,将端口置于TCP服务器、TCP客户端或UDP模式.
*			从端口状态字节Socket_State可以判断端口的工作情况
*******************************************************************************/
void W5500_Socket_Set(void)
{
	if(S0_State==0)//端口0初始化配置
	{
		if(S0_Mode==TCP_SERVER)//TCP服务器模式 
		{
			if(Socket_Listen(0)==TRUE)
				S0_State=S_INIT;
			else
				S0_State=0;
		}
		else if(S0_Mode==TCP_CLIENT)//TCP客户端模式 
		{
			if(Socket_Connect(0)==TRUE)
				S0_State=S_INIT;
			else
				S0_State=0;
		}
		else//UDP模式 
		{
			if(Socket_UDP(0)==TRUE)
				S0_State=S_INIT|S_CONN;
			else
				S0_State=0;
		}
	}
}


void RCC_Configuration(void)
{
  	/* Enable GPIOA GPIOB SPI1 and USART1 clocks */
  	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB
					| RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD
					| RCC_APB2Periph_AFIO, ENABLE);    
}


 
/*******************************************************************************
* 函数名  : System_Initialization
* 描述    : STM32系统初始化函数(初始化STM32时钟及外设)
* 输入    : 无
* 输出    : 无
* 返回    : 无 
* 说明    : 无
*******************************************************************************/
void System_Initialization(void)
{ 
	RCC_Configuration();		//设置系统时钟为72MHZ(这个可以根据需要改)
  	delay_init();
}

#if DEBUG_FLASH == 0x01
void  test_flash(void)
{
	uint8_t i = 0x00;
	uint8_t Len = 0x04;
	
	uint8_t DateBuff[64]= {0x00};
	uint8_t buff[64]= {0x00};

	FlashReadNByte(BOOT_LOADER_SOFT_VERSION_ADDR,DateBuff,Len);
		
	hextostring(buff,DateBuff,Len);
	
	usart_printf_string("\n");
	usart_printf_string(buff);
	usart_printf_string("\n");

}
#else
void  test_flash(void){}
#endif

/****
*打印版本信息
*	硬件版本信息
*	loader 版本信息
*	应用程序版本信息
*
*
*
***********/
void printf_version_info(void)
{

	u8 loaderversion[2] ={0x00};

	/*打印版本信息*/
	usart_printf_string("\nboard:");
	usart_printf_string(BoardVersion);
	
	/*打印版本信息:从 flash 读取 loader信息*/
	FlashReadNByte(BOOT_LOADER_SOFT_VERSION_ADDR,loaderversion,0x01);
	FlashReadNByte(BOOT_LOADER_SOFT_VERSION_ADDR + 4,loaderversion + 0x01,0x01);

	sprintf(LoaderVersion,"%d.%d",loaderversion[0],loaderversion[1]);
	usart_printf_string("\nloader:");
	usart_printf_string(LoaderVersion);

	/*打印版本信息*/
	usart_printf_string("\napp:");
	usart_printf_string(SoftVersion);

	/*应用程序 编译时间信息*/
	usart_printf_string("\nbuildTime:");
	usart_printf_string(BuildDataStr);
	usart_printf_string(" ");
	usart_printf_string(BuildTimeStr);
	usart_printf_string("\n");
	
}


int main(void)
{ 

#if SYSTEM_SUPPORT_OS == 0x01
	OS_ERR err;
	CPU_SR_ALLOC();
#endif

	System_Initialization();	//STM32系统初始化函数(初始化STM32时钟及外设)    
	FlashInit();
	
	GPIO_OUT_C_Config();      //磁保持继电器控制引脚	
  	Confing_HWLED();          //红外LED配置	  
	EXTI_Pin_Config();        //配置引脚中断

	Serial_Comm_Init();
	ModInit(0x01);

	printf_version_info();

	init_system_start_info();
	init_config_info();
	
	test_flash();

	init_rtc_time();
	SecondTime = Get_Time_T();
	DebugOut(0,"\r\n\r\n\r\nStart :%d-%d-%d   %d:%d:%d \r\n",RtcTime.year+2000,RtcTime.month,RtcTime.day,RtcTime.hour,RtcTime.minute,RtcTime.second);

#if SYSTEM_SUPPORT_OS == 0x01
	SoftWDTInit();

	OSInit(&err);		//初始化UCOSIII
	OS_CRITICAL_ENTER();//进入临界区
	//创建任务
	OSTaskCreate((OS_TCB 	* )&StartTaskTCB,		//任务控制块
				 (CPU_CHAR	* )"start task", 		//任务名称
                 (OS_TASK_PTR )start_task, 			//入口
                 (void		* )0,					//参数
                 (OS_PRIO	  )START_TASK_PRIO,     //优先级
                 (CPU_STK   * )&START_TASK_STK[0],	//堆栈基地值
                 (CPU_STK_SIZE)START_STK_SIZE/10,	//堆栈深度限位
                 (CPU_STK_SIZE)START_STK_SIZE,		//堆栈大小
                 (OS_MSG_QTY  )0,					//任务内部消息队列能够接收到的最大消息数目，为0 禁止接收消息
                 (OS_TICK	  )0,					//当使能时间片轮转时的时间片长度，为0 时默认时间长度
                 (void   	* )0,					//补充存储区，
                 (OS_OPT      )OS_OPT_TASK_STK_CHK|OS_OPT_TASK_STK_CLR, //选项
                 (OS_ERR 	* )&err);				//错误返回值
	OS_CRITICAL_EXIT();	//退出临界区 
	OSStart(&err);  //开启任务
	
	while(1);
#else

	WatchDogInit(IWDG_Prescaler_64,3126); //40k/64 == 0.625k  t= 1/0.625= 1.6ms  tm = 3125* 0.0016 = 5s  

	while (1)
	{	
		FeedDog();		//喂狗 
		
		w5500_dev_handle();
		system_start_info_handle();

		relay_ctrl_handle();

		rtl8711_wifi_module_handle();

		if(FJPG_HW_Send_Bit == 0x02)
		{
			 SendData_Fjpg(FJPG_HW_TAB);	 // 发送红外数据 风机盘管空调 SendData_hw(FJPG_HW_TAB); 	// 发送红外数据 风机盘管空调
			 FJPG_HW_Send_Bit = 0;
		}
		
		if(GL_HW_Send_Bit == 0x02)
		{
		 	SendData_Gl(GL_HW_TAB);		 // 发送红外数据 格力空调 
		 	GL_HW_Send_Bit = 0;
		}	
		if(Haier_HW_Send_Bit == 0x02)
		{
		 	SendData_Haier(Haier_HW_TAB);	 // 发送红外数据 海尔空调 
		 	Haier_HW_Send_Bit = 0;
		}	
		
		if ( GLx_HW_Send_Bit == 0x02 )
		{
			SendData_Glx ( Glx_HW_TAB );	//
			GLx_HW_Send_Bit = 0;
		}
		
		if ( Md_HW_Send_Bit == 0x02 )
		{
			SendData_Midea ( Md_HW_TAB ,Md_HW_Send_Type);	//
			Md_HW_Send_Bit = 0;
		}
		delay_ms(200);

	}
#endif
}

