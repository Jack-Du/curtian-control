#include "serial_comm.h"
#include "rtl8711_wifi_driver.h"

#include "bsp_usart3.h"
#include "RTU.h"


/*函数功能用于Modbus初始化
 函数输入Id为Modbus站号。
 函 数 输 出  无 。
*/
//modbus用通讯参数
//这个 3.5字符时间间隔 的含义是每 一帧数据之间的间隔是 3.5个字符
u8 Tim_Out;//大于3.5个字符时间保守取3ms (波特率9600的时候大约2点几//毫秒)
u8 Rcv_Complete;//一帧是否已经接受完成
u8 Rcv_Buffer[256];//用来存放接收到的完整的一帧数据 第一个字节用来存放接收到的有效字节数也就是数组中的有效字节数
u8 Rcv_Num;//用来表示接收的一帧的有效字节数从功能码到CRC校验
u8  HaveMes; //接收
u8 Comu_Busy;//用来表示能否接收下一帧数据
u8* PointToRcvBuf;//用来指向接收的数据缓存
u8* PointToSendBuf;//用来指向带发送的数据缓存
u8 Send_Complete;//一帧是否已经发送完成
u8 Com_busy;//通讯繁忙表示上一帧还未处理结束
u8 Send_Buffer[256];//用来存放待发送的完整的一帧数据第一个字节用来存放待发送的有效字节数也就是数组中的有效字节数
u8 Rcv_Data;//用来存放接收的一个字节
u8 Send_Data1bit;//用来存放要发送的一字节
u8 Mod_Id;//用来标志作为从站的站号
u8 Send_Num;//用来表示待发送的一帧的字节数
u8 Sent_Num;//代表已经发送的数据


#if SERIAL_COMMUNICATION_ENABLE == 0x01

//modbus 专用定时器
void Serial_Comm_TIM_config ( void );
void Serial_Comm_NVIC_config ( void ); //中断向量初始化
void ModInit ( u8 Id );
void ModSend ( void );
void ModRcv ( void );


/*****
***
*
*串口初始化 : 初始化 串口配置
*					 中断 :如果使用串口进行WiFi通信的话
*
*
*
*
**********************************/
void Serial_Comm_Init(void)
{
	/*引脚初始化*/
	USART3_Config();
	
	/*中断配置 :使用串口进行WiFi通信的话 进行配置*/
	Serial_Comm_TIM_config();
	Serial_Comm_NVIC_config();

	return ;
}

#if SERIAL_WIFI_COMMUNICATION_ENABLE == 0x01

//modbus 专用定时器
void Serial_Comm_TIM_config ( void )
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_DeInit ( TIM3 );
	RCC_APB1PeriphClockCmd ( RCC_APB1Periph_TIM3, ENABLE );
	TIM_TimeBaseStructure.TIM_Period=300;//ARR的值重装值
	TIM_TimeBaseStructure.TIM_Prescaler=3600-1;    //预计分频值
	TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up; //向上计数模式
	TIM_TimeBaseInit ( TIM3, &TIM_TimeBaseStructure );
	TIM_ITConfig ( TIM3,TIM_IT_Update,ENABLE );        //使能 tim中断状态
	TIM_Cmd ( TIM3, ENABLE );                          //开启时钟
}

void Serial_Comm_NVIC_config ( void ) //中断向量初始化
{
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_PriorityGroupConfig ( NVIC_PriorityGroup_2 );                    //选择中断分组1
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;                     //选择串口1中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;             //抢 0  (中断嵌套)
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;                    //响应式中断优先级设置为0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                       //使能中断
	NVIC_Init ( &NVIC_InitStructure );

	NVIC_InitStructure.NVIC_IRQChannel =TIM3_IRQn;                        //指定中断源
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;              //抢 0  (中断嵌套)
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;                    // 指定响应优先级别1
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init ( &NVIC_InitStructure );
}

// 函数功能Modbus专用定时器TIM3
// 这个中断 是发生在 数据接收完成以后的 然后再 处理接收到的数据 准备把处理完的数据发送出去！ 
void TIM3_IRQHandler(void)            
{ 

	uint8_t ret = 0x00;
	
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) //检查指定的TIM中断发//生与否:TIM 中断源 
	{ 
	  TIM_ClearITPendingBit(TIM3, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源 
	  TIM_Cmd(TIM3,DISABLE);     //关闭定时器2
	  TIM_SetCounter(TIM3,0);    //设置计数器的值为 0
	  
	  Tim_Out=1;                 //表示超过了 3.5个字符时间间隔

	  Rcv_Complete=1;            //接收一帧数据完成

#if RTL8711_WIFI_MONITOR_ENABLE == 0x01
		/*at 命令模式下，串口数据接收处理*/
		if (Rtl8711WifiStru.passthroughflag == 0x00)
		{
			if(HaveMes != 0)
			{
				
				Comu_Busy = 0x01;
				ttl_recv_data(PointToRcvBuf,Rcv_Num);

				/*buff[0]存放设备id，作为透传模式下接收数据的比较:区分不同的数据接收对象*/
				Rcv_Buffer[0]=Mod_Id; 

				/*buff[0]存放设备id，作为透传模式下发送到外部的id标识*/
				Send_Buffer[0]=Mod_Id;//站号

				Rcv_Num = 0x00;
				HaveMes = 0x00;

				Comu_Busy = 0x00;
			}
		}
		else
#endif
		{	
			if(HaveMes != 0&&Rcv_Num > 4)  //如果 接收到了数据 并且 接收到一帧数据的有效字节数 大于4
			{ 
				Comu_Busy = 1;  //是否可以接收下一帧数据标志位  设为 1表示不能接收下一帧数据
				//因为要处理数据了所以不能接收下一帧数据！

#if RTL8711_WIFI_MONITOR_ENABLE == 0x01
				/*buff[1]存放设备id，作为透传模式下发送到外部的id标识*/
//				Send_Buffer[1]=Mod_Id;//站号
#endif
				
				ret = MessageHandle(PointToRcvBuf,PointToSendBuf,Rcv_Num,&Send_Num);//把接收到的数据进行处理 
				if (ret != 0x00)
				{
					HaveMes=0;
					Comu_Busy = 0x00;
					
					return ;
				}

				HaveMes=0;

				//启动数据发送
				USART_ITConfig ( USART3, USART_IT_TC, ENABLE ); //开启数据传输完成中断	

			} 
	}
  }  
} 

void USART3_IRQHandler(void)
{	
if(USART_GetITStatus(USART3,USART_IT_RXNE)!=RESET)  
 { 
	 ModRcv();    //用于数据接收 	 
 }  

 if(USART_GetITStatus(USART3,USART_IT_TC)!=RESET) 
  {  
	ModSend();//用于数据发送 发送一帧数据 
  } 	 
}

/************************************************************************************************
******
******
******
******
***********************************************************************************************/
void ModInit ( u8 Id )
{

	//Serial_Comm_Init();

	/*串口数据存储结构 初始化
	**
	**串口接收 发送缓冲区大小
	**串口当前发送 接收数据数量
	**串口接收 发送状态
	**
	**
	**
	**
	*****/
	//modbus参数初始化
	PointToRcvBuf=Rcv_Buffer;	//接收缓存
	PointToSendBuf=Send_Buffer; //发送缓存
	
	Send_Num=0;//发送的数据顺序输出数组的第几个数
	Sent_Num = 0x00;
	Mod_Id=Id;//站号设置

	Rcv_Buffer[0]=Mod_Id;
	Send_Buffer[0]=Mod_Id;//站号

	Comu_Busy=0;
	
	rtl8711_wifi_module_init();
}

/* 函数功能用于Modbus信息发送
函数输入无。
函数输出无。
这个函数是 用 发送数据完成中断标志来产生中断  然后发送完数据就停止中断
*/
void ModSend ( void )  //产生一次发送中断就发一次数据
{

	Send_Data1bit=* ( PointToSendBuf+Sent_Num );  //PointToSendBuf 为 发送数据缓存寄存器
	USART_SendData ( USART3,Send_Data1bit );      //发送数据函数 一次次发送一个字节

	Sent_Num++;
	if ( Sent_Num>= ( Send_Num ))      //如果发送完成了就初始化标志
	{
		Comu_Busy=0;                   //可以再接收下一帧数据
		Rcv_Num=0;                     //接收 索引 初始化为 0
		Send_Num=0;                    //发送 索引 初始化为 1
		Sent_Num = 0x00;
		
		USART_ITConfig ( USART3, USART_IT_TC, DISABLE ); //关闭数据发送中断
	}

}
/* 函数功能用于Modbus信息接收
函数输入无。
函 数 输 出  无
所谓的超个3.5个字符的时间 其实就是在一帧数据发送完以后和另外一帧数据之间的间隔
*/
void ModRcv ( void )
{

	HaveMes=1;//表示接收到了信息
	
#if RTL8711_WIFI_MONITOR_ENABLE == 0x01
	/*设置接收到数据帧标记*/
	Rtl8711WifiStru.tcprecvdataflag = 0x01;
#endif

	Rcv_Data=USART_ReceiveData ( USART3 );
	if ( Comu_Busy != 1 ) //可以接收下一帧信息
	{

		TIM_Cmd ( TIM3, DISABLE ); //关闭定时器2
		TIM_SetCounter ( TIM3,0 ); //将定时器初值变成0

		//Tim_Out = 1 表示 超过了 3.5个字符时间 默认是 0	 一上电定时器就工作然后置超时标志 如果
#if RTL8711_WIFI_MONITOR_ENABLE == 0x01
		if ( Rtl8711WifiStru.passthroughflag == 0x00 )
		{
			if ( ( Tim_Out!=0 ) ) //如果间隔时间超过了3.5个字符则认为接收开始
			{
				Rcv_Complete=0;//表示数据帧接收开始
				Rcv_Num=0;//接收数据个数初始化

				/*Rcv_Buffer:第一个字节存放接收的字节长度，第二个存放模块id，从第三个字节开始存放接收到的有效数据*/
				Rcv_Buffer[Rcv_Num]=Rcv_Data;//将数据放入接收数组中

				Rcv_Num++;//同时个数加一
			}

			if (( 0 == Tim_Out ) && ( 0==Rcv_Complete ) ) //如果处于接收一帧的正常过程中
			{
				if ( Rcv_Num<250 )  //小于 250 字节
				{
					Rcv_Buffer[Rcv_Num]=Rcv_Data;//将数据放入接收数组中
					Rcv_Num++;//同时个数加一
				}
				else
				{
					/*buff[0]存放设备id，作为透传模式下接收数据的比较:区分不同的数据接收对象*/
					Rcv_Buffer[0]=Mod_Id; 

					/*buff[0]存放设备id，作为透传模式下发送到外部的id标识*/
					Send_Buffer[0]=Mod_Id;//站号
					
					Rcv_Num = 0x00;
					HaveMes=0;//清除信息位
				}
			}
						
		}
		else
#endif
		{

			if (( Tim_Out!=0 ) && ( Rcv_Data==Mod_Id ) ) //如果间隔时间超过了3.5个字符同时接受的字节和自己的站号一致则认为接收开始
			{	
			
				Rcv_Complete=0;//表示数据帧接收开始
				Rcv_Num=0;//接收数据个数初始化
				Rcv_Num++;//同时个数加一
			}

			if (( 0 == Tim_Out ) && ( 0==Rcv_Complete ) ) //如果处于接收一帧的正常过程中
			{
				if ( Rcv_Num<100 )  //小于 100个字节
				{
					Rcv_Buffer[Rcv_Num]=Rcv_Data;//将数据放入接收数组中
					Rcv_Num++;//同时个数加一
				}
				else
				{
					Rcv_Complete=1;
					Comu_Busy=1;
					
					* ( PointToSendBuf+1 ) =* ( PointToRcvBuf+1 ); //获取功能码
					
					ErrorHandle ( 6,PointToSendBuf, &Send_Num); //表示超出了字节数(从机设备忙碌)
					USART_ITConfig ( USART3, USART_IT_TC, ENABLE ); //开启传输数据完成中断
					
					HaveMes=0;//清除信息位
					Rcv_Num=0;
				}
			}
		}

		//接收完一个字节的数据时 就把超时 3.5个字符 标志设为 0 表示没有超时
		//然后再开启定时器 因为是 串口中断和定时器中断 所以发一帧数据 就进入串口中断
		//定时器虽然启动了但是  如果是正常的接收一帧数据 串口中断会比定时器中断先发生 所以又把定时器给关了。
		//也就出现了 这个函数
		Tim_Out=0;
		TIM_Cmd ( TIM3, ENABLE ); //开启15ms计时三个半字符的保守估计

	}
}
#else
//modbus 专用定时器
void Serial_Comm_TIM_config ( void ){}
void Serial_Comm_NVIC_config ( void ){}
void TIM3_IRQHandler(void)            {}
void USART3_IRQHandler(void){}

void ModInit ( u8 Id ){
	//Serial_Comm_Init();
}
void ModSend ( void ){}
void ModRcv ( void ){}

#endif
#else
//modbus 专用定时器

void Serial_Comm_Init(void){}

void Serial_Comm_TIM_config ( void ){}
void Serial_Comm_NVIC_config ( void ){}
void TIM3_IRQHandler(void)            {}
void USART3_IRQHandler(void){}

void ModInit ( u8 Id ){}
void ModSend ( void ){}
void ModRcv ( void ){}


#endif
