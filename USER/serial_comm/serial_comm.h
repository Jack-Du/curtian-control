#ifndef _SERIAL_COMM_H
#define _SERIAL_COMM_H

#include "stm32f10x.h"
#include "config.h"

/**********************************************wifi 串口通信********************************************/
/**********************************************wifi 串口通信********************************************/

/*使能串口通信*/
#define SERIAL_COMMUNICATION_ENABLE 0X01


/*函数功能用于Modbus初始化
 函数输入Id为Modbus站号。
 函 数 输 出  无 。
*/
//modbus用通讯参数
//这个 3.5字符时间间隔 的含义是每 一帧数据之间的间隔是 3.5个字符
extern u8 Tim_Out;//大于3.5个字符时间保守取3ms (波特率9600的时候大约2点几//毫秒)
extern u8 Rcv_Complete;//一帧是否已经接受完成
extern u8 Rcv_Buffer[256];//用来存放接收到的完整的一帧数据 第一个字节用来存放接收到的有效字节数也就是数组中的有效字节数
extern u8 Rcv_Num;//用来表示接收的一帧的有效字节数从功能码到CRC校验
extern u8  HaveMes; //接收
extern u8 Comu_Busy;//用来表示能否接收下一帧数据
extern u8* PointToRcvBuf;//用来指向接收的数据缓存
extern u8* PointToSendBuf;//用来指向带发送的数据缓存
extern u8 Send_Complete;//一帧是否已经发送完成
extern u8 Com_busy;//通讯繁忙表示上一帧还未处理结束
extern u8 Send_Buffer[256];//用来存放待发送的完整的一帧数据第一个字节用来存放待发送的有效字节数也就是数组中的有效字节数
extern u8 Rcv_Data;//用来存放接收的一个字节
extern u8 Send_Data1bit;//用来存放要发送的一字节
extern u8 Mod_Id;//用来标志作为从站的站号
extern u8 Send_Num;//用来表示待发送的一帧的字节数
extern u8 Sent_Num;//代表已经发送的数据

/*串口通信控制初始化*/
/*串口配置：控制端口 ，中断*/
/*定时器配置   :定时器中断*/

extern void Serial_Comm_Init(void);

extern void TIM3_IRQHandler(void);
extern void USART3_IRQHandler(void);

extern void ModInit ( u8 Id );
extern void ModSend ( void );
extern void ModRcv ( void );

/**/
/**********************************************wifi 串口通信 end********************************************/

#endif
