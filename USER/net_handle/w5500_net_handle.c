#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "w5500_net_handle.h"
#include "system_start_info.h"
#include "W5500.h"	
#include "flash.h"
#include "delay.h"
#include "wdg.h"
#include "rtc_timer.h"

NET_INFO_STRUCT NetInfoStruct = {0x00};
SOCKET_INFO_STRUCT SocketInfo[SOCKET_NUM] = {0x00};

static uint8_t handlestep = 0x00;

uint16_t InfoCounter = 0x00;
uint8_t InfoBuffer[256] = {0x00};
uint8_t  InfoLen = 0x00;

/*
*timecnt :x(s)延时
*
*
*
*/
static	u8 delay_xs ( int timecnt )
{
	do
	{
		delay_ms ( 1000 );

#if SYSTEM_SUPPORT_OS == 0x01
		SoftWdtFeed(ETHERNET_COMMUNICATION_TASK_ID);
#else
		FeedDog();	   //喂狗
#endif

	}
	while ( ( -- timecnt )> 0);

	return 1;
}

#if ENABLE_W5500_NET == 0x01

void w5500_load_net_parameters(void);
void w5500_initialization(void);
void init_default_net_info();



/*******************************************************************************
* 函数名  : Process_Socket_Data
* 描述    : W5500接收并发送接收到的数据
* 输入    : s:端口号
* 输出    : 无
* 返回值  : 无
* 说明    : 本过程先调用S_rx_process()从W5500的端口接收数据缓冲区读取数据,
*			然后将读取的数据从Rx_Buffer拷贝到Temp_Buffer缓冲区进行处理。
*			处理完毕，将数据从Temp_Buffer拷贝到Tx_Buffer缓冲区。调用S_tx_process()
*			发送数据。
*******************************************************************************/
void Process_Socket_Data(SOCKET s);

/*******************************************************************************
* 函数名  : Process_Socket_Data
* 描述    : W5500接收并发送接收到的数据
* 输入    : s:端口号
* 输出    : 无
* 返回值  : 无
* 说明    : 本过程先调用S_rx_process()从W5500的端口接收数据缓冲区读取数据,
*			然后将读取的数据从Rx_Buffer拷贝到Temp_Buffer缓冲区进行处理。
*			处理完毕，将数据从Temp_Buffer拷贝到Tx_Buffer缓冲区。调用S_tx_process()
*			发送数据。
*******************************************************************************/
void Process_Udp_Socket_Data(SOCKET s);


/*w5500 基本配置步骤:
*** 配置管脚: 	    W5500_SPI_Configuration();		//W5500 SPI初始化配置(STM32 SPI1)
****			W5500_GPIO_Configuration();	  //W5500 GPIO初始化配置	
*** 加载网络参数
***
***				Load_Net_Parameters();		//装载网络参数    /////////////////////////////配IP 	
***				W5500_Hardware_Reset();		//硬件复位W5500
***				W5500_Initialization();		//W5500初始货配置   
***			  	IINCHIP_WRITE(Sn_KPALVTR(0),0X02);
*/

/*网络参数配置*/
/*初始化: 读取flash 网络参数
**设置sock
**开启数据传输
***/
void init_net_info(void)
{
	memset(&NetInfoStruct,0,sizeof(NET_INFO_STRUCT));

	/*从flash中读取 存储的网络参数*/
	if (get_system_start_info_recover_net_flag() == 0x01)
	{
		set_system_start_info_recover_net_flag(0x00);
		set_net_modify_model((uint8_t)NET_RECOVER_BY_HAND);

		init_default_net_info();
		write_net_info();
	}
	else
	{
		read_net_info();
	}
}


/*设置默认的网络参数*/
void init_default_net_info()
{
	
	w5500_usart_printf_string("\n。。。读取默认网络参数。。。\n"); 	

	NetInfoStruct.ipaddr.byte[0] = DEFAULT_IP_1;//加载网关参数
	NetInfoStruct.ipaddr.byte[1] = DEFAULT_IP_2;
	NetInfoStruct.ipaddr.byte[2] = DEFAULT_IP_3;
	NetInfoStruct.ipaddr.byte[3] = DEFAULT_IP_4;

	NetInfoStruct.netmask.byte[0] = DEFAULT_MASK_1;//加载子网掩码
	NetInfoStruct.netmask.byte[1] = DEFAULT_MASK_2;
	NetInfoStruct.netmask.byte[2] = DEFAULT_MASK_3;
	NetInfoStruct.netmask.byte[3] = DEFAULT_MASK_4;


	NetInfoStruct.gateway.byte[0] = DEFAULT_GATEWAY_1;//加载本机IP地址
	NetInfoStruct.gateway.byte[1] = DEFAULT_GATEWAY_2;
	NetInfoStruct.gateway.byte[2] = DEFAULT_GATEWAY_3;
	NetInfoStruct.gateway.byte[3] = DEFAULT_GATEWAY_4;

	/*mac地址 第一位必须为 偶数*/
	NetInfoStruct.mac[0].byte[0] = DEFAULT_PHY_1;	//mac地址
	NetInfoStruct.mac[0].byte[1] = DEFAULT_PHY_2;
	NetInfoStruct.mac[0].byte[2] = DEFAULT_PHY_3;
	NetInfoStruct.mac[0].byte[3] = DEFAULT_PHY_4;
	NetInfoStruct.mac[1].byte[0] = DEFAULT_PHY_5;
	NetInfoStruct.mac[1].byte[1] = DEFAULT_PHY_6;
	
}

/*从flash读取配置信息*/
void read_net_info(void )
{

	Flash_Date ip;
	Flash_Date gateway;
	Flash_Date mask;
	Flash_Date mac[2] = {{0x00},{0x00}};

	/*读取IP地址*/
	FlashReadNByte(LOCAL_IP_ADDRESS,ip.byte,sizeof(ip));

	w5500_usart_printf_string("ip:");	
	w5500_usart_printf_int(ip.byte[0]);
	w5500_usart_printf_string(".");	
	w5500_usart_printf_int(ip.byte[1]);
	w5500_usart_printf_string(".");	
	w5500_usart_printf_int(ip.byte[2]);
	w5500_usart_printf_string(".");
	w5500_usart_printf_int(ip.byte[3]);

	w5500_usart_printf_string("\n");
	
	/*判读IP地址的合法性*/
	if (((ip.byte[0] == 0xff) && (ip.byte[1] == 0xff)) || ((ip.byte[0] == 0x00) || (ip.byte[1] == 0x00)))
	{
		
		set_net_modify_model((uint8_t)NET_NOT_CONFIG );
		init_default_net_info();
		return ;
	}

	/*mask*/
	w5500_usart_printf_string("mask:");	
	FlashReadNByte(LOCAL_MASK_ADDRESS,mask.byte,sizeof(mask));
	w5500_usart_printf_int(mask.byte[0]);
	w5500_usart_printf_string(".");	
	w5500_usart_printf_int(mask.byte[1]);
	w5500_usart_printf_string(".");	
	w5500_usart_printf_int(mask.byte[2]);
	w5500_usart_printf_string(".");
	w5500_usart_printf_int(mask.byte[3]);
	w5500_usart_printf_string("\n");
	if ((mask.byte[0] == 0xff) && (mask.byte[1] == 0xff) &&(mask.byte[2] == 0xff) && (mask.byte[3] == 0xff))
	{
		set_net_modify_model((uint8_t)NET_NOT_CONFIG);

		init_default_net_info();
		return ;
	}

	
	/*gateway*/
	w5500_usart_printf_string("gateway:");	
	FlashReadNByte(LOCAL_GATEWAY_ADDRESS,gateway.byte,sizeof(gateway));
	w5500_usart_printf_int(gateway.byte[0]);
	w5500_usart_printf_string(".");	
	w5500_usart_printf_int(gateway.byte[1]);
	w5500_usart_printf_string(".");	
	w5500_usart_printf_int(gateway.byte[2]);
	w5500_usart_printf_string(".");
	w5500_usart_printf_int(gateway.byte[3]);
	w5500_usart_printf_string("\n");
	if ((gateway.byte[0] == 0xff) && (gateway.byte[1] == 0xff))
	{
		set_net_modify_model((uint8_t)NET_NOT_CONFIG);

		init_default_net_info();
		return ;
	}

	/*mac*/
	w5500_usart_printf_string("mac:");	
	FlashReadNByte(LOCAL_MAC_ADDRESS,(uint8_t *)mac,sizeof(mac)-0x02);
	w5500_usart_printf_int(mac[0].byte[0]);
	w5500_usart_printf_string(":");	
	w5500_usart_printf_int(mac[0].byte[1]);
	w5500_usart_printf_string(":");	
	w5500_usart_printf_int(mac[0].byte[2]);
	w5500_usart_printf_string(":");
	w5500_usart_printf_int(mac[0].byte[3]);
	w5500_usart_printf_string(":");
	w5500_usart_printf_int(mac[1].byte[0]);
	w5500_usart_printf_string(":");
	w5500_usart_printf_int(mac[1].byte[1]);
	w5500_usart_printf_string("\n");
	if (((NetInfoStruct.mac[0].byte[0] == 0xff) && (NetInfoStruct.mac[0].byte[1] == 0xff) &&(NetInfoStruct.mac[0].byte[2] == 0xff) \
		&& (NetInfoStruct.mac[0].byte[3] == 0xff) && (NetInfoStruct.mac[1].byte[0] == 0xff) && (NetInfoStruct.mac[1].byte[1] == 0xff)) \
		|| (((NetInfoStruct.mac[0].byte[0] % 2) != 0x00) && (NetInfoStruct.mac[0].byte[0] != 0x000)))
	{
		set_net_modify_model((uint8_t)NET_NOT_CONFIG);

		init_default_net_info();
		return ;
	}
	
	NetInfoStruct.gateway.byte[0] = gateway.byte[0];//加载网关参数
	NetInfoStruct.gateway.byte[1] = gateway.byte[1];
	NetInfoStruct.gateway.byte[2] = gateway.byte[2];
	NetInfoStruct.gateway.byte[3] = gateway.byte[3];

	NetInfoStruct.netmask.byte[0] = mask.byte[0];//加载子网掩码
	NetInfoStruct.netmask.byte[1] = mask.byte[1];
	NetInfoStruct.netmask.byte[2] = mask.byte[2];
	NetInfoStruct.netmask.byte[3] = mask.byte[3];


	NetInfoStruct.ipaddr.byte[0] = ip.byte[0];//加载本机IP地址
	NetInfoStruct.ipaddr.byte[1] = ip.byte[1];
	NetInfoStruct.ipaddr.byte[2] = ip.byte[2];
	NetInfoStruct.ipaddr.byte[3] = ip.byte[3];

	NetInfoStruct.mac[0].byte[0] = mac[0].byte[0];//加载物理地址
	NetInfoStruct.mac[0].byte[1] = mac[0].byte[1];
	NetInfoStruct.mac[0].byte[2] = mac[0].byte[2];
	NetInfoStruct.mac[0].byte[3] = mac[0].byte[3];
	NetInfoStruct.mac[1].byte[0] = mac[1].byte[0];
	NetInfoStruct.mac[1].byte[1] = mac[1].byte[1];

}

/***********************
*****
***	将修改的信息存储到flash
****
**** 写入之前 先进行数据合理性检测
*****		  每次写之后都进行读取操作，确保写入成功
*****
*****
*****
***********************************/
void write_net_info(void)
{
	uint8_t ret = 0x00;
	uint8_t i = 0x00;
	uint8_t retry = 0x00;
	uint8_t notsameflag = 0x00;

	/*先备份原来的数据*/
	Flash_Date ip;
	Flash_Date gateway;
	Flash_Date mask;
	Flash_Date mac[2] = {{0x00},{0x00}};

	Flash_Date backip;
	Flash_Date backgateway;
	Flash_Date backmask;
	Flash_Date backmac[2] = {{0x00},{0x00}};

	/*写入之前进行数据合理性 判断，是否存在不合理地址*/

	/*读取IP地址*/
	FlashReadNByte(LOCAL_IP_ADDRESS,backip.byte,sizeof(backip));
	FlashReadNByte(LOCAL_MASK_ADDRESS,backmask.byte,sizeof(backmask));
	FlashReadNByte(LOCAL_GATEWAY_ADDRESS,backgateway.byte,sizeof(backgateway));
	FlashReadNByte(LOCAL_MAC_ADDRESS,(uint8_t *)backmac,sizeof(backmac)-0x02);

	/**判读要写入的数据 是否和已经存在的一致*/
	for(i = 0x00;i < 4;i ++)
	{
		if (ip.byte[i] != NetInfoStruct.ipaddr.byte[i])
		{
			ret = 0x01;
			break;
		}
	}

	if (ret != 0x00)
	{
		notsameflag = 0x01;
	}
	ret = 0x00;

	for(i = 0x00;i < 4;i ++)
	{
		if (gateway.byte[i] != NetInfoStruct.gateway.byte[i])
		{
			ret = 0x01;
			break;
		}
	}

	if (ret != 0x00)
	{
		notsameflag = 0x01;
	}
	ret = 0x00;

	for(i = 0x00;i < 4;i ++)
	{
		if (mask.byte[i] != NetInfoStruct.netmask.byte[i])
		{
			ret = 0x01;
			break;
		}
	}

	if (ret != 0x00)
	{
		notsameflag = 0x01;
	}
	ret = 0x00;

	for(i = 0x00;i < 4;i ++)
	{
		if (mac[0].byte[i] != NetInfoStruct.mac[0].byte[i])
		{
			ret = 0x01;
			break;
		}
	}

	if (ret != 0x00)
	{
		notsameflag = 0x01;
	}
	ret = 0x00;

	for(i = 0x00;i < 4;i ++)
	{
		if (mac[1].byte[i] != NetInfoStruct.mac[1].byte[i])
		{
			ret = 0x01;
			break;
		}
	}

	if (ret != 0x00)
	{
		notsameflag = 0x01;
	}

	/*要写入的数据和已经存在的数据相同 ，则不再次进行写操作*/
	if (notsameflag == 0x00)
	{
		w5500_usart_printf_string("\n。。。设备网络信息未修改。。。\n");		
		return ;
	}

	
	for (retry = 0x00; retry < 0x03;retry ++)
	{
		ret = 0x00;
			
		/*先写在读 确定写入正确*/
		FlashWriteData(LOCAL_IP_ADDRESS,(uint32_t )NetInfoStruct.ipaddr.byte,sizeof(NetInfoStruct.ipaddr.byte));
		FlashWriteData(LOCAL_GATEWAY_ADDRESS,(uint32_t )NetInfoStruct.gateway.byte,sizeof(NetInfoStruct.gateway.byte));
		FlashWriteData(LOCAL_MASK_ADDRESS,(uint32_t )NetInfoStruct.netmask.byte,sizeof(NetInfoStruct.netmask.byte));
		FlashWriteData(LOCAL_MAC_ADDRESS,(uint32_t )NetInfoStruct.mac[0].byte,sizeof(NetInfoStruct.mac) - 0x02);
	
		FlashReadNByte(LOCAL_IP_ADDRESS,ip.byte,sizeof(ip));
		FlashReadNByte(LOCAL_MASK_ADDRESS,mask.byte,sizeof(mask));
		FlashReadNByte(LOCAL_GATEWAY_ADDRESS,gateway.byte,sizeof(gateway));
		FlashReadNByte(LOCAL_MAC_ADDRESS,(uint8_t *)mac,sizeof(mac)-0x02);
	
		/*判读写入和读取的数据是否一致*/
		for(i = 0x00;i < 4;i ++)
		{
			if (ip.byte[i] != NetInfoStruct.ipaddr.byte[i])
			{
				ret = 0x01;
				break;
			}
		}
	
		if (ret != 0x00)
		{
			continue;
		}
		ret = 0x00;
	
		for(i = 0x00;i < 4;i ++)
		{
			if (gateway.byte[i] != NetInfoStruct.gateway.byte[i])
			{
				ret = 0x01;
				break;
			}
		}
	
		if (ret != 0x00)
		{
			continue;
		}
		ret = 0x00;
	
		for(i = 0x00;i < 4;i ++)
		{
			if (mask.byte[i] != NetInfoStruct.netmask.byte[i])
			{
				ret = 0x01;
				break;
			}
		}
	
		if (ret != 0x00)
		{
			continue;
		}
		ret = 0x00;
	
		for(i = 0x00;i < 4;i ++)
		{
			if (mac[0].byte[i] != NetInfoStruct.mac[0].byte[i])
			{
				ret = 0x01;
				break;
			}
		}
	
		if (ret != 0x00)
		{
			continue;
		}
		ret = 0x00;
	
		for(i = 0x00;i < 4;i ++)
		{
			if (mac[1].byte[i] != NetInfoStruct.mac[1].byte[i])
			{
				ret = 0x01;
				break;
			}
		}
	
		if (ret != 0x00)
		{
			continue;
		}
		ret = 0x00;
		break;

	}
	
	
	w5500_usart_printf_string("\n。。。设备网络信息已修改。。。\n"); 	
	return ;
}

/**********************************************************************************************************************************/
/**********************************************************************************************************************************/

void init_sock_info_by_num(uint8_t sock)
{

	if (sock > SOCKET_NUM)
	{
		return ;
	}
	
	switch(sock)
	{
		case 0x00:
			{
				/*端口:8899 tcp 服务器*/
				SocketInfo[sock].sock = sock;
				SocketInfo[sock].mode = TCP_SERVER;
				SocketInfo[sock].Port[0] = 0x22;
				SocketInfo[sock].Port[1] = 0xc3;
				SocketInfo[sock].enable = 0x01;

				//设置分片长度，参考W5500数据手册，该值可以不修改	
				Write_W5500_SOCK_2Byte(SocketInfo[sock].sock, Sn_MSSR, 1460);//最大分片字节数=1460(0x5b4)

				//设置端口0的端口号
				Write_W5500_SOCK_2Byte(SocketInfo[sock].sock, Sn_PORT, SocketInfo[sock].Port[0]*256+SocketInfo[sock].Port[1]); 
			}
			break;

		case 0x01:
			{
			
				/*端口9999 udp */
				SocketInfo[sock].sock = sock;
				SocketInfo[sock].mode = UDP_MODE;
				
				SocketInfo[sock].Port[0] = 0x27;
				SocketInfo[sock].Port[1] = 0x0f;
				SocketInfo[sock].enable = 0x01;
					
				
				//设置分片长度，参考W5500数据手册，该值可以不修改	
				Write_W5500_SOCK_2Byte(SocketInfo[sock].sock, Sn_MSSR, 1460);//最大分片字节数=1460(0x5b4)
				
				//设置端口0的端口号
				Write_W5500_SOCK_2Byte(SocketInfo[sock].sock, Sn_PORT, SocketInfo[sock].Port[0]*256+SocketInfo[sock].Port[1]);		

			}
			break;

		case 0x02:
			break;

		case 0x03:
			break;

		case 0x04:
			break;

		case 0x05:
			break;

		case 0x06:
			break;

		case 0x07:
			break;

		default:break;
	}

}

/*******************************************************************************
* 函数名  : Socket_Init
* 描述    : 指定Socket(0~7)初始化
* 输入    : s:待初始化的端口
* 输出    : 无
* 返回值  : 无
* 说明    : 无
*******************************************************************************/
void init_sock_info(void)
{

	memset(SocketInfo,0,sizeof(SOCKET_INFO_STRUCT)*SOCKET_NUM);

	init_sock_info_by_num(0x00);
	init_sock_info_by_num(0x01);
	
	return ;
}


/**********************************************************************************************************************************/
/**********************************************************************************************************************************/

/*******************************************************************************
* 函数名  : 
* 描述    : W5500 :初始化w5500 通讯引脚 
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 无
*
*******************************************************************************/
void w5500_comm_init(void)
{
	/*********w5500模块初始化*********/
	W5500_SPI_Configuration();		//W5500 SPI初始化配置(STM32 SPI1)
	W5500_GPIO_Configuration(); 	//W5500 GPIO初始化配置 
	
	W5500_NVIC_Configuration(); 	//W5500 接收引脚中断优先级设置
}

/*******************************************************************************
* 函数名  : Load_Net_Parameters
* 描述    : 装载网络参数
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 网关、掩码、物理地址、本机IP地址、端口号、目的IP地址、目的端口号、端口工作模式
*******************************************************************************/
void w5500_load_net_parameters(void)
{
	
	Gateway_IP[0] = NetInfoStruct.gateway.byte[0];//加载网关参数
	Gateway_IP[1] = NetInfoStruct.gateway.byte[1];
	Gateway_IP[2] = NetInfoStruct.gateway.byte[2];
	Gateway_IP[3] = NetInfoStruct.gateway.byte[3];

	Sub_Mask[0]=NetInfoStruct.netmask.byte[0];//加载子网掩码
	Sub_Mask[1]=NetInfoStruct.netmask.byte[1];
	Sub_Mask[2]=NetInfoStruct.netmask.byte[2];
	Sub_Mask[3]=NetInfoStruct.netmask.byte[3];

	Phy_Addr[0]=NetInfoStruct.mac[0].byte[0];//加载物理地址
	Phy_Addr[1]=NetInfoStruct.mac[0].byte[1];
	Phy_Addr[2]=NetInfoStruct.mac[0].byte[2];
	Phy_Addr[3]=NetInfoStruct.mac[0].byte[3];
	Phy_Addr[4]=NetInfoStruct.mac[1].byte[0];
	Phy_Addr[5]=NetInfoStruct.mac[1].byte[1];

	IP_Addr[0]=NetInfoStruct.ipaddr.byte[0];//加载本机IP地址
	IP_Addr[1]=NetInfoStruct.ipaddr.byte[1];
	IP_Addr[2]=NetInfoStruct.ipaddr.byte[2];
	IP_Addr[3]=NetInfoStruct.ipaddr.byte[3];

}

/*******************************************************************************
* 函数名  : W5500_Hardware_Reset
* 描述    : 硬件复位W5500
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : W5500的复位引脚保持低电平至少500us以上,才能重围W5500
*******************************************************************************/
void w5500_hardware_reset(void)
{
	uint8_t delaycnt = 0x00;
	
	GPIO_ResetBits(W5500_RST_PORT, W5500_RST);//复位引脚拉低
	delay_ms(50);
	GPIO_SetBits(W5500_RST_PORT, W5500_RST);//复位引脚拉高
	delay_ms(200);

	//等待以太网连接完成
	do
		{
			delaycnt++;
			if ((Read_W5500_1Byte(PHYCFGR)&LINK) == LINK)
			{
				break;
			}
			delay_xs(1);
		}
	while (delaycnt < 30);
	
	//while((Read_W5500_1Byte(PHYCFGR)&LINK)==0);//
}


/*******************************************************************************
* 函数名  : W5500_Read_Phy_status
* 描述    : phy 连接检测
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 0 ：未连接      1:已连接
*******************************************************************************/
uint8_t W5500_Read_Phy_Link_Status(void)
{
	return ((Read_W5500_1Byte(PHYCFGR)&LINK)==0x01);
}


/*******************************************************************************
* 函数名  : W5500_Initialization
* 描述    : W5500初始货配置
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 无
*******************************************************************************/
void w5500_initialization(void)
{
	W5500_Init();		  //初始化W5500寄存器函数 定义了W5500的中断reg mac ip 等等
	Detect_Gateway();		//检查网关服务器就是获取 目标的MAC地址
	init_sock_info();
}

/*******************************************************************************
* 函数名  : W5500_Interrupt_Process
* 描述    : W5500中断处理程序框架
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 无
* 为什么？清除中断就在对应的位置上写 1
这里的[R]，而不是[R/W] 表示不能由主机写‘1’让W5500产生中断，
只能由主机设置‘1’ ，清除某一位中断。
*******************************************************************************/
void w5500_interrupt_process(void)
{
	unsigned char i,j;
	
	uint8_t k = 0x00;
	uint8_t cnt = 0x00;
	
IntDispose:
	W5500_Interrupt=0;//清零中断标志
	i = Read_W5500_1Byte(IR);//读取中断标志寄存器 
	//i = 1000 0000
	Write_W5500_1Byte(IR, (i&0xf0));//回写清除中断标志 
	//  1111 000
	if((i & CONFLICT) == CONFLICT)//IP地址冲突异常处理
	{
		 //自己添加代码
	}

	if((i & UNREACH) == UNREACH)//UDP模式下地址无法到达异常处理
	{
		//自己添加代码
	}

	
	i=Read_W5500_1Byte(SIR);  //读取端口中断标志寄存器	
	Write_W5500_1Byte(SIR,i); //回写清除中断标志 
	
	w5500_usart_printf_string("\n等待处理接收数据\n");
	w5500_usart_printf_int(i);
	w5500_usart_printf_string("\n");
	
	for (k = 0x00;k < SOCKET_NUM;k ++)
	{
		if((i & (1<<k)) == (1<<k))//是否是Socket0触发的中断 
		{
			j=Read_W5500_SOCK_1Byte(SocketInfo[k].sock,Sn_IR);//Socket中断类型
			Write_W5500_SOCK_1Byte(SocketInfo[k].sock,Sn_IR,j); //把
			
			if(j&IR_CON)//在TCP模式下,Socket0成功连接 
			{
				SocketInfo[k].initstate|=S_CONN;//网络连接状态0x02,端口完成连接，可以正常传输数据
				
				w5500_usart_printf_string("\nsock ");
				w5500_usart_printf_int(SocketInfo[k].sock);
				w5500_usart_printf_string("网络连接成功\n");
			}
			if(j&IR_DISCON)//在TCP模式下Socket断开连接处理
			{
				Write_W5500_SOCK_1Byte(SocketInfo[k].sock,Sn_CR,CLOSE);//关闭端口,等待重新打开连接 
				init_sock_info_by_num(SocketInfo[k].sock); 	//指定Socket(0~7)初始化,初始化端口0
				SocketInfo[k].initstate=0; 	  		//网络连接状态0x00,端口连接失败

				w5500_usart_printf_string("\nsock ");
				w5500_usart_printf_int(SocketInfo[k].sock);
				w5500_usart_printf_string("客户端关闭连接，等待重新连接\n");

			}
			if(j&IR_SEND_OK)	//Socket0数据发送完成,可以再次启动S_tx_process()函数发送数据 
			{
				SocketInfo[k].datastate|=S_TRANSMITOK;//端口发送一个数据包完成 
			}
			if(j&IR_RECV)	   //Socket接收到数据,可以启动S_rx_process()函数 
			{
				SocketInfo[k].datastate|=S_RECEIVE;//端口接收到一个数据包

				w5500_usart_printf_string("\nsock ");
				w5500_usart_printf_int(SocketInfo[k].sock);
				w5500_usart_printf_string("接收到数据\n");
			}
			if(j&IR_TIMEOUT)//Socket连接或数据传输超时处理 
			{
				Write_W5500_SOCK_1Byte(SocketInfo[k].sock,Sn_CR,CLOSE);// 关闭端口,等待重新打开连接 

				SocketInfo[k].initstate=0;//网络连接状态0x00,端口连接失败 
				SocketInfo[k].timeout = 0x01;

				w5500_usart_printf_string("\nsock ");
				w5500_usart_printf_int(SocketInfo[k].sock);
				w5500_usart_printf_string("连接超时 ，网络关闭\n");
			}

			break;
		}		
	}

	if(Read_W5500_1Byte(SIR) != 0)   //如果这个寄存器不等于0 就表示用中断产生一直循环检测是那个中断
	{
		goto IntDispose;
	}
}

/*******************************************************************************
* 函数名  : W5500_Socket_Set
* 描述    : W5500端口初始化配置
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 分别设置4个端口,根据端口工作模式,将端口置于TCP服务器、TCP客户端或UDP模式.
*			从端口状态字节Socket_State可以判断端口的工作情况
*******************************************************************************/
void w5500_socket_set(void)
{
	uint8_t i = 0x00;

	for (i = 0x00;i < SOCKET_NUM;i ++)
	{
		if (SocketInfo[i].enable == 0x00)
		{
			continue;
		}

		if (SocketInfo[i].initstate != 0x00)
		{
			continue;
		}
		
		if(SocketInfo[i].mode==TCP_SERVER)//TCP服务器模式 
		{
			if(Socket_Listen(SocketInfo[i].sock)==TRUE)
				SocketInfo[i].initstate =S_INIT;
			else
				SocketInfo[i].initstate =0;
		}
		else if(SocketInfo[i].mode==TCP_CLIENT)//TCP客户端模式 
		{
			if(Socket_Connect(SocketInfo[i].sock)==TRUE)
				SocketInfo[i].initstate =S_INIT;
			else
				SocketInfo[i].initstate =0;
		}
		else//UDP模式 
		{
			if(Socket_UDP(SocketInfo[i].sock)==TRUE)
				SocketInfo[i].initstate=S_INIT|S_CONN;
			else
				SocketInfo[i].initstate=0;
		}
	}
}

/*******************************************************************************
* 函数名  : w5500_socket_timeout
* 描述    : W5500端口连接或者数据传输超时
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 分别设置4个端口,根据端口工作模式,将端口置于TCP服务器、TCP客户端或UDP模式.
*			从端口状态字节Socket_State可以判断端口的工作情况
*******************************************************************************/
uint8_t w5500_socket_timeout(void)
{
	uint8_t i = 0x00;
	uint8_t ret = 0x00;

	for (i = 0x00;i < SOCKET_NUM;i ++)
	{
		if (SocketInfo[i].enable == 0x00)
		{
			continue;
		}
		if (SocketInfo[i].timeout == 0x01)
		{
			ret = 0x01;
			break;
		}
	}

	return ret;
}


/**********************
*
*void sock_recv_handle()
*	处理接收数据
*
*
*
*
*
*****************************/
void sock_recv(void)
{

	uint8_t i = 0x00;
	for (i = 0x00;i < SOCKET_NUM;i ++)
	{
		switch(SocketInfo[i].mode)
		{
			case TCP_SERVER:
				{
					/*客户端未连接到该服务器*/
					if (SocketInfo[i].initstate == S_INIT)
					{
						w5500_usart_printf_string("sock 0 ....等待网络连接");
						
						SocketInfo[i].waitconntimeout ++;
						SocketInfo[i].recvdatatimeout = 0x00;
						
						if (SocketInfo[i].waitconntimeout > 600) /*两分钟无客户端连接*/
						{
							SocketInfo[i].waitconntimeout = 0x00;
							SocketInfo[i].timeout = 0x01;
						}
						continue;
					}

					if (SocketInfo[i].initstate == (S_CONN |S_INIT))
					{

						SocketInfo[i].waitconntimeout = 0x00;

#if ENABLE_W5500_RECV_TIMEOUT_CHECK == 0x01
						SocketInfo[i].recvdatatimeout ++;	
						if (SocketInfo[i].recvdatatimeout > W5500_RECV_TIMEOUT) /*10分钟无客户端连接*/
						{
							SocketInfo[i].recvdatatimeout = 0x00;
							SocketInfo[i].timeout = 0x01;
						}
#endif						
						Process_Socket_Data(SocketInfo[i].sock);	//W5500接收数据处理函数 
					}
				}
				break;
			case UDP_MODE:
				{
					if (SocketInfo[i].initstate == (S_CONN |S_INIT))
					{
						Process_Udp_Socket_Data(SocketInfo[i].sock);
					}
				}
				break;

			default:
				break;
				
		}
	}
}

/**************************w5500生命周期***************************/
/*****
*
*w5500处理流程
*		初始化：配置参数、网卡、sock
*		等待连接、连接、等待数据接收处理
*		异常重连
*
***/
void w5500_dev_handle(void)
{

	static uint32_t timecnt = 0x00;
	
	switch(handlestep)
	{
		case W5500_DEV_INIT:
			{
			
				w5500_usart_printf_string("w5500 init....\n");
				w5500_comm_init();
				w5500_load_net_parameters();
				
				w5500_hardware_reset();
				w5500_initialization();
				
				IINCHIP_WRITE(Sn_KPALVTR(0),0X02); //Ｗ５５００与建立链接的ＳＯＣＫＥＴ有通信，这个keepalive　帧是不发送的，如果在１０Ｓ内的周期内没有网络数据　交互，该帧才发送

				handlestep = W5500_DEV_RUNNING;
			}
			break;

		case W5500_DEV_RUNNING:
			{
				w5500_socket_set();//W5500端口初始化配置 可以选择是什么模式
				if(W5500_Interrupt)//处理W5500中断
				{
					w5500_interrupt_process();//W5500中断处理程序框架  
				}
				sock_recv();	

				/*超时处理*/
				if (w5500_socket_timeout())
				{
					handlestep = W5500_DEV_REINIT;
				}

				if (timecnt ++ > 150)
				{
					timecnt = 0x00;
					if (!W5500_Read_Phy_Link_Status())
					{
						w5500_usart_printf_string("w5500 网卡断开连接。。。\n");
						handlestep = W5500_DEV_REINIT;
					}
				}

			}
			break;

		case W5500_DEV_REINIT:
			{
				timecnt  = 0x00;

				w5500_usart_printf_string("w5500 reinit....\n");
				
				w5500_hardware_reset();
				w5500_initialization();
				
				IINCHIP_WRITE(Sn_KPALVTR(0),0X02); //Ｗ５５００与建立链接的ＳＯＣＫＥＴ有通信，这个keepalive　帧是不发送的，如果在１０Ｓ内的周期内没有网络数据　交互，该帧才发送				
				handlestep = W5500_DEV_RUNNING;
		}
			break;
		default:
			break;
	}
	
}


/*******************************************************************************
* 函数名  : Process_Socket_Data
* 描述    : W5500接收并发送接收到的数据
* 输入    : s:端口号
* 输出    : 无
* 返回值  : 无
* 说明    : 本过程先调用S_rx_process()从W5500的端口接收数据缓冲区读取数据,
*			然后将读取的数据从Rx_Buffer拷贝到Temp_Buffer缓冲区进行处理。
*			处理完毕，将数据从Temp_Buffer拷贝到Tx_Buffer缓冲区。调用S_tx_process()
*			发送数据。
*******************************************************************************/
void Process_Socket_Data(SOCKET s)
{
	u16 size_r;
    u16 CalKey;     //计算的校验
    u16 RcvKey;     //接收的校验
	u8 size_s;      //发送数据
	u8 ret;
	
	uint8_t len = 0x00;
	uint8_t tmp[256] = {0x00};
	
	size_r=Read_SOCK_Data_Buffer(s, SocketInfo[s].Rx_Buffer);  //Rx_Buffer 这个是接收数据 size为接收到数据长度	

	if (size_r > 0x00)
	{
		w5500_usart_printf_string("length:");
		w5500_usart_printf_int(size_r);
		w5500_usart_printf_string("\n");
	}


	if ((size_r > 256) || (size_r == 0x00))
	{
		return;
	}

	/*接收到数据 ，清除数据接收超时标记*/
	SocketInfo[s].recvdatatimeout = 0x00;

#if DEBUG_W5500_PRINTF_ENABLE == 0x01		
	if (size_r < 128)
	{

		len = hextostring(tmp,SocketInfo[s].Rx_Buffer,size_r);	
		/*udp 数据帧头	: 对方IP地址   、端口、 接收字节长度					占用8字节长度*/
		w5500_usart_printf_string("data:");
		w5500_usart_printf_string(tmp);
		w5500_usart_printf_string("\n");
	}
#endif	

#if RTU_RECEIVE_DEBUG_ENABLE == 0X01
		/*记录接收到的数据信息
		*
		*
		*
		*
		*
		***/
		InfoCounter ++;
		InfoLen = size_r;
		
		/*数据接收时间*/
		Get_Time();

		memset(InfoBuffer,0,sizeof(InfoBuffer));
		sprintf(InfoBuffer,"%d-%02d-%02d %02d:%02d:%02d\t",Time.tm_year,Time.tm_mon,Time.tm_mday,Time.tm_hour,Time.tm_min,Time.tm_sec);

		if (InfoLen > 100)
		{
			InfoLen = 100;
		}
		len = hextostring(tmp,SocketInfo[s].Rx_Buffer,InfoLen);	
		memcpy(InfoBuffer+strlen(InfoBuffer),tmp,len);
#endif

	ret = MessageHandle(SocketInfo[s].Rx_Buffer,SocketInfo[s].Tx_Buffer,size_r,&size_s);
	if (ret == 0x00)
	{
		Write_SOCK_Data_Buffer(s,SocketInfo[s].Tx_Buffer,size_s); 		  //Socker发送数据 
	}

	memset(SocketInfo[s].Tx_Buffer,0,sizeof(SocketInfo[s].Tx_Buffer));
	memset(SocketInfo[s].Rx_Buffer,0,sizeof(SocketInfo[s].Rx_Buffer));

}

/*******************************************************************************
* 函数名  : Process_Socket_Data
* 描述    : W5500接收并发送接收到的数据
* 输入    : s:端口号
* 输出    : 无
* 返回值  : 无
* 说明    : 本过程先调用S_rx_process()从W5500的端口接收数据缓冲区读取数据,
*			然后将读取的数据从Rx_Buffer拷贝到Temp_Buffer缓冲区进行处理。
*			处理完毕，将数据从Temp_Buffer拷贝到Tx_Buffer缓冲区。调用S_tx_process()
*			发送数据。
*******************************************************************************/
void Process_Udp_Socket_Data(SOCKET s)
{
	u16 size_r = 0x00;
	u16 CalKey = 0x00;     //计算的校验
	u16 RcvKey = 0x00;     //接收的校验	
	u8 size_s = 0x00;      //发送数据

	uint8_t *rxPtr = NULL;
	uint8_t *txPtr = NULL;

	uint8_t rxlen = 0x00;
	uint8_t txlen = 0x00;
	
	uint8_t len = 0x00;	
	uint8_t tmp[256] = {0x00};
	
	size_r=Read_SOCK_Data_Buffer(s, SocketInfo[s].Rx_Buffer);  //Rx_Buffer 这个是接收数据 size为接收到数据长度
	if (size_r > 0x00)
	{
		w5500_usart_printf_string("length:");
		w5500_usart_printf_int(size_r);
		w5500_usart_printf_string("\n");
	}

	if ((size_r > 256) || (size_r == 0x00))
	{
		return;
	}
	
#if DEBUG_W5500_PRINTF_ENABLE == 0x01		
	if (size_r < 128)
	{	
		len = hextostring(tmp,SocketInfo[s].Rx_Buffer,size_r);

		/*udp 数据帧头  : 对方IP地址   、端口、 接收字节长度              	占用8字节长度*/
		w5500_usart_printf_string("data:");
		w5500_usart_printf_string(tmp);
		w5500_usart_printf_string("\n");
	}
#endif

	/*发送方 IP地址 和端口*/
  	memcpy(UDP_DIPR,SocketInfo[s].Rx_Buffer,0x04);
	memcpy(UDP_DPORT,SocketInfo[s].Rx_Buffer+ 0x04,0x02);
	
	w5500_usart_printf_string("client port:");
	w5500_usart_printf_int((UDP_DPORT[0] << 0x08) | UDP_DPORT[1]);
	w5500_usart_printf_string("\n");

	/*取出内容部分数据*/
	rxPtr = SocketInfo[s].Rx_Buffer+ 0x08;
	rxlen = size_r - 0x08;
	
	txPtr = SocketInfo[s].Tx_Buffer;

	anylise_udp_recevice(rxPtr,txPtr,rxlen,&size_s);

	w5500_usart_printf_string("size_s:");
	w5500_usart_printf_int(size_s);
	w5500_usart_printf_string("\n");

	if(size_s > 0x00)
	{
		/*同时向配置工具 和网关软件发送 响应*/
	
		/*网管软件；接收和发送端口号一致*/
		Write_SOCK_Data_Buffer(s,txPtr,size_s); 		  //Socker发送数据
		delay_ms(50);
		
		/*配置工具默认数据接收端口 :9998*/
		UDP_DPORT[0] = DEFAULT_UDP_CLIENT_PORT >> 8;
		UDP_DPORT[1] = DEFAULT_UDP_CLIENT_PORT & 0xff;
		
		Write_SOCK_Data_Buffer(s,txPtr,size_s); 		  //Socker发送数据
	}
	
	memset(SocketInfo[s].Tx_Buffer,0,sizeof(SocketInfo[s].Tx_Buffer));
	memset(SocketInfo[s].Rx_Buffer,0,sizeof(SocketInfo[s].Rx_Buffer));
}

#if RTU_RECEIVE_DEBUG_ENABLE == 0X01
u16 GetRecvMessageCounter(void)
{
	return InfoCounter;
}

u8 GetRecvMessage(u8 *pdata)
{
	memcpy(pdata,InfoBuffer,strlen(InfoBuffer));
	return strlen(InfoBuffer);
}
#else
u8 GetRecvMessage(u8 *pdata)
{
	return 0x00;
}
u16 GetRecvMessageCounter(void)
{
	0x00;
}

#endif

#else
void init_sock_info(void){}

/*网络参数配置*/
/*初始化*/
void init_net_info(){}

/*从flash读取配置信息*/
void read_net_info(){}

/**将修改的信息存储到flash*/
void write_net_info(){}
void w5500_dev_handle(void){}

u8 GetRecvMessage(u8 *pdata)
{
	return 0x00;
}
u16 GetRecvMessageCounter(void)
{
	0x00;
}

#endif


