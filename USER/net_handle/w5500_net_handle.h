#ifndef _W5500_NET_HANDLE_H
#define _W5500_NET_HANDLE_H

#include "flash.h"
#include "bsp_usart3.h"
#include "config.h"

//#define DEBUG_W5500_PRINTF_ENABLE 0X00

#define ENABLE_W5500_RECV_TIMEOUT_CHECK 0X01
#define W5500_RECV_TIMEOUT 5*600
#define RTU_RECEIVE_DEBUG_ENABLE 0X01


/*信息存储flash位置:起始地址*/
#define LOCAL_IP_ADDRESS 			DEVICE_NETINFO_STORAGE_PAGE + 0x00
#define LOCAL_GATEWAY_ADDRESS 	    DEVICE_NETINFO_STORAGE_PAGE+ 0X04
#define LOCAL_MASK_ADDRESS 			DEVICE_NETINFO_STORAGE_PAGE+ 0X08
#define LOCAL_MAC_ADDRESS 			DEVICE_NETINFO_STORAGE_PAGE+ 0X0C

/**是否使能手动复位IP地址*/
#define LOCAL_HAND_RECOVER_NET_ENABLE_ADDR DEVICE_NETINFO_STORAGE_PAGE + 0X14

/*多个 net 需要连接 */
#define SOCKET_NUM 0X02
#define BUFFER_LENGTH 512

/*默认网卡配置信息*/
#define DEFAULT_IP_1 192
#define DEFAULT_IP_2 168
#define DEFAULT_IP_3 0
#define DEFAULT_IP_4 200

#define DEFAULT_GATEWAY_1 192
#define DEFAULT_GATEWAY_2 168
#define DEFAULT_GATEWAY_3 0
#define DEFAULT_GATEWAY_4 1

#define DEFAULT_MASK_1 255
#define DEFAULT_MASK_2 255
#define DEFAULT_MASK_3 255
#define DEFAULT_MASK_4 0

#define DEFAULT_PHY_1 0x0c//加载物理地址
#define DEFAULT_PHY_2 0x29
#define DEFAULT_PHY_3 0xab
#define DEFAULT_PHY_4 0x7c
#define DEFAULT_PHY_5 0x00
#define DEFAULT_PHY_6 0x03

#define DEFAULT_UDP_CLIENT_PORT 9998

#if DEBUG_W5500_PRINTF_ENABLE == 0x01
	#define w5500_usart_printf_string(a)   usart_printf_string(a)
	#define w5500_usart_printf_int(a)   usart_printf_int(a)
	#define w5500_usart_printf(a,b)   usart_printf(a,b)
#else	
	#define w5500_usart_printf_string(a) 
	#define w5500_usart_printf_int(a) 
	#define w5500_usart_printf(a,b)   
#endif

/*******************
*交互的udp 客户端信息
*	
*网管IP地址 、端口
*配置工具 IP地址 、端口
*
**/


/*参数修改之类的标记*/
/*冷启动 :恢复默认IP地址*/
/*网络修改IP地址*/
/*定时时间到*/

/*网络参数*/
typedef struct
{
	Flash_Date ipaddr;
	Flash_Date gateway;
	Flash_Date netmask;
	Flash_Date mac[2];
}NET_INFO_STRUCT;


/*每个对象需要一个结构体 进行处理*/
typedef struct
{

	uint8_t Port[2];	//端口0的端口号() 
	uint8_t DIP[4];		//端口0目的IP地址 
	uint8_t DPort[2];	//端口0目的端口号() 

	uint8_t UDP_DIPR[4];	//UDP(广播)模式,目的主机IP地址
	uint8_t UDP_DPORT[2];	//UDP(广播)模式,目的主机端口号

	/*端口0的运行模式,0:TCP服务器模式,1:TCP客户端模式,2:UDP(广播)模式*/
	uint8_t mode;

	/*端口0状态记录,1:端口完成初始化,2端口完成连接(可以正常传输数据)*/
	uint8_t initstate;
	
	
	/******端口0接收和发送数据的状态,1:端口接收到数据,2:端口发送数据完成 ****/
	uint8_t datastate;		

	/***************----- 端口数据缓冲区 -----***************/
	uint8_t  Rx_Buffer[BUFFER_LENGTH];	//端口接收数据缓冲区 
	uint8_t  Tx_Buffer[BUFFER_LENGTH];	//端口发送数据缓冲区 

	uint8_t enable;		/*sock是否启用*/
	uint8_t sock;		/*socknum*/
	uint8_t timeout;
	uint32_t waitconntimeout;	/*等待连接超时*/
	uint32_t recvdatatimeout;	/*等待接收数据超时*/
	
}SOCKET_INFO_STRUCT;

/**************************w5500生命周期***************************/
typedef enum
{
	W5500_DEV_INIT = 0X00,
	W5500_DEV_RUNNING=0X01,
	W5500_DEV_REINIT = 0X02,
	
}W5500_NET_HANDLE_STEP;

extern SOCKET_INFO_STRUCT SocketInfo[SOCKET_NUM];
extern NET_INFO_STRUCT NetInfoStruct;


/*网络参数配置*/
/*初始化*/
extern void init_sock_info(void);

/*网络参数配置*/
/*初始化*/
extern void init_net_info(void);

/*从flash读取配置信息*/
extern void read_net_info(void);

/**将修改的信息存储到flash*/
extern void write_net_info(void);

extern void w5500_dev_handle(void);
static	u8 delay_xs ( int timecnt );

extern u8 GetRecvMessage(u8 *pdata);
extern u16 GetRecvMessageCounter(void);


#endif
