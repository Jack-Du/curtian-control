#include "os_cfg_app.h"
#include "os.h"
#include "os_cpu.h"
#include "cpu.h"
#include "wdg.h"	

SOFT_WATCH_DOG_TIMER  SoftWatchDogTimerList[MAX_TASK_ID]={0};

u8 StopWDTFeedMake=FALSE;

#if IWDG_ENABLE == 0x01
void FeedDog(void)
{
	IWDG_ReloadCounter();
}

void WatchDogInit(u8 prer,u16 rlr)
{
	
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	
  	IWDG_SetPrescaler(prer);

  	IWDG_SetReload(rlr);

	IWDG_ReloadCounter();

	IWDG_Enable();
}

void SuspenWdt(SWD_ID swd_id)
{
	SOFT_WATCH_DOG_TIMER *SoftWatchDogTimerPtr=SoftWatchDogTimerList;

	SoftWatchDogTimerPtr += swd_id;
	SoftWatchDogTimerPtr->WatchDogState=SWDT_STAT_SUSPEND;
}

void RunWdt(SWD_ID swd_id)
{
	SOFT_WATCH_DOG_TIMER *SoftWatchDogTimerPtr=SoftWatchDogTimerList;

	SoftWatchDogTimerPtr += swd_id;
	SoftWatchDogTimerPtr->WatchDogState=SWDT_STAT_RUN;
}

void SoftWdtISR(void)
{
	SOFT_WATCH_DOG_TIMER *SoftWatchDogTimerPtr=SoftWatchDogTimerList;
	u8 i =0;
	if(StopWDTFeedMake==TRUE)
	{
		return;	
	}
	
	for(i=0;i<MAX_TASK_ID;i++)
	{
		if(SoftWatchDogTimerPtr->WatchDogState == SWDT_STAT_RUN)		//运行状态
		{
			if(SoftWatchDogTimerPtr->WatchDogCurCount != SoftWatchDogTimerPtr->WatchDogLastCount)
			{
				SoftWatchDogTimerPtr->WatchDogLastCount=SoftWatchDogTimerPtr->WatchDogCurCount;
				SoftWatchDogTimerPtr->WatchDogTime=SoftWatchDogTimerPtr->WatchDogTimeOut;
			}
			else if(--SoftWatchDogTimerPtr->WatchDogTime == 0)
			{
				StopWDTFeedMake=TRUE;   	/*代表停止喂硬看门狗*/
				DebugOut(0,"\r\n %d 喂狗丢失，系统重启",i);
				
				return;
			}
		}
		SoftWatchDogTimerPtr++;
	}
	FeedDog();	
}

void SoftWDTInit(void)
{
	memset(SoftWatchDogTimerList,0,sizeof(SOFT_WATCH_DOG_TIMER)*MAX_TASK_ID);

	StopWDTFeedMake=FALSE;
	WatchDogInit(IWDG_Prescaler_64,0x271);		//1s : 1/40k * 625 

//	4095 * 6 
//		40k /6 == 6.67k
//		6.67k/4095 =  614ms

}

void SoftWDTRst(void)
{
  	u8 i=0;
	uint16_t Tmp_ArmTimDly = 0;
	
	SOFT_WATCH_DOG_TIMER *SoftWatchDogTimerPtr=SoftWatchDogTimerList;
	
	WatchDogInit(IWDG_Prescaler_64,0x271);//1s	
	DebugOut(0,"\r\n 看门狗停止，系统重启");
	
	for(i=0;i<MAX_TASK_ID;i++)
	{       
	   SoftWatchDogTimerPtr->WatchDogTime = 0;		
	   StopWDTFeedMake=TRUE;   	/*代表停止喂硬看门狗*/	   
	   SoftWatchDogTimerPtr++;
	}
}

void SoftReset(void)
{
   
}

bool SoftWdtInit(SWD_ID swd_id,u16 TimerTop)
{
	SOFT_WATCH_DOG_TIMER *SoftWatchDogTimerPtr=SoftWatchDogTimerList;
	u16 osTick=0;
	
	SoftWatchDogTimerPtr+=swd_id;
	if(SoftWatchDogTimerPtr->WatchDogState==SWDT_STAT_IDLE)
	{
#if SYSTEM_SUPPORT_OS == 0x01		
		osTick=(u16)OSCfg_TickRate_Hz*TimerTop;	//将s转换为节拍数
#endif
		
		SoftWatchDogTimerPtr->WatchDogTimeOut=osTick;
		SoftWatchDogTimerPtr->WatchDogTime=osTick;
		SoftWatchDogTimerPtr->WatchDogState=SWDT_STAT_RUN;

		return TRUE;
	}
	else
	{
		return FALSE;	
	}
}

void SoftWdtFeed(SWD_ID swd_id)
{
	
#if SYSTEM_SUPPORT_OS == 0x01
	CPU_SR  cpu_sr;
#endif
	
	SOFT_WATCH_DOG_TIMER *SoftWatchDogTimerPtr=SoftWatchDogTimerList;

	SoftWatchDogTimerPtr+=swd_id;
	
#if SYSTEM_SUPPORT_OS == 0x01
	OS_CRITICAL_ENTER();//进入临界区
#endif
	
	SoftWatchDogTimerPtr->WatchDogCurCount++;
	if(SoftWatchDogTimerPtr->WatchDogCurCount == SoftWatchDogTimerPtr->WatchDogLastCount)
	{
		SoftWatchDogTimerPtr->WatchDogCurCount++;
	}
#if SYSTEM_SUPPORT_OS == 0x01	
	OS_CRITICAL_EXIT();//进入临界区
#endif
	
}

#else

void SoftWdtISR(void){}

//系统启动时初始化
void SoftWDTInit(void){}

//任务中初始化软看门狗
bool SoftWdtInit(SWD_ID swd_id,u16 TimerTop){}

//软喂狗
void SoftWdtFeed(SWD_ID swd_id){}

void SuspenWdt(SWD_ID swd_id){}

void RunWdt(SWD_ID swd_id){}

void SoftWDTRst(void){}

void SoftReset(void){}
	
////硬件喂狗
void FeedDog(void){}

void WatchDogInit(u8 prer,u16 rlr){}
#endif
