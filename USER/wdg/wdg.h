#ifndef _WDG_H_
#define _WDG_H_

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "stm32f10x.h"

/*看门狗使能*/
#define IWDG_ENABLE 0x01
#include "w5500.h"

//typedef enum {FALSE = 0, TRUE = !DISABLE} bool;

//#define FALSE 0
//#define TRUE 1

/***************************************************************************************************************/
/***************************************************************************************************************/

/***
*
*定义每个任务对应的软件看门狗 id
*
*	
*
*
*******/
typedef enum
{
	
	WIFI_COMMUNICATION_TASK_ID=0,	
	ETHERNET_COMMUNICATION_TASK_ID,
	BOARD_CONTROL_TASK_ID,
	MAX_TASK_ID,
}SWD_ID;
	

typedef enum
{
	SWDT_STAT_IDLE,                 //空闲
	SWDT_STAT_SUSPEND,               //挂起
	SWDT_STAT_RUN                   //运行
}SWDT_STAT;

typedef struct soft_watch_dog_timer
{
	u16 WatchDogTimeOut;
	u16 WatchDogTime;           //喂狗时间
	u8  WatchDogCurCount;       //
	u8  WatchDogLastCount;      //
	SWDT_STAT WatchDogState;     //运行状态
}SOFT_WATCH_DOG_TIMER;

extern SOFT_WATCH_DOG_TIMER  SoftWatchDogTimerList[MAX_TASK_ID];

/***************************************************************************************/

extern void SoftWdtISR(void);

//系统启动时初始化
extern void SoftWDTInit(void);

//任务中初始化软看门狗
extern bool SoftWdtInit(SWD_ID swd_id,u16 TimerTop);	

//软喂狗
extern void SoftWdtFeed(SWD_ID swd_id);

extern void SuspenWdt(SWD_ID swd_id);

extern void RunWdt(SWD_ID swd_id);

extern void SoftWDTRst(void);

extern void SoftReset(void);

////硬件喂狗
extern void FeedDog(void);

extern void WatchDogInit(u8 prer,u16 rlr);

/***************************************************************************************/
#endif
