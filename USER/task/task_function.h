#ifndef _TASK_FUNCTION_H
#define _TASK_FUNCTION_H

#include "cpu.h"
#include "os.h"
#include "bsp_usart3.h"

/***************************************************************************************************************/
/***************************************************************************************************************/
//任务优先级
#define START_TASK_PRIO		3

//
#define WIFI_COMMUNICATION_TASK_PRIO		5
#define ETHERNET_COMMUNICATION_TASK_PRIO		6

//
#define BOARD_CONTROL_TASK_PRIO		4

//#define SYSTEM_START_INFO_TASK_PRIO		8


/***************************************************************************************************************/
/***************************************************************************************************************/

#define START_STK_SIZE 		128
extern OS_TCB StartTaskTCB;
extern CPU_STK START_TASK_STK[START_STK_SIZE];

//堆栈大小
#define WIFI_COMMUNICATION_STK_SIZE 		256

//任务控制块
extern OS_TCB WifiCommunicationTaskTCB;
//任务堆栈
extern CPU_STK WIFI_COMMUNICATION_TASK_STK[WIFI_COMMUNICATION_STK_SIZE];
//入口

//堆栈大小
#define ETHERNET_COMMUNICATION_STK_SIZE 		256

//任务控制块
extern OS_TCB EthernetCommunicationTaskTCB;
//任务堆栈
extern CPU_STK ETHERNET_COMMUNICATION_TASK_STK[ETHERNET_COMMUNICATION_STK_SIZE];
//入口		 

//堆栈大小
#define BOARD_CONTROL_STK_SIZE 		128

//任务控制块
extern OS_TCB BoardControlTaskTCB;
//任务堆栈
extern CPU_STK BOARD_CONTROL_TASK_STK[BOARD_CONTROL_STK_SIZE];
//入口	


////堆栈大小
//#define SYSTEM_START_INFO_STK_SIZE 		128

////任务控制块
//extern OS_TCB SystemStartInfoTaskTCB;
////任务堆栈
//extern CPU_STK SYSTEM_START_INFO_TASK_STK[SYSTEM_START_INFO_STK_SIZE];
//入口	

/***************************************************************************************************************/
/***************************************************************************************************************/

#define TASK_FUNCTION_DEBUG_ENABLE 0x01

#if TASK_FUNCTION_DEBUG_ENABLE == 0x01
	#define task_function_printf_string(a)   usart_printf_string(a)
	#define task_function_printf_int(a)   usart_printf_int(a)
	#define task_function_printf(a,b)   usart_printf(a,b)
#else	
	#define task_function_printf_string(a) 
	#define task_function_printf_int(a) 
	#define task_function_printf(a,b)   
#endif

/***************************************************************************************************************/
/***************************************************************************************************************/

extern void start_task(void *p_arg);

void WiFiCommunicationThread(void *arg);
void EthernetCommunicationThread(void *arg);
void BoardControlThread(void *arg);
void SystemStartInfoThread(void *arg);

extern void App_TimeTickHook(void);

#endif
