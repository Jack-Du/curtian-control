#include "task_function.h"
#include "out.h"
#include "HWLED.H"  
#include "ext_in.h" 
#include "config.h"
#include "bsp_usart3.h"
#include "w5500_net_handle.h"
#include "serial_comm.h"
#include "system_start_info.h"
#include "rtl8711_wifi_driver.h"
#include "wdg.h"

OS_TCB StartTaskTCB;
CPU_STK START_TASK_STK[START_STK_SIZE];

//任务控制块
OS_TCB WifiCommunicationTaskTCB;
//任务堆栈
CPU_STK WIFI_COMMUNICATION_TASK_STK[WIFI_COMMUNICATION_STK_SIZE];
//入口


//任务控制块
OS_TCB EthernetCommunicationTaskTCB;
//任务堆栈
CPU_STK ETHERNET_COMMUNICATION_TASK_STK[ETHERNET_COMMUNICATION_STK_SIZE];
//入口		 


//任务控制块
OS_TCB BoardControlTaskTCB;
//任务堆栈
CPU_STK BOARD_CONTROL_TASK_STK[BOARD_CONTROL_STK_SIZE];

////任务控制块
//OS_TCB SystemStartInfoTaskTCB;
////任务堆栈
//CPU_STK SYSTEM_START_INFO_TASK_STK[SYSTEM_START_INFO_STK_SIZE];


//定时器配置信息
OS_TMR SystemStartInfoTmr;
void SystemStartInfoTmrTask(void *p_tmr, void *p_arg);

//入口	
/*! @function
********************************************************************************
<PRE>
函数名   : Start_Task
功能     : 初始化任务
----------------------------------------------------------------------------------
参数     : 无
返回值   : 无
</PRE>
*******************************************************************************/         
void start_task(void *p_arg)
{
	OS_ERR err;
	CPU_SR_ALLOC();
	p_arg = p_arg;
	
	CPU_Init();
#if OS_CFG_STAT_TASK_EN > 0u
   OSStatTaskCPUUsageInit(&err);  	//统计任务          
#endif
	
#ifdef CPU_CFG_INT_DIS_MEAS_EN		//测量中断关闭
    CPU_IntDisMeasMaxCurReset();	
#endif
	
#if	OS_CFG_SCHED_ROUND_ROBIN_EN  //当使用时间片轮转的时候
	 //使能时间片轮转调度功能，时间片长度为1个系统时钟节拍， 即 1*5 = 5ms
	OSSchedRoundRobinCfg(DEF_ENABLED,1,&err);  
#endif		

	/*创建子任务*/
	OS_CRITICAL_ENTER();	//进入临界区

	/*wifi网络通信*/
	OSTaskCreate((OS_TCB 	* )&WifiCommunicationTaskTCB,		
			 (CPU_CHAR	* )"WiFi Communication task", 		
             (OS_TASK_PTR )WiFiCommunicationThread, 			
             (void		* )0,					
             (OS_PRIO	  )WIFI_COMMUNICATION_TASK_PRIO,     
             (CPU_STK   * )&WIFI_COMMUNICATION_TASK_STK[0],	
             (CPU_STK_SIZE)WIFI_COMMUNICATION_STK_SIZE/10,	
             (CPU_STK_SIZE)WIFI_COMMUNICATION_STK_SIZE,		
             (OS_MSG_QTY  )0,					
             (OS_TICK	  )0,					
             (void   	* )0,					
             (OS_OPT      )OS_OPT_TASK_STK_CHK|OS_OPT_TASK_STK_CLR,
             (OS_ERR 	* )&err);	

	/*以太网网络通信*/
	 OSTaskCreate((OS_TCB	 * )&EthernetCommunicationTaskTCB,	 
			  (CPU_CHAR  * )"Ethernet Communication task",		 
			  (OS_TASK_PTR )EthernetCommunicationThread, 		 
			  (void 	 * )0,					 
			  (OS_PRIO	   )ETHERNET_COMMUNICATION_TASK_PRIO, 	
			  (CPU_STK	 * )&ETHERNET_COMMUNICATION_TASK_STK[0],	 
			  (CPU_STK_SIZE)ETHERNET_COMMUNICATION_STK_SIZE/10, 
			  (CPU_STK_SIZE)ETHERNET_COMMUNICATION_STK_SIZE,		 
			  (OS_MSG_QTY  )0,					 
			  (OS_TICK	   )0,					 
			  (void 	 * )0,					 
			  (OS_OPT	   )OS_OPT_TASK_STK_CHK|OS_OPT_TASK_STK_CLR,
			  (OS_ERR	 * )&err);	 

	/*冷启动；网络参数恢复默认值*/
//  	OSTaskCreate((OS_TCB    * )&SystemStartInfoTaskTCB,   
//  			(CPU_CHAR  * )"System Start Info Task",	   
//  			(OS_TASK_PTR )SystemStartInfoThread,		   
//  			(void	   * )0,				   
//  			(OS_PRIO	 )SYSTEM_START_INFO_TASK_PRIO,   
//  			(CPU_STK   * )&SYSTEM_START_INFO_TASK_STK[0],	   
//  			(CPU_STK_SIZE)SYSTEM_START_INFO_STK_SIZE/10, 
//  			(CPU_STK_SIZE)SYSTEM_START_INFO_STK_SIZE,		   
//  			(OS_MSG_QTY  )0,				   
//  			(OS_TICK	 )0,				   
//  			(void	   * )0,				   
//  			(OS_OPT 	 )OS_OPT_TASK_STK_CHK|OS_OPT_TASK_STK_CLR,
//  			(OS_ERR    * )&err);   


//创建定时器1
/**
*
*	定时器 中断频率 :OS_CFG_TMR_TASK_RATE_HZ = 100hz  = 10ms
*	
*
**/
	OSTmrCreate((OS_TMR		*)&SystemStartInfoTmr,		//定时器1
		(CPU_CHAR	*)"System  Start Info Tmr",		//定时器名字
		(OS_TICK	 )0,			//
		(OS_TICK	 )(100*30),          //100*10 * 30=1000ms * 30 = 30s
		(OS_OPT		 )OS_OPT_TMR_PERIODIC, //周期模式
		(OS_TMR_CALLBACK_PTR)SystemStartInfoTmrTask,//定时器1回调函数
		(void	    *)0,			//参数为0
		(OS_ERR	    *)&err);		//返回的错误码
								
	OSTmrStart(&SystemStartInfoTmr,&err);	//开启定时器1			

	
	/*继电器、红外控制*/
	  OSTaskCreate((OS_TCB	  * )&BoardControlTaskTCB,	  
			   (CPU_CHAR  * )"Board Control task",		  
			   (OS_TASK_PTR )BoardControlThread,		  
			   (void	  * )0, 				  
			   (OS_PRIO 	)BOARD_CONTROL_TASK_PRIO,	 
			   (CPU_STK   * )&BOARD_CONTROL_TASK_STK[0],   
			   (CPU_STK_SIZE)BOARD_CONTROL_STK_SIZE/10, 
			   (CPU_STK_SIZE)BOARD_CONTROL_STK_SIZE,		  
			   (OS_MSG_QTY	)0, 				  
			   (OS_TICK 	)0, 				  
			   (void	  * )0, 				  
			   (OS_OPT		)OS_OPT_TASK_STK_CHK|OS_OPT_TASK_STK_CLR,
			   (OS_ERR	  * )&err);   

	/*定时器 ：冷启动 网络参数配置检测*/
	
	OS_CRITICAL_EXIT();	//退出临界区

}


/*****
****
****wifi 网络通信
****
****
****
*****************/
void WiFiCommunicationThread(void *arg)
{
	OS_ERR err;

	SoftWdtInit(WIFI_COMMUNICATION_TASK_ID,30);
	RunWdt(WIFI_COMMUNICATION_TASK_ID);
	
	while(1)
	{
	
		SoftWdtFeed(WIFI_COMMUNICATION_TASK_ID);
		
		rtl8711_wifi_module_handle();
		OSTimeDly(40,OS_OPT_TIME_HMSM_STRICT,&err);	//
	}
}

/*****
****
****以太网 网络通信
****
****
****
*****************/
void EthernetCommunicationThread(void *arg)
{
	OS_ERR err;

	SoftWdtInit(ETHERNET_COMMUNICATION_TASK_ID,30);
	RunWdt(ETHERNET_COMMUNICATION_TASK_ID);

	while (1)
	{
		SoftWdtFeed(ETHERNET_COMMUNICATION_TASK_ID);
		
		w5500_dev_handle();
		OSTimeDly(40,OS_OPT_TIME_PERIODIC,&err); //UCOSIII延时采用周期模式

		//task_function_printf_string("\n....ethernet recycle....\n");
		
	}
}

/*****
****
****记录系统启动信息
****	如果系统正常冷启动，并且运行时间超过5分钟，则删除冷启动次数标记
****			上电检测冷启动测试标记，如果冷启动测试标记 > 5,则 将以太网配置网卡参数设置成默认值
****			定时器定时时间超过5分钟之后，自动删除该定时器
****				
****		定时器周期 : 周期 1/100 * 100 = 1s
****参数 :null
****返回值 :null
*****************/
//void SystemStartInfoThread(void *arg)
//{
//	OS_ERR err;

//	while (1)
//	{		
//		system_start_info_handle();
//		OSTimeDly(40,OS_OPT_TIME_HMSM_STRICT,&err);	//900us 	

//		if (0)
//		{
//			OSTaskDel(0,&err);
//		}
//	}
//}

void SystemStartInfoTmrTask(void *p_tmr, void *p_arg)
{
	u8 ret = 0x00;
	OS_ERR err;

	system_start_info_handle();
	
	/*当时5分钟之后 ，自动删除该定时器*/
	if (ret = get_system_start_info_timeout_state())
	{
		//
	}
//	task_function_printf_string("\n....Timer Task....\n");

}


/*****
****
****板载设备控制
****
****
****
*****************/
void BoardControlThread(void *arg)
{
	OS_ERR err;

	SoftWdtInit(BOARD_CONTROL_TASK_ID,30);
	RunWdt(BOARD_CONTROL_TASK_ID);
	
	while(1)
	{
	
		SoftWdtFeed(BOARD_CONTROL_TASK_ID);

		relay_ctrl_handle();


		/* 关闭系统任务调度 :避免任务调度，保证红外指令发送完全*/
		if(FJPG_HW_Send_Bit == 0x02)
		{

			 delay_osschedlock();						//阻止OS调度，防止打断us延时
			 SendData_Fjpg(FJPG_HW_TAB);	 // 发送红外数据 风机盘管空调 SendData_hw(FJPG_HW_TAB); 	// 发送红外数据 风机盘管空调
			 delay_osschedunlock(); 					 //恢复OS调度										 
			 FJPG_HW_Send_Bit = 0;
		}
		
		if(GL_HW_Send_Bit == 0x02)
		{
			delay_osschedlock();						//阻止OS调度，防止打断us延时	
			SendData_Gl(GL_HW_TAB); 	 // 发送红外数据 格力空调 
			delay_osschedunlock();						//恢复OS调度										
			GL_HW_Send_Bit = 0;
		}	
		if(Haier_HW_Send_Bit == 0x02)
		{
			delay_osschedlock();						//阻止OS调度，防止打断us延时
			SendData_Haier(Haier_HW_TAB);	 // 发送红外数据 海尔空调 	
			delay_osschedunlock();						//恢复OS调度										
			Haier_HW_Send_Bit = 0;
		}	
		
		if ( GLx_HW_Send_Bit == 0x02 )
		{
			delay_osschedlock();						//阻止OS调度，防止打断us延时		
			SendData_Glx ( Glx_HW_TAB );	//
			delay_osschedunlock();						//恢复OS调度										
			
			GLx_HW_Send_Bit = 0;
		}

				if ( Md_HW_Send_Bit == 0x02 )
		{
		
			delay_osschedlock();						//阻止OS调度，防止打断us延时
			SendData_Midea ( Md_HW_TAB ,Md_HW_Send_Type);	//
			delay_osschedunlock();						//恢复OS调度										
			
			Md_HW_Send_Bit = 0;
		}
				

		/* 关闭系统任务调度 :避免任务调度，保证红外指令发送完全*/
		
		OSTimeDly(40,OS_OPT_TIME_HMSM_STRICT,&err);	//

	}
	
}

/*****
*
*
*定时器处理 :接口
*	在定时器钩子函数中被调用
*	每个定时周期执行一次
*
*	参数 : null
*	返回值: null
*
*
***********/
void App_TimeTickHook(void)
{

	/*软狗 */
	SoftWdtISR();
	rtc_internal_timing();
	
	return ;
}



/**
*** 打印线程信息
***
***
***
***
********/
//void printf_pcb_info(void)
//{
//	u8 i = 0;
//	u8 err = 0,TaskState = 0;
//	OS_TCB tempOsTcb;
//	OS_STK_DATA tempSTKData;
//	u32 nTotalSTK;
//	printlan(75,'-');
//	
//	DebugOut(0,"\r\n");
//	DebugOut(0,"\r\n Micrium uC/OS-II  \r\n");	
//	DebugOut(0," ST STM32 (Cortex-M3)\r\n\r\n");  
//	DebugOut(0," uC/OS-II: V%ld.%ld%ld\r\n",OSVersion()/100,(OSVersion() % 100) /	10,(OSVersion() % 10));  
//	DebugOut(0," TickRate: %ld	\r\n",OS_TICKS_PER_SEC);  
//	DebugOut(0," CPU Usage: %ld%	\r\n",OSCPUUsage);	
//	DebugOut(0," #Ticks: %ld  \r\n",OSTime);  
//	DebugOut(0," #CtxSw: %ld  \r\n",OSCtxSwCtr); 
//	DebugOut(0,"\r\n任务名		 优先级	 运行状态 堆栈情况(字节)");	
//	DebugOut(0,"\r\n");
//	
//	for(i = 0;i<OS_LOWEST_PRIO+1;i++)
//	{
//		err = OSTaskQuery(i,&tempOsTcb);
//		if(err != OS_ERR_NONE)
//		continue;
//		if(strlen(tempOsTcb.OSTCBTaskName) >= 8)
//		{
//		DebugOut(0,"%s\t%d\t",tempOsTcb.OSTCBTaskName,i);
//		}
//		else
//		{
//		DebugOut(0,"%s\t\t%d\t",tempOsTcb.OSTCBTaskName,i);
//		}
//		TaskState = tempOsTcb.OSTCBStat;
//		switch(TaskState)
//			{
//			case OS_STAT_RDY:
//				DebugOut(0,"就绪\t");
//			break;
//			case OS_STAT_SEM:
//				DebugOut(0,"等待信号量");
//			break;
//			case OS_STAT_MBOX:
//				DebugOut(0,"等待邮箱");
//			break;
//			case OS_STAT_Q:
//				DebugOut(0,"等待队列");
//			break;
//			case OS_STAT_SUSPEND:
//				DebugOut(0,"挂起\t");
//			break;
//			case OS_STAT_MUTEX:
//				DebugOut(0,"互斥量\t");
//			break;
//			case OS_STAT_FLAG:
//				DebugOut(0,"等待事件标志");
//			break;
//			case OS_STAT_MULTI:
//				DebugOut(0,"等待多个事件");
//			break;	
//			default:
//				DebugOut(0,"错误状态:%d",TaskState);
//			break;
//			}
//		DebugOut(0,"\t");
//	
//		err = OSTaskStkChk(i,&tempSTKData);
//		if(err != OS_ERR_NONE)
//		{
//			switch(err)
//				{
//				  case OS_ERR_PRIO_INVALID:
//					DebugOut(0,"优先级高于最大值\r\n");
//					break;
//				  case OS_ERR_TASK_NOT_EXIST:
//					DebugOut(0,"任务尚未创建\r\n");
//					break;
//				  case OS_ERR_TASK_OPT:
//					DebugOut(0,"如果你没有指定os_task_opt_stk_chk当任务被创建\r\n");
//					break;
//				   case OS_ERR_PDATA_NULL:
//					DebugOut(0,"p_stk_data是一个空指针\r\n");
//					break;
//				  default:
//					break;
//				}
//			continue;
//		}
//			
//			nTotalSTK = tempSTKData.OSUsed + tempSTKData.OSFree;
//			DebugOut(0,"%d/%d\r\n",tempSTKData.OSUsed,nTotalSTK);	
//		
//		}
//		printlan(75,'-');
//		DebugOut(0,"\r\n");
//}

