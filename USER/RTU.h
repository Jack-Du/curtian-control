#ifndef __RTU_H__
#define __RTU_H__

#include "stm32f10x.h"
#include "CRC.h"


void ReadHoldingReg(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s);//pointer_1用作输入pointer_2用作输出 
void ReadIntputReg(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s);  // pointer_1是从网口接收缓存指针pointer_2是网口发送缓存指针  04 功能码  
void WritHoldingReg(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s); //写数据到单个寄存器中  06 
void WritHoldingReg7(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s);//批量控制寄存器      07功能码
void WritHoldingReg8(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s);//批量控制寄存器      08功能码
void WritHoldingReg9(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s);//批量控制寄存器      09功能码
void WritHoldingRegA(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s);//批量控制寄存器      0A功能码
void WritHoldingRegB(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s);//批量控制寄存器      0B功能码
void WritHoldingRegC(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s);//批量控制寄存器      0C功能码
void ReadHeartBeat(u8 *pointer_1,u8 *pointer_2,u8 len_r,u8* len_s);		//心跳   	      16功能码

extern void   ErrorHandle ( u8 Mode,u8* Pointer ,u8 *SendNum);
extern u8 MessageHandle ( u8* pointer_in,u8* pointer_out,u8 rev_num,u8 * send_num);

#endif


